/*====*====*====*====*====*====*====*====*====*====*====*====*====*====*====*
  Copyright (c) 2015-2017, 2020 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
=============================================================================*/

package com.qualcomm.location.izatprovider;

import java.io.IOException;
import java.lang.NullPointerException;
import android.content.pm.PackageManager.NameNotFoundException;
import java.util.concurrent.atomic.AtomicBoolean;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationProvider;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationRequest;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.os.WorkSource;
import android.os.RemoteException;
import android.util.Log;
import com.android.internal.location.ProviderProperties;
import java.util.List;
import java.util.ArrayList;
import com.android.internal.location.ILocationProvider;
import com.android.internal.location.ProviderRequest;
import com.android.server.location.LocationProviderProxy;
import com.android.server.location.AbstractLocationProvider;
import com.android.server.location.AbstractLocationProvider.State;
import com.android.server.location.MockableLocationProvider;
import com.qualcomm.location.izatprovider.IzatProvider.IzatProviderContext;
import com.qualcomm.location.R;
import com.qualcomm.location.izat.izatconfig.IzatConfigService;

import vendor.qti.gnss.V3_0.ILocHidlGnss;
import vendor.qti.gnss.V1_0.ILocHidlIzatOsNpGlue;
import vendor.qti.gnss.V1_0.ILocHidlIzatOsNpGlueCallback;
import vendor.qti.gnss.V1_0.LocHidlIzatRequest;
import vendor.qti.gnss.V1_0.LocHidlIzatLocation;
import vendor.qti.gnss.V1_0.LocHidlNetworkPositionSourceType;
import java.util.NoSuchElementException;
import com.qualcomm.location.hidlclient.HidlClientUtils;
import com.qualcomm.location.hidlclient.BaseHidlClient;
import com.qualcomm.location.utils.IZatServiceContext;

// Wraps OSNP service
public class NpProxy implements Handler.Callback {

    private int getNpImplPackage(int startIdx, String packageName) {
        for (int i = startIdx; i < mNpImplIds.length; i++) {
            if (mContext.getResources().getString(mNpImplIds[i][0]).equals(packageName)) {
                return i;
            }
        }
        return mNpImplIds.length;
    }

    private void setNpImplePackageNames() {

        String defaultServicePackageName = IzatConfigService.getOsnpPackagename(mContext);
        String regionServicePackageName = IzatConfigService.getRegionalNlpPackagename(mContext);
        int nlp_mode = IzatConfigService.getNlpMode(mContext);
        mTestMode = IzatConfigService.getNlpTestMode(mContext);

        log("defaultServicePackageName = " + defaultServicePackageName);
        log("regionServicePackageName = " + regionServicePackageName);
        log("nlp_mode = " + nlp_mode);

        boolean osNlpOnly = ( OSNLP_ONLY == nlp_mode);
        mQnpOnly = ( QNP_ONLY == nlp_mode);

        mIzatProviderContext.setNlpConfig(osNlpOnly, mQnpOnly);

        mOverlayResId = R.bool.config_enableNpOverlay;
        mOsnlpResId = R.string.config_OSNP_PackageName;
        mRegionalOsnlpResId = R.string.config_regional_OSNP_Baidu_PackageName;

        int implIdx = mNpImplIds.length;
        if ((implIdx = getNpImplPackage(0, defaultServicePackageName)) < mNpImplIds.length) {
            mOsnlpResId = mNpImplIds[implIdx][0];
            mAccuracyThreshold = mNpImplIds[implIdx][1];
        }

        if ((implIdx = getNpImplPackage(1, regionServicePackageName)) < mNpImplIds.length) {
            mRegionalOsnlpResId = mNpImplIds[implIdx][0];
            mRegionalAccuracyThreshold = mNpImplIds[implIdx][1];
        }

        if (mTestMode) {
            mOverlayResId = R.bool.testconfig_enableNpOverlay;
            mOsnlpResId = R.string.testconfig_PackageName;
        }
    }

    private boolean checkPackageExists(String packageName)
    {
        boolean exists = (packageName != null && !packageName.isEmpty());

        if (exists) {
            try {
                String info = mContext.getPackageManager()
                                      .getPackageInfo(packageName, 0)
                                      .versionName;
                exists = (info != null || !info.isEmpty());
            } catch (NameNotFoundException e) {
                exists = false;
            }
        }

        return exists;
    }

    private void initServiceBinding() {
        if (null == mOSNpHandle && !mQnpOnly) {
            mOSNpHandle = new LocationController();
            LocationProviderProxy OsNp =
                    LocationProviderProxy.createAndRegister(mContext,
                                                        NETWORK_LOCATION_SERVICE_ACTION,
                                                        R.bool.config_enableNpOverlay,
                                                        mOsnlpResId);
            mOSNpHandle.attachProvider(OsNp);
        }

        if (null == mRegionOSNpHandle) {
            mRegionOSNpHandle = new LocationController();
            LocationProviderProxy RegionalNp =
                    LocationProviderProxy.createAndRegister(mContext,
                                                        NETWORK_LOCATION_SERVICE_ACTION,
                                                        R.bool.config_enableNpOverlay,
                                                        mRegionalOsnlpResId);
            mRegionOSNpHandle.attachProvider(RegionalNp);
        }

        updateServiceBinding();
    }

    private void updateServiceBinding() {
        boolean useRegional = mDeviceInRegion;
        boolean hasDefaultNp = checkPackageExists(mContext.getResources().getString(mOsnlpResId));
        boolean hasRegionNp = checkPackageExists(mContext.getResources()
                .getString(mRegionalOsnlpResId));

        log("hasRegionNp = " + hasRegionNp + "; hasDefaultNp = " +
            hasDefaultNp);

        // only having default NP doesn't mean we could use it. If device is in
        // the reagion where it can not be used, i.e if (useRegional == true),
        // having default NP does no good. So hasAndAllowDefaultNp means we have
        // default NP AND we can use it.
        // Also if configuration is QnpOnly, don't use it.
        boolean hasAndAllowDefaultNp = (hasDefaultNp && !mQnpOnly && !useRegional);

        // prefer default NP if we can use it
        if (hasAndAllowDefaultNp) {
            mActiveNpHandle = mOSNpHandle;
            mActiveAccuracyThreshold = mAccuracyThreshold;
            log("OS NLP is selected to run");
        } else if (hasRegionNp) {
            mActiveNpHandle = mRegionOSNpHandle;
            mActiveAccuracyThreshold = mRegionalAccuracyThreshold;
            log("Regional NLP is selected to run");
        } else {
            // no NLP we can use
            Log.w(TAG, "Device has no NLP service to use");
        }
    }

    private void setRequestTry() {
        if (null != mRequests && null != mActiveNpHandle) {
            mActiveNpHandle.setRequest(mRequests);
            if (!mRequests.reportLocation) {
                mRequests = null;
            }
        }
    }

    NpProxy(Context context, Looper looper, IzatProviderContext izatProviderContext) {
        mContext = context;
        mIzatProviderContext = izatProviderContext;
        mHandler = new Handler(looper, this);
        mHandler.sendEmptyMessage(MSG_INIT);
        mDeviceInRegion = false;
        mRequests = null;
    }

    void destroy() {
        mRequests = null;
    }

    // Methods Called by OsNpHidlClient
    private void onSetRequest(long interval, float smallestDisplacement, int numUpdates) {
        LocationRequest locationRequest = LocationRequest
                .createFromDeprecatedProvider(LocationManager.NETWORK_PROVIDER,
                                              interval, smallestDisplacement,
                                              numUpdates == 1);
        ProviderRequest.Builder requestBuilder = new ProviderRequest.Builder();
        requestBuilder.setInterval(interval);
        ArrayList<LocationRequest> locationRequests = new ArrayList<LocationRequest>();
        locationRequests.add(locationRequest);
        requestBuilder.setLocationRequests(locationRequests);

        WorkSource workSource = new WorkSource();
        if (null != mIzatProviderContext.getNetworkProviderInner(mContext)) {
            WorkSource ws = mIzatProviderContext
                    .getNetworkProviderInner(mContext).getWorkSource();
            if (null != ws) {
                workSource.add(ws);
            }
        }
        requestBuilder.getWorkSource().add(workSource);
        mHandler.obtainMessage(MSG_SET_REQUEST, requestBuilder.build()).sendToTarget();
    }

    private void onStopRequest() {
        ProviderRequest.Builder requestBuilder = new ProviderRequest.Builder();
        mHandler.obtainMessage(MSG_SET_REQUEST, requestBuilder.build()).sendToTarget();
    }

    private boolean clearMarkerIfForScreening(Location location) {
        Bundle extras = location.getExtras();
        boolean marked = (extras != null &&
                          extras.containsKey ("com.qualcomm.location.nlp:screen"));

        if (marked) {
            extras.remove("com.qualcomm.location.nlp:screen");
        }

        return marked;
    }

    static private void log(String msg) {
        if (DEBUG) Log.d(TAG, msg);
    }

    // Message handler
    public boolean handleMessage(Message msg) {
        int msgID = msg.what;
        log("handleMessage what - " + msgID);

        switch (msgID) {
        case MSG_INIT:
        {
            mHidlClient = new OsNpHidlClient();
            setNpImplePackageNames();

            initServiceBinding();

            break;
        }
        case MSG_SET_REQUEST:
        {
            mRequests = (ProviderRequest)msg.obj;
            setRequestTry();
            break;
        }
        case MSG_LOCATION_CHANGED:
        {
            int source = -1;
            Location location = (Location)msg.obj;

            if (location != null) {

                log("Location: " + location.toString());

                Bundle extras = location.getExtras();
                if (extras != null) {
                    String strSource = extras.getString(IzatProvider.NLP_TYPE_KEY, "");
                    if (strSource != null) {
                        if (strSource.equalsIgnoreCase("cell")) {
                            source = 0;
                        } else if (strSource.equalsIgnoreCase("wifi")) {
                            source = 1;
                        }
                    }
                }

                mHidlClient.locationChanged(true, location.getTime(),
                                            true, location.getElapsedRealtimeNanos(),
                                            true, location.getLatitude(),
                                            true, location.getLongitude(),
                                            location.hasAccuracy(),
                                            location.getAccuracy() < mActiveAccuracyThreshold?
                                            mActiveAccuracyThreshold : location.getAccuracy(),
                                            location.hasVerticalAccuracy(),
                                            location.getVerticalAccuracyMeters(),
                                            location.hasAltitude(), location.getAltitude(),
                                            location.hasBearing(), location.getBearing(),
                                            location.hasSpeed(), location.getSpeed(),
                                            source != -1, source);
            }
            break;
        }
        case MSG_REGIONAL_NP_UPDATE:
            boolean deviceInRegion = (msg.arg1 == 1);
            if (mDeviceInRegion != deviceInRegion) {
                mDeviceInRegion = deviceInRegion;
                updateServiceBinding();
            }
            break;
        default:
            Log.w(TAG, "Unhandled Message " + msg.what);
        }
        return true;
    }

    private class LocationController implements MockableLocationProvider.Listener {

        private MockableLocationProvider mProvider;

        private LocationController() {
            mProvider = new MockableLocationProvider(new Object(), this);
        }

        public void attachProvider(AbstractLocationProvider provider) {
            mProvider.setRealProvider(provider);
        }

        public void setRequest(ProviderRequest request) {
            mProvider.setRequest(request);
        }

        @Override
        public void onReportLocation(Location location) {
            if (LocationManager.NETWORK_PROVIDER.equals(location.getProvider())) {
                mHandler.obtainMessage(MSG_LOCATION_CHANGED, location).sendToTarget();
            }
        }

        @Override
        public void onReportLocation(List<Location> locations) {
        }

        @Override
        public void onStateChanged(State oldState, State newState) {
            if (null != mIzatProviderContext.getNetworkProviderInner(mContext)) {
                if (null != mIzatProviderContext) {
                    mIzatProviderContext.setOsConsent(newState.allowed);
                }
            }
        }
    }

    // Constant data members
    private static final int MSG_INIT               = IZatServiceContext.MSG_NPPROXY_BASE + 1;
    private static final int MSG_SET_REQUEST        = IZatServiceContext.MSG_NPPROXY_BASE + 3;
    private static final int MSG_LOCATION_CHANGED   = IZatServiceContext.MSG_NPPROXY_BASE + 4;
    private static final int MSG_REGIONAL_NP_UPDATE = IZatServiceContext.MSG_NPPROXY_BASE + 5;

    private static final int        OSNLP_ONLY                = 1;
    private static final int        QNP_ONLY                  = 2;

    private static final String     TAG   = "NpProxy";
    private static final boolean    DEBUG = Log.isLoggable (TAG, Log.DEBUG);

    // static class members
    // data members
    private Handler                 mHandler;
    private Context                 mContext;
    private IzatProviderContext     mIzatProviderContext;
    private boolean                 mDeviceInRegion;
    private ProviderRequest         mRequests;
    private OsNpHidlClient          mHidlClient;

    private LocationController      mOSNpHandle;
    private LocationController      mRegionOSNpHandle;
    private LocationController      mActiveNpHandle;
    private float                   mAccuracyThreshold;
    private float                   mRegionalAccuracyThreshold;
    private float                   mActiveAccuracyThreshold;
    private boolean                 mQnpOnly;

    private boolean                 mTestMode = false;

    // initialized in the static clause, per izat.conf
    private static int              mOverlayResId;
    private static int              mRegionalOsnlpResId;
    private static int              mOsnlpResId;

    //3rd column is used to populate hardcoded threshold associated with that NLP implementation
    private static final int mNpImplIds[][] = {
            {R.string.config_OSNP_PackageName, 0},
            {R.string.config_regional_OSNP_Baidu_PackageName, 1000},
            {R.string.config_regional_OSNP_AutoNavi_PackageName, 0},
            {R.string.config_regional_OSNP_Tencent_PackageName, 0},
            {R.string.config_regional_OSNP_AutoNavi_PackageName_1, 0}};

    private static final String NETWORK_LOCATION_SERVICE_ACTION =
            "com.android.location.service.v3.NetworkLocationProvider";

    public void setRegionalNpRegulate(boolean regulated) {
        mHandler.obtainMessage(MSG_REGIONAL_NP_UPDATE, regulated ? 1 : 0, 0).sendToTarget();
    }

    private class OsNpHidlClient extends BaseHidlClient
            implements BaseHidlClient.IServiceDeathCb {

        private ILocHidlIzatOsNpGlue mOsNpIface;
        private OsNpCallback mOsNpCb;

        private OsNpHidlClient() {
            getOsNpIface();
            if (null != mOsNpIface) {
                try {
                    mOsNpCb = new OsNpCallback(NpProxy.this);
                    mOsNpIface.setCallback(mOsNpCb);
                } catch (RemoteException e) {
                }
            }
        }

        private void getOsNpIface() {
            if (null == mOsNpIface) {
                ILocHidlGnss service = (ILocHidlGnss)getGnssService();

                if (null != service) {
                    try {
                        mOsNpIface = service.getExtensionLocHidlIzatOsNpGlue();
                    } catch (RemoteException e) {
                    }
                }
            }
        }

        @Override
        public void onServiceDied() {
            mOsNpIface = null;
            getOsNpIface();
            try {
                mOsNpIface.setCallback(mOsNpCb);
            } catch (RemoteException e) {
            }
        }

        public class OsNpCallback extends ILocHidlIzatOsNpGlueCallback.Stub {
            private NpProxy mNpProxy;

            public OsNpCallback(NpProxy npProxy) {
                mNpProxy = npProxy;
            }

            @Override
            public void onSetRequest(LocHidlIzatRequest request) {
                HidlClientUtils.fromHidlService(TAG);
                long interval = request.timeIntervalBetweenFixes;
                float smallestDisplacement = request.smallestDistanceBetweenFixes;
                int numUpdates = request.numUpdates;
                mNpProxy.onSetRequest(interval, smallestDisplacement, numUpdates);
            }

            @Override
            public void onStopRequest() {
                HidlClientUtils.fromHidlService(TAG);
                mNpProxy.onStopRequest();
            }
        }

        private void locationChanged(boolean hasTime, long time,
                                    boolean hasTimeNanos, long timeNanos,
                                    boolean hasLatitude, double latitude,
                                    boolean hasLongitude, double longitude,
                                    boolean hasAccuracy, float accuracy,
                                    boolean hasVerticalAccuracy, float verticalAccuracy,
                                    boolean hasAltitude, double altitude,
                                    boolean hasBearing, float bearing,
                                    boolean hasSpeed, float speed,
                                    boolean hasSource, int source) {
            LocHidlIzatLocation location = new LocHidlIzatLocation();
            location.hasUtcTimestampInMsec = hasTime;
            location.hasElapsedRealTimeInNanoSecs = hasTimeNanos;
            location.hasLatitude = hasLatitude;
            location.hasLongitude = hasLongitude;
            location.hasHorizontalAccuracy = hasAccuracy;
            location.hasAltitudeWrtEllipsoid = hasAltitude;
            location.hasBearing = hasBearing;
            location.hasSpeed = hasSpeed;
            location.hasNetworkPositionSource = hasSource;
            location.hasVertUnc = hasVerticalAccuracy;

            location.utcTimestampInMsec = time;
            location.elapsedRealTimeInNanoSecs = timeNanos;
            location.latitude = latitude;
            location.longitude = longitude;
            location.horizontalAccuracy = accuracy;
            location.altitudeWrtEllipsoid = altitude;
            location.bearing = bearing;
            location.speed = speed;
            location.vertUnc = verticalAccuracy;
            location.networkPositionSource = source;
            if (null != mOsNpIface) {
                try {
                    HidlClientUtils.toHidlService(TAG);
                    mOsNpIface.locationChanged(location);
                } catch (RemoteException e) {
                }
            }
        }
    }
}
