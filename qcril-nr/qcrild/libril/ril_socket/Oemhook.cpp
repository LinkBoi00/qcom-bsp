/******************************************************************************
#  Copyright (c) 2020 Qualcomm Technologies, Inc.
#  All Rights Reserved.
#  Confidential and Proprietary - Qualcomm Technologies, Inc.
#******************************************************************************/

#include <ril_socket_api.h>
#include <interfaces/voice/QcRilRequestRejectIncomingCallMessage.h>
#include <Marshal.h>
#include "qcril_qmi_oem_reqlist.h"
#include "qcril_qmi_oemhook_utils.h"
#undef TAG
#define TAG "RILQ"

namespace ril {
namespace socket {
namespace api {

RIL_Errno rejectIncomingCallWithCause21(std::shared_ptr<SocketRequestContext> context,
                                                uint8_t * /*data*/, uint32_t /*dataLen*/) {
  RIL_Errno result = RIL_E_NO_MEMORY;
  QCRIL_LOG_FUNC_ENTRY();
  auto msg = std::make_shared<QcRilRequestRejectIncomingCallMessage>(context);
  if (msg) {
    GenericCallback<QcRilRequestMessageCallbackPayload> cb(
        ([context](std::shared_ptr<Message> /*msg*/, Message::Callback::Status status,
                        std::shared_ptr<QcRilRequestMessageCallbackPayload> resp) -> void {
          RIL_Errno errorCode = RIL_E_GENERIC_FAILURE;
          if (status == Message::Callback::Status::SUCCESS && resp != nullptr) {
            errorCode = resp->errorCode;
          }
          sendResponse(context, errorCode, nullptr);
        }));
    msg->setCallback(&cb);
    msg->dispatch();
    result = RIL_E_SUCCESS;
  }
  QCRIL_LOG_FUNC_RETURN();
  return result;
}

/*
 * Oemhook Request has the following message format
 * [OEM_NAME(8)] [Request Id (4)] [Payload Length (4)] [Payload]
 */
void dispatchOemhookRaw(std::shared_ptr<SocketRequestContext> context, Marshal& p) {
  QCRIL_LOG_FUNC_ENTRY();
  RIL_Errno errorCode = RIL_E_SUCCESS;
  do {
    char *data = nullptr;
    size_t dataLen = 0;
    if (p.dataAvail()) {
      p.readAndAlloc(data, dataLen);
    }
    QCRIL_LOG_INFO("data length=%d", dataLen);
    if (data == nullptr ||
        dataLen < (QCRIL_OTHER_OEM_NAME_LENGTH + QCRIL_OTHER_OEM_REQUEST_ID_LEN)) {
      QCRIL_LOG_INFO("Invalid parameters; invalid data length");
      errorCode = RIL_E_INVALID_ARGUMENTS;
      break;
    }
    uint32_t cmd_id = 0;
    uint32_t reqLen = 0;
    uint8_t *reqData = nullptr;
    if (dataLen > QCRIL_HOOK_HEADER_SIZE) {
      memcpy(&reqLen, &data[QCRIL_OTHER_OEM_NAME_LENGTH + QCRIL_OTHER_OEM_REQUEST_ID_LEN],
             QCRIL_OTHER_OEM_REQUEST_DATA_LEN);
      reqData = (uint8_t*)data + QCRIL_HOOK_HEADER_SIZE;
    }
    memcpy(&cmd_id, &data[QCRIL_OTHER_OEM_NAME_LENGTH], QCRIL_OTHER_OEM_REQUEST_ID_LEN);

    switch (cmd_id) {
      case QCRIL_REQ_HOOK_REJECT_INCOMING_CALL_CAUSE_21:
        errorCode = rejectIncomingCallWithCause21(context, reqData, reqLen);
        break;
      default:
        errorCode = RIL_E_REQUEST_NOT_SUPPORTED;
        break;
    }
  } while (FALSE);
  if (errorCode != RIL_E_SUCCESS) {
    sendResponse(context, errorCode, nullptr);
  }
  QCRIL_LOG_FUNC_RETURN();
}

}  // namespace api
}  // namespace socket
}  // namespace ril
