/*
 * Copyright (c) 2020 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#include <telephony/ril.h>
#include <marshal/OperatorInfo.h>

template <>
Marshal::Result Marshal::write<RIL_OperatorInfo>(const RIL_OperatorInfo& arg) {
  int32_t longPresent = 0;
  int32_t shortPresent = 0;
  int32_t numericPresent = 0;
  if(arg.alphaLong) {
    longPresent = 1;
    write(longPresent);
    write(arg.alphaLong);
  } else {
    write(longPresent);
  }
  if(arg.alphaShort) {
    shortPresent = 1;
    write(shortPresent);
    write(arg.alphaShort);
  } else {
    write(shortPresent);
  }
  if(arg.operatorNumeric) {
    numericPresent = 1;
    write(numericPresent);
    write(arg.operatorNumeric);
  } else {
    write(numericPresent);
  }
  write(static_cast<uint32_t>(arg.status));
  return Result::SUCCESS;
}

template <>
Marshal::Result Marshal::read<RIL_OperatorInfo>(RIL_OperatorInfo& arg) const {
  int32_t longPresent = 0;
  int32_t shortPresent = 0;
  int32_t numericPresent = 0;

  read(longPresent);
  if(longPresent) {
    read(arg.alphaLong);
  }
  read(shortPresent);
  if(shortPresent) {
    read(arg.alphaShort);
  }
  read(numericPresent);
  if(numericPresent) {
    read(arg.operatorNumeric);
  }
  read(*reinterpret_cast<uint32_t*>(&arg.status));
  return Result::SUCCESS;
}
