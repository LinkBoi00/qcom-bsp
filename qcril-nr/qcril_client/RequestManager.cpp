/******************************************************************************
#  Copyright (c) 2019 Qualcomm Technologies, Inc.
#  All Rights Reserved.
#  Confidential and Proprietary - Qualcomm Technologies, Inc.
#******************************************************************************/

#include <unistd.h>
#include <errno.h>
#include <poll.h>
#include <arpa/inet.h>
#include <framework/Log.h>
#include <record_stream.h>
#include <RequestManager.hpp>

#define TAG "[RequestManager]"

RequestManager::RequestManager(int socketFd) : socketFd(socketFd) {
    requestListener = std::thread([this] { this->listenForRequests(); });
    responseListener = std::thread([this] { this->listenForResponses(); });
}

RequestManager::~RequestManager() {
    // TODO: Gracefully handle pending requests, responses
    // TODO: Shutdown request/response listener threads
}

void RequestManager::listenForRequests() {
    Log::getInstance().d("[RequestManager]: Starting loop to listen for requests.");

    while (true) {
        Request req;
        GenericResponseCallback* cb = nullptr;

        {
            std::unique_lock<std::mutex> lock(requestsWaitingToBeIssuedMutex);
            requestNotifier.wait(lock, [this] { return !(this->requestsWaitingToBeIssued).empty(); });
            // TODO: make sure the requestsWaitingToBeIssuedMutex is acquired at this point
            Log::getInstance().d("[RequestManager]: Got one or more requests. Removing request at the front of the queue.");
            req = std::move(requestsWaitingToBeIssued.front());
            cb = std::get<2>(req).get();
            requestsWaitingToBeIssued.pop();
        }

        {
            std::lock_guard<std::mutex> lock(requestsWaitingForResponseMutex);
            requestsWaitingForResponse[cb] = std::move(std::get<2>(req));
        }

        Log::getInstance().d("[RequestManager]: Sending request to the server.");

        // TODO: copy all pending requests to a local queue and issue all of them
        if (sendRequestToServer(reinterpret_cast<uint64_t>(cb), std::get<0>(req), std::move(std::get<1>(req))) != Status::SUCCESS) {
            std::lock_guard<std::mutex> lock(requestsWaitingForResponseMutex);
            if (requestsWaitingForResponse[cb] != nullptr) {
                (*(requestsWaitingForResponse[cb]))(RIL_E_SYSTEM_ERR, nullptr);
                requestsWaitingForResponse.erase(cb);
            }
        }
    }
}

Status RequestManager::sendRequestToServer(uint64_t token, int32_t requestId, std::unique_ptr<Marshal> parcel) {
    if (socketFd < 0) {
        Log::getInstance().d("Socket FD not valid: " + std::to_string(socketFd));
        return Status::FAILURE;
    }

    // TODO: Introduce mutex to prevent concurrent writes to the socket

    // TODO: assert parcel size is not larger than the maximum 32-bit unsigned integer
    uint32_t bufferSize = htonl(static_cast<uint32_t>(sizeof(token) + sizeof(requestId) + (parcel ? parcel->dataSize() : 0)));
    Log::getInstance().d("[RequestManager]: Sending buffer of size "
                         + std::to_string(sizeof(token) + sizeof(requestId) + (parcel ? parcel->dataSize() : 0)));

    if (writeToSocket(socketFd, &bufferSize, sizeof(bufferSize)) < 0) {
        // TODO: return a more specific error
        Log::getInstance().d("[RequestManager]: Failed to transmit the size of the buffer over the socket.");
        return Status::FAILURE;
    }

    // write request ID
    if (writeToSocket(socketFd, &requestId, sizeof(requestId)) < 0) {
        // TODO: return a more specific error
        Log::getInstance().d("[RequestManager]: Failed to transmit request ID over the socket.");
        return Status::FAILURE;
    }

    // write token
    if (writeToSocket(socketFd, &token, sizeof(token)) < 0) {
        // TODO: return a more specific error
        Log::getInstance().d("[RequestManager]: Failed to transmit token over the socket.");
        return Status::FAILURE;
    }

    if (parcel) {
        // write request parameter parcel
        if (writeToSocket(socketFd, parcel->data(), parcel->dataSize()) < 0) {
            // TODO: return a more specific error
            Log::getInstance().d("[RequestManager]: Failed to transmit request parameter parcel over the socket.");
            return Status::FAILURE;
        }
    }

    return Status::SUCCESS;
}

void printBuffer(const void* buffer, size_t bufferLength, std::string& s) {
    char hex[4];
    for (int i = 0; i < bufferLength; i++) {
        std::snprintf(hex, sizeof(hex), "%-3.2hhX", *(static_cast<const uint8_t*>(buffer) + i));
        s += hex;
    }
}

ssize_t writeToSocket(int socketFd, const void* buffer, size_t bufferLength) {
    // TODO: Should writes to the socket timeout?
    std::string s;
    printBuffer(buffer, bufferLength, s);
    Log::getInstance().d("[RequestManager]: Buffer contents: " + s);

    ssize_t totalBytesWritten = 0;
    while (totalBytesWritten < bufferLength) {
        ssize_t bytesWritten = write(socketFd, static_cast<const uint8_t*>(buffer) + totalBytesWritten,
                                     bufferLength - totalBytesWritten);
        if (bytesWritten < 0) {
            Log::getInstance().d("[RequestManager]: Failed to write to the socket: " + std::string(strerror(errno)));
            return bytesWritten;
        }
        Log::getInstance().d("[RequestManager]: Wrote " + std::to_string(bytesWritten) + " bytes to the socket.");
        totalBytesWritten += bytesWritten;
    }

    return totalBytesWritten;
}

void RequestManager::listenForResponses() {
    Log::getInstance().d("[RequestManager]: Starting loop to listen for responses.");

    // Start polling on sockets and exit socket
    struct pollfd fds;
    fds.fd = socketFd;
    fds.events = POLLIN;

    std::unique_ptr<RecordStream, void (*)(RecordStream *)> rs(
        record_stream_new(socketFd, MAX_COMMAND_BYTES), record_stream_free);

    int ret;
    void *record;
    size_t recordLen;

    while (true) {
        // revents is set by the poll() implementation to signify which events caused the poll to return
        fds.revents = 0;

        ret = poll(&fds, 1, -1);

        if (ret < 0) {
            Log::getInstance().d("Error in polling socket, exiting " + std::to_string(ret));
            break;
        }

        if (fds.revents & (POLLERR | POLLHUP | POLLNVAL)) {
            Log::getInstance().d("Error in poll, exiting listener thread");
            break;
        }

        if (fds.revents != POLLIN) {
            continue;
        }

        int errsv = 0;

        while (true) {
            errno = 0;
            ret = record_stream_get_next(rs.get(), &record, &recordLen);
            errsv = errno;

            Log::getInstance().d("record_stream_get_next: recordLen: " + std::to_string(recordLen)
                                 + ", ret: " + std::to_string(ret)
                                 + ", errno: " + std::to_string(ret ? errsv : 0)
                                 + ", errStr: " + strerror(errsv)
                                 + ", socket fd: " + std::to_string(socketFd));

            if(ret == 0 && record == nullptr) {
                // EOS - Done reading data from socket, client has disconnected
                break;
            } else if(ret < 0 && errsv != EAGAIN) {
                // error condition
                break;
            } else if(ret < 0 && errsv == EAGAIN) {
                // client has not finished writing to socket, wait for more data before processing
                break;
            } else if(ret == 0) {
                if(recordLen > MAX_COMMAND_BYTES) {
                    ret = -1;
                    Log::getInstance().d("Received parcel is too big to be valid; not handling");
                    break;
                } else {
                    Log::getInstance().d("Got a response parcel");
                    std::shared_ptr<Marshal> p = std::make_shared<Marshal>();
                    // TODO: check if we could end up with a dangling pointer inside
                    // parcel if this thread exits before RIL has a chance to process parcel
                    // (because that will destroy the RecordStream instance and thus free "record")
                    if (p) {
                        p->setData(std::string(reinterpret_cast<const char*>(record), recordLen));

                        uint32_t responseType;
                        p->read(responseType);
                        //TODO: Use macro RESPONSE_SOLICITED instead of 0
                        if (responseType == 0) {
                            // TODO: Handle response in a separate thread?
                            handleResponse(p);
                        } else if (responseType == 1) {
                            handleIndication(p);
                        }
                    }
                }
            }
        }

        // Fatal error or end-of-stream
        if (ret == 0 || !(errsv == EAGAIN || errsv == EINTR)) {
            if(ret != 0) {
                Log::getInstance().d("Error on reading socket recordLen: " + std::to_string(recordLen)
                                     + ", ret: " + std::to_string(ret) +
                                     + ", errno: " + std::to_string(ret ? errsv : 0)
                                     + ", errStr: " + strerror(errsv)
                                     + ", socket fd:" + std::to_string(socketFd));
            } else {
                Log::getInstance().d("EOS on socket recordLen: " + std::to_string(recordLen)
                                     + ", ret: " + std::to_string(ret)
                                     + ", errno: " + std::to_string(ret ? errsv : 0)
                                     + ", errStr: " + strerror(errsv)
                                     + ", socket fd: " + std::to_string(socketFd));
            }
            break;
        }
    }
}

Status RequestManager::handleResponse(std::shared_ptr<Marshal> p) {
    if (p == nullptr) {
        return Status::FAILURE;
    }

    // TODO: static assert sizeof(uint64_t) == sizeof(GenericResponseCallback*)
    // TODO: check for errors during unmarshalling
    uint64_t token;
    uint32_t errorCode;
    p->read(token);
    p->read(errorCode);

    std::unique_ptr<GenericResponseCallback> cb;

    {
        std::lock_guard<std::mutex> lock(requestsWaitingForResponseMutex);

        auto iter = requestsWaitingForResponse.find(reinterpret_cast<GenericResponseCallback*>(token));
        if (iter == requestsWaitingForResponse.end()) {
            return Status::FAILURE;
        } else {
            if (iter->second == nullptr) {
                return Status::FAILURE;
            } else {
                cb = std::move(iter->second);
                //(*(iter->second))(static_cast<RIL_Errno>(errorCode), p);
            }
            requestsWaitingForResponse.erase(iter);
        }
    }

    if (cb != nullptr) {
        (*cb)(static_cast<RIL_Errno>(errorCode), p);
    }

    return Status::SUCCESS;
}

Status RequestManager::handleIndication(std::shared_ptr<Marshal> p) {
    if (p == nullptr) {
        return Status::FAILURE;
    }

    // TODO: check for errors during unmarshalling
    uint32_t indicationId;
    p->read(indicationId);

    GenericIndicationHandler indicationHandler;

    {
        std::lock_guard<std::mutex> lock(indicationHandlersMutex);
        auto iter = indicationHandlers.find(indicationId);
        if (iter == indicationHandlers.end()) {
            return Status::FAILURE;
        } else {
            if (iter->second == nullptr) {
                return Status::FAILURE;
            } else {
                //GenericIndicationHandler& indHandler = *(iter->second);
                //indicationHandler = indHandler;
                indicationHandler = *(iter->second);
            }
        }
    }

    if (indicationHandler) {
        indicationHandler(p);
    }

    return Status::SUCCESS;
}

Status RequestManager::issueRequest(int32_t requestId, std::unique_ptr<Marshal> p, const GenericResponseCallback& cb) {
    Log::getInstance().d("[RequestManager]: Pushing a request to the queue.");

    std::unique_ptr<GenericResponseCallback> cbCopy = std::make_unique<GenericResponseCallback>(cb);
    if (cbCopy == nullptr) {
        return Status::FAILURE;
    }

    {
        std::lock_guard<std::mutex> lock(requestsWaitingToBeIssuedMutex);
        requestsWaitingToBeIssued.push(std::make_tuple(requestId, std::move(p), std::move(cbCopy)));
    }
    requestNotifier.notify_one();
    Log::getInstance().d("[RequestManager]: Pushed a request to the queue.");

    return Status::SUCCESS;
}

Status RequestManager::registerIndicationHandler(int32_t indicationId, const GenericIndicationHandler& indicationHandler) {
    Log::getInstance().d("[RequestManager]: Registering indication handler for indication " + std::to_string(indicationId));

    std::unique_ptr<GenericIndicationHandler> indicationHandlerCopy = std::make_unique<GenericIndicationHandler>(indicationHandler);
    if (indicationHandlerCopy == nullptr) {
        return Status::FAILURE;
    }

    {
        std::lock_guard<std::mutex> lock(indicationHandlersMutex);
        indicationHandlers[indicationId] = std::move(indicationHandlerCopy);
    }

    Log::getInstance().d("[RequestManager]: Registered indication handler for indication " + std::to_string(indicationId));
    return Status::SUCCESS;
}
