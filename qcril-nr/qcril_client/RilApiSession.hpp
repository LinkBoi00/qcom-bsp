/******************************************************************************
#  Copyright (c) 2019-2020 Qualcomm Technologies, Inc.
#  All Rights Reserved.
#  Confidential and Proprietary - Qualcomm Technologies, Inc.
#******************************************************************************/

#pragma once

#include <memory>
#include <telephony/ril.h>
#include <telephony/ril_ims.h>
#include <Status.hpp>
#include <RequestManager.hpp>

enum class RilInstance {
    FIRST = 0,
    SECOND = 1,
};

// TODO: Define response and indication callbacks in different namespaces
typedef std::function<void()> VoidIndicationHandler;
typedef std::function<void(const char *)> CharStarIndicationHandler;
typedef std::function<void(int)> IntIndicationHandler;
typedef std::function<void(RIL_Errno)> DialCallback;
typedef std::function<void(RIL_Errno, const RIL_CellInfo_v12[], size_t)> CellInfoCallback;
typedef std::function<void(const RIL_SignalStrength&)> SignalStrengthIndicationHandler;
typedef std::function<void(const RIL_CellInfo_v12[], size_t)> CellInfoIndicationHandler;
typedef std::function<void(const RIL_CDMA_SMS_Message&)> CdmaNewSmsIndicationHandler;

typedef std::function<void(RIL_Errno)> VoidResponseHandler;
typedef std::function<void(RIL_Errno, const char *, size_t)> CharStarSizeResponseHandler;
typedef std::function<void(RIL_Errno, RIL_VoiceRegistrationStateResponse)> GetVoiceRegCallback;
typedef std::function<void(RIL_Errno, RIL_DataRegistrationStateResponse)> GetDataRegCallback;
typedef std::function<void(RIL_Errno)> SetNetworkTypeCallback;
typedef std::function<void(RIL_Errno, int)> GetNetworkTypeCallback;
typedef std::function<void(RIL_Errno, const size_t numCalls, const RIL_Call **)> GetCurrentCallsCallback;
typedef std::function<void(RIL_Errno)> AnswerCallback;
typedef std::function<void(RIL_Errno)> ConferenceCallback;
typedef std::function<void(RIL_Errno)> SwitchWaitingOrHoldingAndActiveCallback;
typedef std::function<void(RIL_Errno)> UdubCallback;
typedef std::function<void(RIL_Errno)> HangupWaitingOrBackgroundCallback;
typedef std::function<void(RIL_Errno)> HangupForegroundResumeBackgroundCallback;
typedef std::function<void(RIL_Errno)> SeparateConnectionCallback;
typedef std::function<void(RIL_Errno, const int *)> QueryAvailableBandModeCallback;
typedef std::function<void(RIL_Errno, const RIL_SignalStrength *)> GetSignalStrengthCallback;
typedef std::function<void(RIL_Errno)> RadioPowerCallback;
typedef std::function<void(RIL_Errno)> AllowDataCallback;
typedef std::function<void(RIL_Errno)> EnableSimCallback;
typedef std::function<void(RIL_Errno, int)> GetEnableSimStatusCallback;
typedef std::function<void(RIL_Errno)> SetLocationUpdatesCallback;
typedef std::function<void(RIL_Errno, RIL_ActivityStatsInfo)> GetActivityInfoCallback;
typedef std::function<void(RIL_Errno, int)> VoiceRadioTechCallback;
typedef std::function<void(RIL_Errno, const char **)> DeviceIdentityCallback;
typedef std::function<void(RIL_Errno, const char **)> OperatorNameCallback;
typedef std::function<void(RIL_Errno, int)> QueryNetworkSelectionModeCallback;
typedef std::function<void(RIL_Errno)> SetNetworkSelectionAutomaticCallback;
typedef std::function<void(RIL_Errno)> SetNetworkSelectionManualCallback;
typedef std::function<void(RIL_Errno, const char *)> BasebandVersionCallback;
typedef std::function<void(RIL_Errno)> SetRadioCapabilityCallback;
typedef std::function<void(RIL_Errno, const RIL_RadioCapability&)> GetRadioCapabilityCallback;
typedef std::function<void(const RIL_RadioCapability&)> RadioCapabilityIndHandler;
typedef std::function<void(RIL_Errno)> ShutDownCallback;
typedef std::function<void(RIL_Errno)> ExitEmergencyCbModeCallback;
typedef std::function<void(RIL_Errno)> SetBandModeCallback;
typedef std::function<void(RIL_Errno)> HangupCallback;
typedef std::function<void(RIL_Errno)> SendUssdCallback;
typedef std::function<void(RIL_Errno)> CancelUssdCallback;
typedef std::function<void(RIL_Errno)> DtmfCallback;
typedef std::function<void(RIL_Errno)> DtmfStartCallback;
typedef std::function<void(RIL_Errno)> DtmfStopCallback;
typedef std::function<void(RIL_Errno, const RIL_LastCallFailCauseInfo&)> LastCallFailCauseCallback;

typedef std::function<void(RIL_Errno, const RIL_SMS_Response&)> SendSmsCallback;
typedef std::function<void(RIL_Errno)> AckSmsCallback;
typedef std::function<void(RIL_Errno, const char* addr)> GetSmscAddressCallback;
typedef std::function<void(RIL_Errno)> SetSmscAddressCallback;
typedef std::function<void(RIL_Errno, const size_t numCallFwdInfos, const RIL_CallForwardInfo**)>
    QueryCallForwardStatusCallback;
typedef std::function<void(RIL_Errno)> SetCallForwardStatusCallback;
typedef std::function<void(RIL_Errno, const int enabled, const int serviceClass)>
    QueryCallWaitingCallback;
typedef std::function<void(RIL_Errno)> SetCallCallWaitingCallback;
typedef std::function<void(RIL_Errno)> ChangeBarringPasswordCallback;
typedef std::function<void(RIL_Errno, const int status)> QueryClipCallback;
typedef std::function<void(RIL_Errno)> SetSuppSvcNotificationCallback;
typedef std::function<void(const uint8_t[], size_t)> Incoming3GppSmsIndicationHandler;
typedef std::function<void(const bool)> EnableSimStatusIndicationHandler;
typedef std::function<void(const RIL_CdmaSubscriptionSource)> CdmaSubscriptionSourceChangedIndicationHandler;
typedef std::function<void(const std::vector<uint8_t>&)> BroadcastSmsIndicationHandler;
typedef std::function<void(const std::string&)> ModemRestartIndicationHandler;
typedef std::function<void(int)> RilServerReadyIndicationHandler;
typedef std::function<void(const RIL_NetworkScanResult&)> NetworkScanIndicationHandler;
typedef std::function<void(const RIL_EmergencyList&)> EmergencyListIndicationHandler;
typedef std::function<void(RIL_Errno, int32_t)> WriteSmsToSimCallback;
typedef std::function<void(RIL_Errno)> DeleteSmsOnSimCallback;
typedef std::function<void(RIL_Errno)> ReportSmsMemoryStatusCallback;
typedef std::function<void(RIL_Errno)> SetCdmaSubscriptionSourceCallback;
typedef std::function<void(RIL_Errno, RIL_CdmaSubscriptionSource)> GetCdmaSubscriptionSourceCallback;
typedef std::function<void(RIL_Errno)> SetCdmaRoamingPreferenceCallback;
typedef std::function<void(RIL_Errno, RIL_CdmaRoamingPreference)> GetCdmaRoamingPreferenceCallback;
typedef std::function<void(RIL_Errno, const RIL_GSM_BroadcastSmsConfigInfo *, size_t)> GsmGetBroadcastSmsConfigCallback;
typedef std::function<void(RIL_Errno)> SetSignalStrengthReportingCriteriaCallback;
typedef std::function<void(RIL_Errno, const char *, size_t)> OemhookRawCallback;
typedef std::function<void(const char *, size_t)> OemhookIndicationHandler;

// IMS Requests
typedef std::function<void(RIL_Errno, const RIL_IMS_Registration&)> ImsGetRegistrationStateCallback;
typedef std::function<void(RIL_Errno, const size_t, const RIL_IMS_ServiceStatusInfo**)>
    ImsQueryServiceStatusCallback;
typedef std::function<void(RIL_Errno, const RIL_IMS_SubConfigInfo&)> ImsGetImsSubConfigCallback;
typedef std::function<void(RIL_Errno)> ImsRegistrationChangeCallback;
typedef std::function<void(RIL_Errno)> ImsSetServiceStatusCallback;
typedef std::function<void(RIL_Errno)> ImsDialCallback;
typedef std::function<void(RIL_Errno)> ImsAnswerCallback;
typedef std::function<void(RIL_Errno)> ImsHangupCallback;
typedef std::function<void(RIL_Errno)> ImsModifyCallInitiateCallback;
typedef std::function<void(RIL_Errno)> ImsModifyCallConfirmCallback;
typedef std::function<void(RIL_Errno)> ImsCancelModifyCallCallback;
typedef std::function<void(RIL_Errno, const RIL_IMS_SipErrorInfo&)> ImsAddParticipantCallback;
typedef std::function<void(RIL_Errno, const RIL_IMS_ConfigInfo&)> ImsSetImsConfigCallback;
typedef std::function<void(RIL_Errno, const RIL_IMS_ConfigInfo&)> ImsGetImsConfigCallback;
typedef std::function<void(RIL_Errno, const RIL_IMS_QueryCallForwardStatusInfo&)>
    ImsQueryCallForwardStatusCallback;
typedef std::function<void(RIL_Errno, const RIL_IMS_SetCallForwardStatusInfo&)>
    ImsSetCallForwardStatusCallback;
typedef std::function<void(RIL_Errno)> ImsExplicitCallTransferCallback;
typedef std::function<void(RIL_Errno, const RIL_IMS_ClirInfo&)> ImsGetClirCallback;
typedef std::function<void(RIL_Errno, const RIL_IMS_ClipInfo&)> ImsQueryClipCallback;
typedef std::function<void(RIL_Errno, const RIL_IMS_ServiceClassStatus&)>
    ImsSetSuppSvcNotificationCallback;
typedef std::function<void(RIL_Errno)> ImsSetClirCallback;
typedef std::function<void(RIL_Errno)> ImsDeflectCallCallback;
typedef std::function<void(RIL_Errno)> ImsSendUiTtyModeCallback;
typedef std::function<void(RIL_Errno, const RIL_IMS_SuppSvcResponse&)> ImsSuppSvcStatusCallback;
typedef std::function<void(RIL_Errno, const RIL_IMS_ColrInfo&)> ImsGetColrCallback;
typedef std::function<void(RIL_Errno, const RIL_IMS_SipErrorInfo&)> ImsSetColrCallback;
typedef std::function<void(RIL_Errno)> ImsRegisterMultiIdentityLinesCallback;
typedef std::function<void(RIL_Errno, const RIL_IMS_QueryVirtualLineInfoResponse&)>
        ImsQueryVirtualLineInfoCallback;
typedef std::function<void(RIL_Errno, const RIL_IMS_SendSmsResponse&)> ImsSendSmsCallback;
typedef std::function<void(RIL_Errno)> ImsAckSmsCallback;
typedef std::function<void(RIL_Errno, RIL_IMS_SmsFormat)> ImsGetSmsFormatCallback;
typedef std::function<void(RIL_Errno, const RIL_IMS_SipErrorInfo&)> ImsSetCallWaitingCallback;
typedef std::function<void(RIL_Errno, const RIL_IMS_QueryCallWaitingResponse&)> ImsQueryCallWaitingCallback;

typedef std::function<void(RIL_Errno)> AckCdmaSmsCallback;
typedef std::function<void(RIL_Errno, const RIL_CDMA_BroadcastSmsConfigInfo[], size_t)> CdmaGetBroadcastSmsConfigCallback;
typedef std::function<void(RIL_Errno)> CdmaSetBroadcastSmsConfigCallback;
typedef std::function<void(RIL_Errno)> CdmaSmsBroadcastActivationCallback;
typedef std::function<void(RIL_Errno, int32_t recordNumber)> WriteCdmaSmsToSimCallback;
typedef std::function<void(RIL_Errno)> DeleteCdmaSmsOnRuimCallback;
typedef std::function<void(const int&)> RingBackToneIndicationHandler;
typedef std::function<void()> ImsNetworkStateChangeIndicationHandler;
typedef std::function<void()> VoiceNetworkStateIndicationHandler;
typedef std::function<void(int)> VoiceRadioTechIndicationHandler;
typedef std::function<void(const char*)> NitzTimeIndicationHandler;
typedef std::function<void(RIL_RadioState)> RadioStateIndicationHandler;
typedef std::function<void(const int&)> RestrictedStateChangedIndicationHandler;
typedef std::function<void(const RIL_CDMA_SignalInfoRecord *)> CallRingIndicationHandler;
typedef std::function<void(const char& mode, const std::string& message)> OnUssdIndicationHandler;
typedef std::function<void(const RIL_SuppSvcNotification& suppSvc)>
    SuppSvcNotificationIndicationHandler;
typedef std::function<void(const RIL_CDMA_CallWaiting_v6& callWaitingRecord)>
    CdmaCallWaitingIndicationHandler;
typedef std::function<void(const RIL_CDMA_OTA_ProvisionStatus& status)>
    CdmaOtaProvisionStatusIndicationHandler;
typedef std::function<void(const RIL_CDMA_InformationRecords& records)> CdmaInfoRecIndicationHandler;
typedef std::function<void(const RIL_StkCcUnsolSsResponse& ss)> OnSupplementaryServiceIndicationHandler;
typedef std::function<void(const RIL_SrvccState& state)> SrvccStatusIndicationHandler;
typedef std::function<void(RIL_Errno)> ExplicitCallTransferCallback;
typedef std::function<void(RIL_Errno)> SetMuteCallback;
typedef std::function<void(RIL_Errno, int)> GetMuteCallback;
typedef std::function<void(RIL_Errno)> SetClirCallback;
typedef std::function<void(RIL_Errno, int, int)> GetClirCallback;
typedef std::function<void(RIL_Errno)> SendRttCallback;
typedef std::function<void(RIL_Errno)> GeolocationInfoCallback;
typedef std::function<void(RIL_Errno)> SetUnsolCellInfoListRateCallback;
typedef std::function<void()> CallStateChangeIndicationHandler;
typedef std::function<void(RIL_EmergencyCbMode)> EmergencyCbModeIndHandler;
typedef std::function<void(int)> UiccSubStatusChangedHandler;
typedef std::function<void(int)> CdmaPrlChangedHandler;
typedef std::function<void(const RIL_IMS_Registration&)> ImsRegistrationStateIndicationHandler;
typedef std::function<void(const size_t, const RIL_IMS_ServiceStatusInfo**)>
    ImsServiceStatusIndicationHandler;
typedef std::function<void(const RIL_IMS_SubConfigInfo&)> ImsSubConfigChangeIndicationHandler;
typedef std::function<void(const std::string&)> ImsRttMessageIndicationHandler;
typedef std::function<void(double, double)> ImsGeolocationIndicationHandler;
typedef std::function<void(const RIL_IMS_VowifiCallQuality &)> ImsVowifiQualityIndicationHandler;
typedef std::function<void()> ImsCallRingIndicationHandler;
typedef std::function<void(const RIL_IMS_ToneOperation&)> ImsRingBackToneIndicationHandler;
typedef std::function<void(RIL_IMS_CallInfo*, size_t count)> ImsCallStateChangedIndicationHandler;
typedef std::function<void()> ImsEnterEcbmIndicationHandler;
typedef std::function<void()> ImsExitEcbmIndicationHandler;
typedef std::function<void(const RIL_IMS_CallModifyInfo&)> ImsModifyCallIndicationHandler;
typedef std::function<void(const RIL_IMS_MessageWaitingInfo&)> ImsMessageWaitingIndicationHandler;
typedef std::function<void(const RIL_IMS_SuppSvcNotification&)> ImsSuppSvcNotificationIndicationHandler;
typedef std::function<void(const RIL_IMS_StkCcUnsolSsResponse&)> ImsOnSsIndicationHandler;
typedef std::function<void(const bool)> ImsVopsChangedIndicationHandler;
typedef std::function<void(const RIL_IMS_ParticipantStatusInfo&)>
    ImsParticipantStatusInfoIndicationHandler;
typedef std::function<void(const RIL_IMS_RegistrationBlockStatus&)>
    ImsRegistrationBlockStatusIndicationHandler;
typedef std::function<void(const uint8_t&)> ImsWfcRoamingModeConfigSupportIndicationHandler;
typedef std::function<void(const RIL_IMS_UssdInfo&)> ImsUssdFailedIndicationHandler;
typedef std::function<void(const RIL_IMS_VoiceInfoType&)> ImsVoiceInfoIndicationHandler;
typedef std::function<void(RIL_Errno, const RIL_IMS_SipErrorInfo&)> ImsRequestConferenceCallback;
typedef std::function<void(RIL_Errno, const RIL_IMS_SipErrorInfo&)> ImsRequestHoldCallback;
typedef std::function<void(RIL_Errno, const RIL_IMS_SipErrorInfo&)> ImsRequestResumeCallback;
typedef std::function<void(RIL_Errno)> ImsExitECBMCallback;
typedef std::function<void(const RIL_IMS_IncomingSms&)> ImsIncomingSmsHandler;
typedef std::function<void(const RIL_IMS_SmsStatusReport&)> ImsSmsStatusReportHandler;
typedef std::function<void(const std::vector<uint8_t>&)> ImsViceInfoHandler;
typedef std::function<void(RIL_IMS_TtyModeType)> ImsTtyNotificationHandler;
typedef std::function<void(const RIL_IMS_HandoverInfo&)> ImsHandoverIndicationHandler;
typedef std::function<void(const RIL_IMS_RefreshConferenceInfo&)>
    ImsRefreshConferenceInfoIndicationHandler;
typedef std::function<void(const RIL_IMS_MultiIdentityLineInfo*, size_t)>
    MultiIdentityRegistrationStatusHandler;
typedef std::function<void(const RIL_IMS_GeoLocationDataStatus&)>
    RetrievingGeoLocationDataStatusHandler;

typedef std::function<void(RIL_Errno, const Status, const RIL_Data_Call_Response_v11*)> SetupDataCallCallback;
typedef std::function<void(RIL_Errno)> DeactivateDataCallCallback;
typedef std::function<void(const RIL_Data_Call_Response_v11[], size_t)> DataCallListChangedIndicationHandler;
typedef std::function<void(RIL_Errno)> SetInitialAttachApnCallback ;
typedef std::function<void(RIL_Errno)> SetDataProfileCallback ;
typedef std::function<void(const RIL_Errno, const RIL_Data_Call_Response_v11*, const size_t)> GetDataCallListCallback;
typedef std::function<void(const RIL_Errno)> GetRilDataDumpCallback;
typedef std::function<void(RIL_Errno, const RIL_LceStatusInfo* )> StartLceDataCallback;
typedef std::function<void(RIL_Errno, const RIL_LceStatusInfo* )> StopLceDataCallback;
typedef std::function<void(RIL_Errno, const RIL_LceDataInfo* )> PullLceDataCallback;
typedef std::function<void(RIL_Errno, const int*)> SetLinkCapFilterCallback;
typedef std::function<void(const RIL_PCO_Data)> RILPCODataChangeIndicationHandler;
typedef std::function<void(const RIL_LinkCapacityEstimate)> RILLCEDataChangeIndicationHandler;
typedef std::function<void()> CarrierInfoForImsiHandler;
typedef std::function<void(const Status, const uint32_t, const RIL_KeepaliveStatusCode)> RILDataRadioKeepAliveHandler;
typedef std::function<void(const Status, const std::vector<RIL_PhysicalChannelConfig> )> PhysicalConfigStructHandlerHandler;
typedef std::function<void(const Status, const five_g_icon_type )> RILDataNrIconChangeIndicationHandler;
typedef std::function<void(RIL_Errno, const RIL_LinkCapCriteriaResult*)> SetLinkCapRptCriteriaCallback;
typedef std::function<void(RIL_Errno, const Status, const five_g_icon_type*)> GetDataNrIconTypeCallback;
typedef std::function<void(RIL_Errno, const Status, const RIL_ResponseError*)> SetPreferredDataModemCallback;
typedef std::function<void(RIL_Errno, const Status, const RIL_ResponseError*, const uint32_t*, const RIL_KeepaliveStatus_t*)> StartKeepAliveCallback;
typedef std::function<void(RIL_Errno, const Status, const RIL_ResponseError*)> StopKeepAliveCallback;
typedef std::function<void(const RIL_Errno, const Status, const RIL_Errno*)> CarrierInfoForImsiEncryptionCallback;

typedef std::function<void(const int&)> NewSmsOnSimIndicationHandler;
typedef std::function<void()> SimSmsStorageFullIndicationHandler;
typedef std::function<void(const char*)> NewSmsStatusReportIndicationHandler;
typedef std::function<void()> CdmaSmsRuimStorageFullIndicationHandler;
typedef std::function<void(RIL_Errno, const std::vector<RIL_OperatorInfo>&)> QueryAvailableNetworksCallback;

/* QCRIL_UIM Request Message Callbacks */
typedef std::function<void(RIL_Errno, const char *)> GetImsiReqCallback;
typedef std::function<void(RIL_Errno, const RIL_SIM_IO_Response *)> IOReqCallback;
typedef std::function<void(RIL_Errno, const RIL_CardStatus_v6*)> GetSimStatusReqCallback;
typedef std::function<void(RIL_Errno, const int *, size_t len)> OpenChannelReqCallback;
typedef std::function<void(RIL_Errno)> CloseChannelReqCallback;
typedef std::function<void(RIL_Errno, const RIL_SIM_IO_Response *)> TransmitApduBasicReqCallback;
typedef std::function<void(RIL_Errno, const RIL_SIM_IO_Response *)> TransmitApduChannelReqCallback;
typedef std::function<void(RIL_Errno, int *)> ChangePinReqCallback;
typedef std::function<void(RIL_Errno, int *)> ChangePin2ReqCallback;
typedef std::function<void(RIL_Errno, int *)> EnterPukReqCallback;
typedef std::function<void(RIL_Errno, int *)> EnterPuk2ReqCallback;
typedef std::function<void(RIL_Errno, int *)> EnterPinReqCallback;
typedef std::function<void(RIL_Errno, int *)> EnterPin2ReqCallback;
typedef std::function<void(RIL_Errno, int32_t *)> QueryFacilityLockReqCallback;
typedef std::function<void(RIL_Errno, int *)> SetFacilityLockReqCallback;
typedef std::function<void(RIL_Errno, const char *)> IsimAuthenticationReqCallback;
typedef std::function<void(RIL_Errno, const RIL_SIM_IO_Response *)> StkSendEnvelopeWithStatusReqCallback;
typedef std::function<void(RIL_Errno, const RIL_SIM_IO_Response *)> AuthenticationReqCallback;

typedef std::function<void(RIL_Errno)> SetTtyModeCallBack;
typedef std::function<void(RIL_Errno, const int&)> GetTtyModeCallBack;
typedef std::function<void(RIL_Errno)> SetCdmaVoicePrefModeCallBack;
typedef std::function<void(RIL_Errno, const int&)> GetCdmaVoicePrefModeCallBack;
typedef std::function<void(RIL_Errno)> SendCdmaFlashCallBack;
typedef std::function<void(RIL_Errno)> SendCdmaBurstDtmfCallBack;
typedef std::function<void(RIL_Errno, const bool&, const RIL_RadioTechnologyFamily&)> GetImsRegStateCallBack;

typedef std::function<void(RIL_Errno, const char*[5])> GetCdmaSubInfoCallback;
typedef std::function<void(RIL_Errno, RIL_AdnCountInfo *)> GetAdnRecordCallback;
typedef std::function<void(const RIL_SimRefreshResponse_v7 *)> SimRefreshIndicationHandler;

/* RilApiSession maintains state associated with an API session
 * opened by a client of RIL. */
class RilApiSession {
    public:
        RilApiSession(RilInstance i);
        ~RilApiSession();
        Status initialize();
        Status registerIndicationHandler(int32_t indicationId, const RequestManager::GenericIndicationHandler& indicationHandler);
        Status dial(const RIL_Dial& dialParams, const DialCallback& cb);
        Status getCellInfo(const CellInfoCallback& cb);
        Status registerSignalStrengthIndicationHandler(const SignalStrengthIndicationHandler& indicationHandler);
        Status registerCellInfoIndicationHandler(const CellInfoIndicationHandler& indicationHandler);
        Status registerCdmaNewSmsIndicationHandler(const CdmaNewSmsIndicationHandler& indicationHandler);

        Status setPreferredNetworkType(int nwType, const SetNetworkTypeCallback &cb);
        Status getPreferredNetworkType(const GetNetworkTypeCallback &cb);
        Status getCurrentCalls(const GetCurrentCallsCallback& cb);
        Status answer(const AnswerCallback& cb);
        Status conference(const ConferenceCallback& cb);
        Status switchWaitingOrHoldingAndActive(const SwitchWaitingOrHoldingAndActiveCallback& cb);
        Status udub(const UdubCallback& cb);
        Status hangupWaitingOrBackground(const HangupWaitingOrBackgroundCallback& cb);
        Status hangupForegroundResumeBackground(const HangupForegroundResumeBackgroundCallback& cb);
        Status separateConnection(int callId, const SeparateConnectionCallback& cb);
        Status setRadioCapability(const RIL_RadioCapability &radioCap, const SetRadioCapabilityCallback& cb);
        Status getRadioCapability(const GetRadioCapabilityCallback& cb);
        Status shutDown(const ShutDownCallback& cb);
        Status exitEmergencyCbMode(const ExitEmergencyCbModeCallback& cb);
        Status setBandMode(const int& bandMode, const SetBandModeCallback& cb);
        Status queryAvailableBandMode(const QueryAvailableBandModeCallback& cb);
        Status getSignalStrength(const GetSignalStrengthCallback& cb);
        Status VoiceRadioTech(const VoiceRadioTechCallback &cb);
        Status getVoiceRegStatus(const GetVoiceRegCallback& cb);
        Status getDataRegStatus(const GetDataRegCallback& cb);
        Status radioPower(const bool& on, const RadioPowerCallback& cb);
        Status enableSim(const bool& on, const EnableSimCallback& cb);
        Status getEnableSimStatus(const GetEnableSimStatusCallback& cb);
        Status allowData(const bool& on, const AllowDataCallback& cb);
        Status setLocationUpdates(const bool& on, const SetLocationUpdatesCallback& cb);
        Status getActivityInfo(const GetActivityInfoCallback& cb);
        Status deviceIdentity(const DeviceIdentityCallback &cb);
        Status operatorName(const OperatorNameCallback& cb);
        Status setIndicationFilter(int32_t filter, const VoidResponseHandler &cb);
        Status QueryNetworkSelectionMode(const QueryNetworkSelectionModeCallback &cb);
        Status setNetworkSelectionAutomatic(const SetNetworkSelectionAutomaticCallback &cb);
        Status setNetworkSelectionManual(std::string mcc, std::string mnc, const SetNetworkSelectionAutomaticCallback &cb);
        Status basebandVersion(const BasebandVersionCallback &cb);
        Status hangup(const int& connid, const HangupCallback& cb);
        Status sendUssd(const std::string& ussd, const SendUssdCallback& cb);
        Status cancelUssd(const CancelUssdCallback& cb);
        Status dtmf(const char& dtmf, const DtmfCallback& cb);
        Status dtmfStart(const char& dtmf, const DtmfStartCallback& cb);
        Status dtmfStop(const DtmfStopCallback& cb);
        Status lastCallFailCause(const LastCallFailCauseCallback& cb);
        Status explicitCallTransfer(const ExplicitCallTransferCallback& cb);
        Status setMute(const bool& mute, const SetMuteCallback& cb);
        Status getMute(const GetMuteCallback& cb);
        Status setClir(const int* clir, const SetClirCallback& cb);
        Status imsSendRttMessage(const char* msg, size_t len, const SendRttCallback& cb);
        Status imsSendGeolocationInfo( const RIL_IMS_GeolocationInfo& geolocationInfo,
                                       const GeolocationInfoCallback& cb);
        Status registerImsRttMessageIndicationHandler(const
                    ImsRttMessageIndicationHandler& indicationHandler);
        Status registerImsGeolocationIndicationHandler(const
                    ImsGeolocationIndicationHandler& indicationHandler);
        Status registerImsUnsolRetrievingGeoLocationDataStatus(
            const RetrievingGeoLocationDataStatusHandler& indicationHandler);
        Status registerImsVowifiQualityIndicationHandler(const
                    ImsVowifiQualityIndicationHandler& indicationHandler);
        Status getClir(const GetClirCallback& cb);
        Status queryCallForwardStatus(const RIL_CallForwardInfo& callFwdInfo,
                                      const QueryCallForwardStatusCallback& cb);
        Status setCallForward(const RIL_CallForwardInfo& callFwdInfo,
                              const SetCallForwardStatusCallback& cb);
        Status queryCallWaiting(const int& serviceClass, const QueryCallWaitingCallback& cb);
        Status setCallWaiting(const int& enabled, const int& serviceClass,
                              const SetCallCallWaitingCallback& cb);
        Status changeBarringPassword(const std::string& facility, const std::string& oldPassword,
                                     const std::string& newPassword,
                                     const ChangeBarringPasswordCallback& cb);
        Status queryClip(const QueryClipCallback& cb);
        Status setSuppSvcNotification(const int& enabled, const SetSuppSvcNotificationCallback& cb);
        Status setUnsolCellInfoListRate(const int32_t& mRate, const SetUnsolCellInfoListRateCallback& cb);
        Status sendSms(const std::string& message, const std::string& smscAddress, bool expectMore, const SendSmsCallback& cb);
        Status ackSms(const RIL_GsmSmsAck ack, const AckSmsCallback& cb);
        Status getSmscAddress(const GetSmscAddressCallback& cb);
        Status setSmscAddress(const std::string& addr, const SetSmscAddressCallback& cb);
        Status sendCdmaSms(const RIL_CDMA_SMS_Message& smsParams, const SendSmsCallback& cb);
        Status ackCdmaSms(const RIL_CDMA_SMS_Ack& ackParams, const AckCdmaSmsCallback& cb);
        Status getCdmaSmsBroadcastConfig(const CdmaGetBroadcastSmsConfigCallback& cb);
        Status setCdmaSmsBroadcastConfig(const RIL_CDMA_BroadcastSmsConfigInfo cfgParams[], size_t size, const CdmaSetBroadcastSmsConfigCallback& cb);
        Status setCdmaSmsBroadcastActivation(const int32_t value, const CdmaSmsBroadcastActivationCallback& cb);
        Status writeCdmaSmsToRuim(const RIL_CDMA_SMS_WriteArgs arg, const WriteCdmaSmsToSimCallback& cb);
        Status deleteCdmaSmsOnRuim(int32_t index, const DeleteCdmaSmsOnRuimCallback& cb);
        Status getCdmaSubscription(const GetCdmaSubInfoCallback& cb);

        Status writeSmsToSim(const std::string& pdu, const std::string& smscAddress, const int status, const WriteSmsToSimCallback& cb);
        Status deleteSmsOnSim(int32_t recordIndex, const DeleteSmsOnSimCallback& cb);
        Status reportSmsMemoryStatus(bool storageSpaceAvailable, const ReportSmsMemoryStatusCallback& cb);
        Status setCdmaSubscriptionSource(RIL_CdmaSubscriptionSource subscriptionSource, const SetCdmaSubscriptionSourceCallback& cb);
        Status getCdmaSubscriptionSource(const GetCdmaSubscriptionSourceCallback& cb);
        Status setCdmaRoamingPreference(RIL_CdmaRoamingPreference roamingPref, const SetCdmaRoamingPreferenceCallback& cb);
        Status getCdmaRoamingPreference(const GetCdmaRoamingPreferenceCallback& cb);
        Status setSignalStrengthReportingCriteria(const RIL_SignalStrengthReportingCriteria& criteria,
                const SetSignalStrengthReportingCriteriaCallback& cb);
        Status oemhookRaw(const char *data, size_t dataLen, const OemhookRawCallback& cb);

        Status registerUnsolCallStateChangeIndicationHandler(const CallStateChangeIndicationHandler& indicationHandler);
        Status registerRingBackToneIndicationHandler(const RingBackToneIndicationHandler& indicationHandler);
        Status registerImsNetworkStateChangeIndicationHandler(const ImsNetworkStateChangeIndicationHandler& indicationHandler);
        Status registerVoiceNetworkStateIndicationHandler(const VoiceNetworkStateIndicationHandler& indicationHandler);
        Status registerVoiceRadioTechIndicationHandler(const VoiceRadioTechIndicationHandler& indicationHandler);
        Status registerNitzTimeIndicationHandler(const NitzTimeIndicationHandler& indicationHandler);
        Status registerRadioStateIndicationHandler(const RadioStateIndicationHandler& indicationHandler);
        Status registerRestrictedStateChangedIndicationHandler(
                const RestrictedStateChangedIndicationHandler& indicationHandler);
        Status registerCallRingIndicationHandler(const CallRingIndicationHandler& indicationHandler);
        Status registerEmergencyCbModeIndHandler(const EmergencyCbModeIndHandler& indicationHandler);
        Status registerUiccSubStatusIndHandler(const UiccSubStatusChangedHandler& indicationHandler);
        Status registerCdmaPrlChangedIndHandler(const CdmaPrlChangedHandler& indicationHandler);
        Status registerRadioCapabilityChangedIndHandler(
            const RadioCapabilityIndHandler& indicationHandler);
        Status registerStkUnsolSessionEnd(
                const VoidIndicationHandler &indicationHandler);
        Status registerStkUnsolProactiveCommand(
                const CharStarIndicationHandler &indicationHandler);
        Status registerStkEventNotify(
                const CharStarIndicationHandler &indicationHandler);
        Status registerStkCallSetup(
                const IntIndicationHandler &indicationHandler);
        Status registerOnUssdIndicationHandler(const OnUssdIndicationHandler& indicationHandler);
        Status registerSuppSvcNotificationIndicationHandler(
            const SuppSvcNotificationIndicationHandler& indicationHandler);
        Status registerCdmaCallWaitingIndicationHandler(
            const CdmaCallWaitingIndicationHandler& indicationHandler);
        Status registerCdmaOtaProvisionStatusIndicationHandler(
            const CdmaOtaProvisionStatusIndicationHandler& indicationHandler);
        Status registerCdmaInfoRecIndicationHandler(
            const CdmaInfoRecIndicationHandler& indicationHandler);
        Status registerOnSupplementaryServiceIndicationHandler(
            const OnSupplementaryServiceIndicationHandler& indicationHandler);
        Status registerSrvccStatusIndicationHandler(
            const SrvccStatusIndicationHandler& indicationHandler);
        Status registerIncoming3GppSmsIndicationHandler(
            const Incoming3GppSmsIndicationHandler& indicationHandler);
        Status registerEnableSimStatusIndicationHandler(
            const EnableSimStatusIndicationHandler& indicationHandler);
        Status registerCdmaSubscriptionSourceChangedIndicationHandler(
            const CdmaSubscriptionSourceChangedIndicationHandler& indicationHandler);
        Status registerBroadcastSmsIndicationHandler(
            const BroadcastSmsIndicationHandler& indicationHandler);
        Status registerModemRestartIndicationHandler(
            const ModemRestartIndicationHandler& indicationHandler);
        Status registerRilServerReadyIndicationHandler(
            const RilServerReadyIndicationHandler& indicationHandler);
        Status registerOemhookIndicationHandler(
            const OemhookIndicationHandler& indicationHandler);

        Status setupDataCall(const RIL_RadioAccessNetworks accessNetwork, const RIL_DataProfileInfo_IRadio_1_4& dataProfile,  const bool roamingAllowed, const RIL_RadioDataRequestReasons reason,const SetupDataCallCallback& cb);
        Status setupDataCall(const RIL_SetUpDataCallParams &callParams, const SetupDataCallCallback& cb);
        Status deactivateDataCall(const int32_t cid, const bool reason, const DeactivateDataCallCallback& cb);
        Status registerDataCallListChangedIndicationHandler(const DataCallListChangedIndicationHandler& indicationHandler);
        Status setInitialAttachApn(const RIL_DataProfileInfo_IRadio_1_4& dataProfile, const SetInitialAttachApnCallback &cb);
        Status setDataProfile(const RIL_DataProfileInfo_IRadio_1_4* dataProfile, const uint32_t size, const SetDataProfileCallback &cb);
        Status getDataCallList(const GetDataCallListCallback &cb);
        Status StartLceData(const int32_t interval, const int32_t mode, const StartLceDataCallback &cb);
        Status StopLceData(const StopLceDataCallback &cb);
        Status PullLceData(const PullLceDataCallback &cb);
        Status SetLinkCapFilter(const RIL_ReportFilter enable_bit, const SetLinkCapFilterCallback &cb);
        Status SetLinkCapRptCriteria(const RIL_LinkCapCriteria criteria, const SetLinkCapRptCriteriaCallback &cb);
        Status GetDataNrIconType(const GetDataNrIconTypeCallback &cb);
        Status registerPcoDataChangeHandler(const RILPCODataChangeIndicationHandler &indicationHandler);
        Status registerLCEDataChangeHandler(const RILLCEDataChangeIndicationHandler &indicationHandler);
        Status registerDataNrIconChangeHandler(const RILDataNrIconChangeIndicationHandler &indicationHandler);
        Status registerCarrierInfoForImsiHandler(const CarrierInfoForImsiHandler &indicationHandler);
        Status SetPreferredDataModem(const int32_t modemId, const SetPreferredDataModemCallback &cb);
        Status StartKeepAlive(const RIL_KeepaliveRequest request, const StartKeepAliveCallback &cb);
        Status registerRadioKeepAliveHandler(const RILDataRadioKeepAliveHandler &indicationHandler);
        Status registerPhysicalConfigStructHandler(const PhysicalConfigStructHandlerHandler &indicationHandler);
        Status StopKeepAlive(const int32_t handle, const StopKeepAliveCallback &cb);
        Status CarrierInfoForImsiEncryption(const RIL_CarrierInfoForImsiEncryption carrier, const RIL_PublicKeyType type,
        const CarrierInfoForImsiEncryptionCallback &cb);
        Status captureRilDataDump(const GetRilDataDumpCallback &cb);
        Status setTtyMode(const int mode, const SetTtyModeCallBack& cb);
        Status getTtyMode(const GetTtyModeCallBack& cb);
        Status setCdmaVoicePrefMode(const int mode, const SetCdmaVoicePrefModeCallBack& cb);
        Status getCdmaVoicePrefMode(const GetCdmaVoicePrefModeCallBack& cb);
        Status sendCdmaFlash(const std::string& flash, const SendCdmaFlashCallBack& cb);
        Status sendCdmaBurstDtmf(const std::string& dtmf, int32_t on, int32_t off,
                                 const SendCdmaBurstDtmfCallBack& cb);
        Status getImsRegState(const GetImsRegStateCallBack& cb);

        Status registerNewSmsOnSimIndicationHandler(
            const NewSmsOnSimIndicationHandler& indicationHandler);
        Status registerSimSmsStorageFullIndicationHandler(
            const SimSmsStorageFullIndicationHandler& indicationHandler);
        Status registerNewSmsStatusReportIndicationHandler(
            const NewSmsStatusReportIndicationHandler& indicationHandler);
        Status registerCdmaSmsRuimStorageFullIndicationHandler(
            const CdmaSmsRuimStorageFullIndicationHandler& indicationHandler);
        Status queryAvailableNetworks(const QueryAvailableNetworksCallback& cb);
        Status startNetworkScan(const RIL_NetworkScanRequest& req, const VoidResponseHandler& cb);
        Status stopNetworkScan(const VoidResponseHandler& cb);
        Status setSystemSelectionChannels(const RIL_SysSelChannels& req, const VoidResponseHandler& cb);
        Status registerNetworksScanIndicationHandler(
            const NetworkScanIndicationHandler& indicationHandler);
        Status registerEmergencyListIndicationHandler(
            const EmergencyListIndicationHandler& indicationHandler);

        Status GsmGetBroadcastSmsConfig(const GsmGetBroadcastSmsConfigCallback& cb);
        Status GsmSetBroadcastSMSConfig(RIL_GSM_BroadcastSmsConfigInfo *config, size_t len,
                const VoidResponseHandler &cb);
        Status GsmSmsBroadcastActivation(uint32_t disable, const VoidResponseHandler &cb);
        Status ReportStkServiceIsRunning(const VoidResponseHandler &cb);
        Status GetAtr(uint8_t slot, const CharStarSizeResponseHandler &cb);
        Status StkHandleCallSetupRequestedFromSim(int32_t accept, const VoidResponseHandler &cb);
        Status StkSendEnvelopeCommand(const char *command, size_t size, const CharStarSizeResponseHandler &cb);
        Status StkSendTerminalResponse(const char *terminalResponse, size_t size, const VoidResponseHandler &cb);
        // IMS APIs
        Status imsGetRegistrationState(const ImsGetRegistrationStateCallback& cb);
        Status imsQueryServiceStatus(const ImsQueryServiceStatusCallback& cb);
        Status imsRegistrationChange(const RIL_IMS_RegistrationState& state,
                                     const ImsRegistrationChangeCallback& cb);
        Status imsSetServiceStatus(const size_t, const RIL_IMS_ServiceStatusInfo **,
                                   const ImsSetServiceStatusCallback& cb);
        Status imsGetImsSubConfig(const ImsGetImsSubConfigCallback& cb);
        Status imsDial(const RIL_IMS_Dial& dial, const ImsDialCallback& cb);
        Status imsAnswer(const RIL_IMS_Answer& answer, const ImsAnswerCallback& cb);
        Status imsHangup(const RIL_IMS_Hangup& hangup, const ImsHangupCallback& cb);
        Status imsDtmf(char digit, const VoidResponseHandler &cb);
        Status imsDtmfStart(char digit, const VoidResponseHandler &cb);
        Status imsDtmfStop(const VoidResponseHandler &cb);
        Status imsModifyCallInitiate(const RIL_IMS_CallModifyInfo& modifyInfo,
                                     const ImsModifyCallInitiateCallback& cb);
        Status imsModifyCallConfirm(const RIL_IMS_CallModifyInfo& modifyInfo,
                                    const ImsModifyCallConfirmCallback& cb);
        Status imsCancelModifyCall(const uint32_t& callId, const ImsCancelModifyCallCallback& cb);
        Status imsAddParticipant(const char* address, const ImsAddParticipantCallback& cb);
        Status imsSetImsConfig(const RIL_IMS_ConfigInfo& config, const ImsSetImsConfigCallback& cb);
        Status imsGetImsConfig(const RIL_IMS_ConfigInfo& config, const ImsGetImsConfigCallback& cb);
        Status imsQueryCallForwardStatus(const RIL_IMS_CallForwardInfo& cfInfo,
                                         const ImsQueryCallForwardStatusCallback& cb);
        Status imsSetCallForwardStatus(const RIL_IMS_CallForwardInfo& cfInfo,
                                       const ImsSetCallForwardStatusCallback& cb);
        Status imsExplicitCallTransfer(const RIL_IMS_ExplicitCallTransfer& explicitCall,
                                       const ImsExplicitCallTransferCallback& cb);
        Status imsGetClir(const ImsGetClirCallback& cb);
        Status imsSetClir(const RIL_IMS_SetClirInfo& clirInfo,
                          const ImsSetClirCallback& cb);
        Status imsQueryClip(const ImsQueryClipCallback& cb);
        Status imsSetSuppSvcNotification(const RIL_IMS_ServiceClassStatus& serviceStatus,
                                         const ImsSetSuppSvcNotificationCallback& cb);
        Status imsDeflectCall(const RIL_IMS_DeflectCallInfo& deflectCall,
                              const ImsDeflectCallCallback& cb);
        Status imsSendUiTtyMode(const RIL_IMS_TtyNotifyInfo& ttyInfo,
                                const ImsSendUiTtyModeCallback& cb);
        Status imsSuppSvcStatus(const RIL_IMS_SuppSvcRequest& suppSvc,
                                const ImsSuppSvcStatusCallback& cb);
        Status imsRequestConference(const ImsRequestConferenceCallback& cb);
        Status imsRequestHoldCall(uint32_t callId, const ImsRequestHoldCallback& cb);
        Status imsRequestResumeCall(uint32_t callId, const ImsRequestResumeCallback& cb);
        Status imsRequestExitEmergencyCallbackMode(const ImsExitECBMCallback& cb);
        Status imsSetColr(const RIL_IMS_ColrInfo& colr, const ImsSetColrCallback& cb);
        Status imsGetColr(const ImsGetColrCallback& cb);
        Status imsRegisterMultiIdentityLines(const RIL_IMS_MultiIdentityLineInfo lineInfo[],
                size_t size, const ImsRegisterMultiIdentityLinesCallback& cb);
        Status imsQueryVirtualLineInfo(const char* msisdn,
                const ImsQueryVirtualLineInfoCallback& cb);
        Status imsSendSms(const RIL_IMS_SmsMessage& smsMessage, const ImsSendSmsCallback& cb);
        Status imsAckSms(const RIL_IMS_SmsAck& smsAck, const ImsAckSmsCallback& cb);
        Status imsGetSmsFormat(const ImsGetSmsFormatCallback& cb);
        Status imsSetCallWaiting(const RIL_IMS_CallWaitingSettings& callWaitingSettings, const ImsSetCallWaitingCallback& cb);
        Status imsQueryCallWaiting(uint32_t serviceClass, const ImsQueryCallWaitingCallback& cb);

        Status registerImsUnsolRegistrationStateIndicationHandler(
            const ImsRegistrationStateIndicationHandler& indicationHandler);
        Status registerImsUnsolServiceStatusIndicationHandler(
            const ImsServiceStatusIndicationHandler& indicationHandler);
        Status registerImsUnsolSubConfigChangeIndicationHandler(
            const ImsSubConfigChangeIndicationHandler& indicationHandler);
        Status registerImsUnsolCallRingIndicationHandler(
            const ImsCallRingIndicationHandler& indicationHandler);
        Status registerImsUnsolRingBackToneIndicationHandler(
            const ImsRingBackToneIndicationHandler& indicationHandler);
        Status registerImsUnsolCallStateChangedIndicationHandler(
            const ImsCallStateChangedIndicationHandler& indicationHandler);
        Status registerImsUnsolEnterEcbmIndicationHandler(
            const ImsEnterEcbmIndicationHandler& indicationHandler);
        Status registerImsUnsolExitEcbmIndicationHandler(
            const ImsExitEcbmIndicationHandler& indicationHandler);
        Status registerImsUnsolModifyCallIndicationHandler(
            const ImsModifyCallIndicationHandler& indicationHandler);
        Status registerImsUnsolMessageWaitingIndicationHandler(
            const ImsMessageWaitingIndicationHandler& indicationHandler);
        Status registerImsUnsolSuppSvcNotificationIndicationHandler(
            const ImsSuppSvcNotificationIndicationHandler& indicationHandler);
        Status registerImsUnsolOnSsIndicationHandler(
            const ImsOnSsIndicationHandler& indicationHandler);
        Status registerImsUnsolVopsChangedIndicationHandler(
            const ImsVopsChangedIndicationHandler& indicationHandler);
        Status registerImsUnsolParticipantStatusInfoIndicationHandler(
            const ImsParticipantStatusInfoIndicationHandler& indicationHandler);
        Status registerImsUnsolRegistrationBlockStatusIndicationHandler(
            const ImsRegistrationBlockStatusIndicationHandler& indicationHandler);
        Status registerImsUnsolWfcRoamingModeConfigSupportIndicationHandler(
            const ImsWfcRoamingModeConfigSupportIndicationHandler& indicationHandler);
        Status registerImsUnsolUssdFailedIndicationHandler(
            const ImsUssdFailedIndicationHandler& indicationHandler);
        Status registerImsUnsolVoiceInfoIndicationHandler(
            const ImsVoiceInfoIndicationHandler& indicationHandler);
        Status registerImsIncomingSmsHandler(const ImsIncomingSmsHandler& handler);
        Status registerImsSmsStatusReportHandler(const ImsSmsStatusReportHandler& handler);
        Status registerImsViceInfoHandler(const ImsViceInfoHandler& handler);
        Status registerImsTtyNotificationHandler(const ImsTtyNotificationHandler& handler);
        Status registerImsUnsolHandoverIndicationHandler(
            const ImsHandoverIndicationHandler& indicationHandler);
        Status registerImsUnsolRefreshConferenceInfo(
            const ImsRefreshConferenceInfoIndicationHandler& indicationHandler);
        Status registerImsUnsolMultiIdentityPendingInfo(
            const VoidIndicationHandler &indicationHandler);
        Status registerImsUnsolMultiIdentityRegistrationStatus(
            const MultiIdentityRegistrationStatusHandler indicationHandler);

        /* QCRIL_UIM Request Message APIs */
        Status simGetImsiReq(const char *aid, const GetImsiReqCallback& cb);
        Status simIOReq(const RIL_SIM_IO_v6& reqParams, const IOReqCallback& cb);
        Status simGetSimStatusReq(const GetSimStatusReqCallback& cb);
        Status simOpenChannelReq(const RIL_OpenChannelParams& reqParams, const OpenChannelReqCallback& cb);
        Status simCloseChannelReq(const int sessionId, const CloseChannelReqCallback& cb);
        Status simTransmitApduBasicReq(const RIL_SIM_APDU& reqParams, const TransmitApduBasicReqCallback& cb);
        Status simTransmitApduChannelReq(const RIL_SIM_APDU& reqParams, const TransmitApduChannelReqCallback& cb);
        Status simChangePinReq(const char ** reqParams, const ChangePinReqCallback& cb);
        Status simChangePin2Req(const char ** reqParams, const ChangePin2ReqCallback& cb);
        Status simEnterPukReq(const char ** reqParams, const EnterPukReqCallback& cb);
        Status simEnterPuk2Req(const char ** reqParams, const EnterPuk2ReqCallback& cb);
        Status simEnterPinReq(const char ** reqParams, const EnterPinReqCallback& cb);
        Status simEnterPin2Req(const char ** reqParams, const EnterPin2ReqCallback& cb);
        Status simQueryFacilityLockReq(const char ** reqParams, const QueryFacilityLockReqCallback& cb);
        Status simSetFacilityLockReq(const char ** reqParams, const SetFacilityLockReqCallback& cb);
        Status simIsimAuthenticationReq(const char * reqParams, const IsimAuthenticationReqCallback& cb);
        Status simStkSendEnvelopeWithStatusReq(const char * reqParams, const StkSendEnvelopeWithStatusReqCallback& cb);
        Status simAuthenticationReq(const RIL_SimAuthentication &reqParams, const AuthenticationReqCallback& cb);
        Status EnterNetworkDepersonalization(const char **reqParams, const EnterPinReqCallback &cb);
        Status GetAdnRecord(const GetAdnRecordCallback &cb);
        Status UpdateAdnRecord(const RIL_AdnRecordInfo *record, const VoidResponseHandler &cb);
        Status registerAdnInitDone(
            const VoidIndicationHandler indicationHandler);
        Status registerSimStatusChanged(
            const VoidIndicationHandler &indicationHandler);
        Status registerStkCcAlphaNotify(
            const CharStarIndicationHandler &indicationHandler);
        Status registerSimRefresh(
            const SimRefreshIndicationHandler &indicationHandler);

    private:
        RilInstance rilInstance;
        int socketFd = -1;
        std::unique_ptr<RequestManager> reqMgr;
};

