#/bin/bash

##################################################################################################
## slim_pb_generate_script.sh
## Description:
## Util script to compile the proto files using protoc to generate the header and C file.
## This is intended to be run once you have made a full build and if any changes are made to proto
## files or if any new proto files are added. This relieves the burden of developer in finding the
## path to protobuf work folder and running those commands.
##
## This script is also used in pre compile stage to compile the proto files so that the generated
## C and header file are based on the protoc compiler for that workspace (LV or LE).
##
## For more details, pls refer the below link
##          - https://developers.google.com/protocol-buffers/docs/reference/cpp-generated
##
## Note:
## 1. Add any new .proto files to "src_proto_files" array
## 2. If protobuf package version is updated, update the values in this script too (variable -
##    protobuf_package_ver).
##
##  Copyright (c) 2020 Qualcomm Technologies, Inc.
##  All Rights Reserved.
##  Confidential and Proprietary - Qualcomm Technologies, Inc.
##################################################################################################

echo -e "\slim_pb_generate_script.sh"
echo      "***************************"
# find workspace path
wsp_path=`repo info platform/hardware/qcom/gps 2>&1|grep -o "Mount path:.*"|sed 's/Mount path: //'|sed 's/hardware\/qcom\/gps//'`
echo "Workspace path is: $wsp_path"
# find manifest
manifest_info=`repo info . 2>/dev/null|grep "Manifest merge branch"|sed 's/Manifest merge branch: refs\/[a-z]*\///'`
echo "Manifest branch/AU is: $manifest_info"

if [ "$wsp_path" == "" ]
then
    ## this happens when this script runs as part of bitbake compilation in do_compile_prepend
    echo "Wsp path is NULL"
    if [ "$WORKSPACE" != "" ];then
        # LE targets has root path in $WORKSPACE
        wsp_path=$WORKSPACE"/"
    elif [ "$SRC_DIR_ROOT" != "" ];then
        # LV targets has root path in $WORKSPACE
        wsp_path=$SRC_DIR_ROOT"/"
    else
        ## Compiling in <ROOT>/poky/build/tmp-glibc/work/sa8155qdrive-agl-linux/slim-daemon/git-r1/gps/slim/
        ## 9 level up - <ROOT> of workspace
        wsp_path="../../../../../../../../../"
    fi
    echo "do_compile_prepend: Wsp path is $wsp_path"
    # find target type
    if [ -e $wsp_path"meta-qti-bsp-prop/meta-qti-base-prop/recipes-location/gps/slim-daemon_git.bb" ]
    then
        manifest_info="LV"
    else
        manifest_info="LE"
    fi
    echo "Target is $manifest_info"
fi

# dynamically obtain the protobuf package version folder
# find the protobuf package version path from the path
protobuf_versions=(`ls $wsp_path"poky/build/tmp-glibc/work/x86_64-linux/protobuf-native/"`)
if [ ${#protobuf_versions[@]} -eq 0 ]; then
    echo "ERROR!! No protobuf package version folder found..."
    exit 1
fi

echo "Protobuf package version folders "${protobuf_versions[@]}
# if multiple, select first one always
protobuf_package_ver=${protobuf_versions[0]}

# get paths to protoc
echo;
protobuf_work_path=$wsp_path"poky/build/tmp-glibc/work/x86_64-linux/protobuf-native/"$protobuf_package_ver
echo "Protobuf work path is : $protobuf_work_path"

##<PROTOBUF_WORK_PATH>/git/src/protoc
protobuf_protoc_path=$protobuf_work_path"/git/src/protoc"
echo "Binary protoc path is : $protobuf_protoc_path"

## array of .proto files. Add any new .proto files to this array.
src_proto_files=(SlimRemoteMsgs.proto)

for src_file in ${src_proto_files[@]}; do
    echo -e "\nProcessing $src_file"
    ## protoc --cpp_out=. EHMsg.proto
    echo " >> Running protoc"
    cmd=$protobuf_protoc_path' --proto_path=. --cpp_out=. '"provider/rp/proto/"$src_file
    eval $cmd
    if [ $? -eq 0 ]; then
        echo "SUCCESS !!! protoc compilation"
    else
        echo "ERROR!! protoc compilation failed"
        exit 1
    fi
done
