"""
Copyright (c) 2020 Qualcomm Technologies, Inc.
All Rights Reserved.
Confidential and Proprietary - Qualcomm Technologies, Inc.
"""

import sys
import os

class HypDump(object):
    def __init__(self, ramdump):
        self.ramdump = ramdump
        self.arm64 = ramdump.arm64
        self.kaslr_addr = None
        self.kaslr_offset = None
        self.page_offset = 0x80000000
        self.hyp_kaslr_addr_offset = None
        self.hyp_cpu_thread = None

    def determine_kaslr(self):
        hyp_kaslr_addr = self.ramdump.address_of('aarch64_kaslr_base')
        hyp_kaslr_addr = hyp_kaslr_addr + self.page_offset
        self.hyp_kaslr_addr_offset = self.ramdump.read_u64(hyp_kaslr_addr,False)
    def get_trace_phy(self):
        hyp_cpu_thread = self.ramdump.address_of('cpulocal_primary_thread')
        hyp_cpu_thread = hyp_cpu_thread - self.hyp_kaslr_addr_offset
        hyp_cpu_thread_offset = hyp_cpu_thread + self.page_offset
        self.hyp_cpu_thread_data = self.ramdump.read_u64(hyp_cpu_thread_offset,False)

    def get_ttbr(self):
        cspace_offset = self.ramdump.field_offset('struct thread', 'cspace_cspace')
        cspace_offset_addr = self.hyp_cpu_thread_data + cspace_offset
        cspace_offset_addr = cspace_offset_addr - self.hyp_kaslr_addr_offset
        cspace_offset_addr_actual = cspace_offset_addr + self.page_offset

        self.cspace_offset_addr_actual_data = self.ramdump.read_u64(cspace_offset_addr_actual,False)
        cap_thread_offset = self.ramdump.field_offset('cspace_t', 'tables')
        self.cspace_table_addr = self.cspace_offset_addr_actual_data +  cap_thread_offset #offset of table
        cspace_table_addr_offset = self.cspace_table_addr - self.hyp_kaslr_addr_offset
        cspace_table_addr_offset = cspace_table_addr_offset + self.page_offset
        self.cspace_table_addr_actual = self.ramdump.read_u64(cspace_table_addr_offset,False)

        cap_count_offset = self.ramdump.field_offset('cspace_t', 'cap_count')
        cap_count_addr = self.cspace_offset_addr_actual_data +  cap_count_offset
        cap_count_addr_offset = cap_count_addr - self.hyp_kaslr_addr_offset
        cap_count_addr_offset = cap_count_addr_offset + self.page_offset
        cap_count_value = self.ramdump.read_u64(cap_count_addr_offset,False)
        i = 0

        cap_slots_offset = self.ramdump.field_offset('cap_table_t', 'cap_slots')
        cap_data_offset = self.ramdump.field_offset('cap_t', 'data')
        cap_object_offset = self.ramdump.field_offset('cap_data_t', 'object')
        cap_info_offset = self.ramdump.field_offset('cap_data_t', 'info')
        cap_bf_offset = self.ramdump.field_offset('cap_info_t', 'bf')
        cap_slots_addr = self.cspace_table_addr_actual +  cap_slots_offset

        object_thread_offset = self.ramdump.field_offset('object_ptr_t', 'thread')
        addrspace_offset = self.ramdump.field_offset('thread_t', 'addrspace')
        vm_pgtable_offset = self.ramdump.field_offset('addrspace_t', 'vm_pgtable')

        vttbr_offset = self.ramdump.field_offset('pgtable_vm_t', 'vttbr_el2')
        regs_el1_offset = self.ramdump.field_offset('thread_t', 'vcpu_regs_el1')
        ttbr1_offset = self.ramdump.field_offset('vcpu_el1_registers_t', 'ttbr1_el1')
        while i < cap_count_value:
            cap_slots_addr_next = self.ramdump.array_index(
                cap_slots_addr, 'cap_t', i)
            cap_data = cap_slots_addr_next + cap_data_offset
            cap_object = cap_data + cap_object_offset
            cap_info = cap_data + cap_info_offset
            cap_bf = cap_info + cap_bf_offset

            cap_bf_addr_offset = cap_bf - self.hyp_kaslr_addr_offset
            cap_bf_addr_offset = cap_bf_addr_offset + self.page_offset
            cap_bf_addr = self.ramdump.read_u64(cap_bf_addr_offset,False)
            cap_type = cap_bf_addr >> 8
            i = i + 1
            if cap_type == 8:
                svm_thread_offset = cap_object + object_thread_offset
                svm_thread_addr_offset = (svm_thread_offset - self.hyp_kaslr_addr_offset) + self.page_offset
                svm_thread_addr = self.ramdump.read_u64(svm_thread_addr_offset,False)

                addrspace_addr_offset = svm_thread_addr + addrspace_offset
                addrspace_addr_data = (addrspace_addr_offset - self.hyp_kaslr_addr_offset) + self.page_offset
                addrspace_addr = self.ramdump.read_u64(addrspace_addr_data,False)
                pgtable_addr = addrspace_addr + vm_pgtable_offset

                vttbr_addr_offset = pgtable_addr + vttbr_offset
                vttbr_addr = (vttbr_addr_offset - self.hyp_kaslr_addr_offset) + self.page_offset
                self.vttbr = self.ramdump.read_u64(vttbr_addr,False)
                self.vttbr = self.vttbr & 0xFFFFFFFFFFFE
                regs_el1 = svm_thread_addr + regs_el1_offset

                ttbr1_addr_offset = regs_el1 + ttbr1_offset
                ttbr1_addr = (ttbr1_addr_offset - self.hyp_kaslr_addr_offset) + self.page_offset
                self.ttbr1 = self.ramdump.read_u64(ttbr1_addr,False)
                self.ttbr1 = self.ttbr1 & 0xFFFFFFFFFFFE
