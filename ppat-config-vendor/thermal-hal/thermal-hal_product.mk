THERMAL_HAL := android.hardware.thermal@2.0-impl
THERMAL_HAL += android.hardware.thermal@2.0-service.qti
THERMAL_HAL += android.hardware.thermal@2.0-service.qti.rc
THERMAL_HAL += android.hardware.thermal@2.0-service.qti.xml

PRODUCT_PACKAGES += $(THERMAL_HAL)

PRODUCT_PACKAGES_DEBUG += thermal_hal_test
