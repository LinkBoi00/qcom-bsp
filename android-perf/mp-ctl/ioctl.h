/******************************************************************************
  @file    ioctl.h
  @brief   Definitions of ioctl calls made to display driver.

  DESCRIPTION

  ---------------------------------------------------------------------------
  Copyright (c) 2020 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
  ---------------------------------------------------------------------------
******************************************************************************/

int early_wakeup_ioctl(int connectorId);