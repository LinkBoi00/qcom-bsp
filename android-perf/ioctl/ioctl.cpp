/******************************************************************************
  @file    ioctl.cpp
  @brief   ioctl call made to display driver

  DESCRIPTION

  ---------------------------------------------------------------------------
  Copyright (c) 2020 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
  ---------------------------------------------------------------------------
******************************************************************************/

#include <ctype.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <cutils/log.h>
#include "ioctl.h"
#include <xf86drm.h>
#include <display/drm/sde_drm.h>

static int g_disp_fd = -1;

int open_display_driver() {
    g_disp_fd = drmOpen("msm_drm", NULL);
    if (g_disp_fd < 0) {
        ALOGE("drmIOCTLLib open failed with error=%s", strerror(errno));
        goto exit;
    }
    drmDropMaster(g_disp_fd);
exit:
    return g_disp_fd;
}

void close_display_driver() {
    if (g_disp_fd < 0) {
        ALOGI("drmIOCTLLib display FD is not valid %d", g_disp_fd);
    }
    else {
        int ret = drmClose(g_disp_fd);
        g_disp_fd = -1; // nothing to do close fails
        ALOGI("drmIOCTLLib close return %d", ret);
    }
}

int early_wakeup_ioctl(int connectorId) {
    int FAILED = -1;
    ALOGI("drmIOCTLLib connectorId: %d", connectorId);
    if ((g_disp_fd < 0) && (open_display_driver() < 0)) {
        return FAILED;
    }

    struct drm_msm_display_hint display_hint= {};
    struct drm_msm_early_wakeup early_wakeup = {};
    display_hint.hint_flags= DRM_MSM_DISPLAY_EARLY_WAKEUP_HINT;
    display_hint.data = (uint64_t) &early_wakeup;
    early_wakeup.wakeup_hint = 1;
    early_wakeup.connector_id = connectorId;
    int ret = drmIoctl(g_disp_fd, DRM_IOCTL_MSM_DISPLAY_HINT, &display_hint);
    ALOGI("drmIOCTLLib ret: %d", ret);
    if(ret) {
        ALOGE("drmIOCTLLib failed ret=%d error=%s", ret, strerror(errno));
        close_display_driver();
        return FAILED;
    }

    close_display_driver();
    return 0;
}