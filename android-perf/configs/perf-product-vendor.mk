# QC_PROP_ROOT is already set when we reach here:
# PATH for reference: QC_PROP_ROOT := vendor/qcom/proprietary
$(call inherit-product-if-exists, $(QC_PROP_ROOT)/android-perf/profiles.mk)

PRODUCT_PACKAGES += \
    libqti-perfd \
    libqti-util \
    vendor.qti.hardware.perf@2.2 \
    vendor.qti.hardware.perf@2.2-service.rc \
    vendor.qti.hardware.perf@2.2-service \
    android.hardware.tests.libhwbinder@1.0-impl \
    libperfgluelayer \
    libperfconfig


#IOP Feature
ifneq ($(call is-board-platform-in-list, msm8937 msm8953 msm8998 qcs605 sdm660 sdm710 sdm845 ),true)
PRODUCT_PACKAGES += \
    libqti-iopd \
    vendor.qti.hardware.iop@2.0 \
    vendor.qti.hardware.iop@2.0-service.rc \
    vendor.qti.hardware.iop@2.0-service
endif

#LM Feature
ifneq ($(call is-board-platform-in-list, msm8953 sdm660 sdm845 ),true)
PRODUCT_PACKAGES += \
    liblearningmodule \
    libmeters \
    libreffeature \
    ReferenceFeature.xml \
    AdaptLaunchFeature.xml \
    AppClassifierFeature.xml \
    GameOptimizationFeature.xml
endif

# For Pixel Targets.
# ODM partition will be created and below rc files will go to ODM partition in pixel targets to disable the perf and IOP services.
ifeq ($(GENERIC_ODM_IMAGE),true)
PRODUCT_PACKAGES += \
    init.pixel.vendor.qti.hardware.perf@2.2-service.rc \
    init.pixel.vendor.qti.hardware.iop@2.0-service.rc
endif

PRODUCT_PACKAGES_DEBUG += \
    perflock_native_test \
    perflock_native_stress_test \
    perfunittests \
    boostConfigParser \
    libqti-tests-mod1 \
    libqti-tests-mod2 \
    libqti-tests-mod3 \
    libqti-tests-mod4 \
    libqti-tests-mod5 \
    libqti-tests-mod6
