/*==========================================================================
Description
  It has implementation for logger class

# Copyright (c) 2017 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Confidential and Proprietary - Qualcomm Technologies, Inc.

===========================================================================*/

#pragma once

#include <hidl/HidlSupport.h>
#include "ring_buffer.h"
#include "uart_ipc.h"
#include "hci_internals.h"
#include "diag_interface.h"
#include "state_info.h"

#define LOG_YEAR_LENGTH 4
#define LOG_TS_UNIT_LENGTH 2
#define LOG_FILE_TS_LENGTH 23
#define LOGS_EXTN ".log"
#define SNOOP_EXTN ".cfa"
#define FW_DUMP_EXTN ".bin"
#define LOGS_TS           "%.04d-%.02d-%.02d_%.02d-%.02d-%.02d"
#define LOG_COLLECTION_DIR "/data/vendor/ssrdump/"
#define SOC_DUMP_PREFIX "ramdump_bt_fw_crashdump_"
#define SOC_DUMP_PATH LOG_COLLECTION_DIR SOC_DUMP_PREFIX LOGS_TS FW_DUMP_EXTN
#define SNOOP_FILE_PATH  LOG_COLLECTION_DIR SNOOP_FILE_NAME_PREFIX LOGS_TS SNOOP_EXTN
#define UART_IPC_TX_LOGS_DEST_PATH  LOG_COLLECTION_DIR UART_IPC_TX_LOG_PREFIX LOGS_TS LOGS_EXTN
#define UART_IPC_RX_LOGS_DEST_PATH LOG_COLLECTION_DIR UART_IPC_RX_LOG_PREFIX LOGS_TS LOGS_EXTN
#define UART_IPC_STATE_LOGS_DEST_PATH LOG_COLLECTION_DIR UART_IPC_STATE_LOG_PREFIX LOGS_TS LOGS_EXTN
#define UART_IPC_PWR_LOGS_DEST_PATH LOG_COLLECTION_DIR UART_IPC_PWR_LOG_PREFIX LOGS_TS LOGS_EXTN
#define BT_STATE_FILE_PATH LOG_COLLECTION_DIR BT_STATE_FILE_NAME_PREFIX LOGS_TS LOGS_EXTN


/* Packet types */
#define LOG_BT_CMD_PACKET_TYPE     0x01
#define LOG_BT_ACL_PACKET_TYPE     0x02
#define LOG_BT_SCO_PACKET_TYPE     0x03
#define LOG_BT_EVT_PACKET_TYPE     0x04
#define LOG_ANT_CTL_PACKET_TYPE    0x0c
#define LOG_ANT_DATA_PACKET_TYPE   0x0e
#define LOG_BT_EVT_VENDOR_SPECIFIC 0xFF
#define LOG_BT_EVT_CMD_CMPLT 0x0E

/* Message type of the log from controller */
#define LOG_BT_CONTROLLER_LOG        0x01
#define LOG_BT_MESSAGE_TYPE_VSTR     0x02
#define LOG_BT_MESSAGE_TYPE_PACKET   0x05
#define LOG_BT_MESSAGE_TYPE_MEM_DUMP 0x08
#define LOG_BT_MESSAGE_TYPE_HW_ERR   0x09
#define LOG_BT_HCI_BUFFER_DUMP       0x0A


/* Sub log ID for the message type PACKET */
#define LOG_BT_HCI_CMD   0
#define LOG_BT_HCI_EVENT 1

#define LOG_BT_RX_LMP_PDU      18
#define LOG_BT_TX_LMP_PDU      19
#define LOG_BT_RX_LE_CTRL_PDU  20
#define LOG_BT_TX_LE_CTRL_PDU  21
#define LOG_BT_TX_LE_CONN_MNGR 22

#define LOG_BT_LINK_MANAGER_STATE    0x80
#define LOG_BT_CONN_MANAGER_STATE    0x81
#define LOG_BT_SECURITY_STATE        0x82
#define LOG_BT_LE_CONN_MANAGER_STATE 0x83
#define LOG_BT_LE_CHANNEL_MAP_STATE  0x84
#define LOG_BT_LE_ENCRYPTION_STATE   0x85

/* Sub log ID for the message type VSTR */
#define LOG_BT_VSTR_ERROR 0
#define LOG_BT_VSTR_HIGH  1
#define LOG_BT_VSTR_LOW   2

/* Diag ID for LMP packers */
#define LOG_BT_DIAG_LMP_LOG_ID 0x1041
#define LOG_BT_DIAG_LMP_RX_ID  0x1048
#define LOG_BT_DIAG_LMP_TX_ID  0x1049

/* To format LMP logs */
#define LOG_BT_QXDM_PKT_LENGTH_POS    0
#define LOG_BT_QXDM_PKT_LENGTH2_POS   1
#define LOG_BT_QXDM_DEVICE_IDX_POS    2
#define LOG_BT_QXDM_PKT_POS           3

#define LOG_BT_DBG_DEVICE_IDX_POS 0
#define LOG_BT_DBG_PKT_LENGTH_POS 1
#define LOG_BT_DBG_PKT_POS 2

/* Headed size of the log */
#define LOG_BT_HEADER_SIZE (sizeof(bt_log_pkt) - 1)
#define CRASH_SOURCE_FILE_PATH_LEN 50

#define MAX_RING_BUF_SIZE (8*1024)
#define OUT_RING_BUF 0
#define IN_RING_BUF 1

#define HOST_SPECIFIC_HW_ERR_EVT 0x0f
#define BT_HW_ERR_EVT            0x10
#define BT_HW_ERR_FRAME_SIZE     0x01


#define LOG_I(tag, fmt, args...) ALOG(LOG_INFO, tag, fmt, ## args)
#define LOG_W(tag, fmt, args...) ALOG(LOG_WARN, tag, fmt, ## args)
#define LOG_E(tag, fmt, args...) ALOG(LOG_ERROR, tag, fmt, ## args)

#define IBS_LOG_INFO(tag, fmt, args...) if(Logger.GetIbsLogLevel() == Logger::log_level::HIGH)LOG_I(tag, fmt, ##args)
#define IBS_LOG_WAR(tag, fmt, args...) if(Logger.GetIbsLogLevel() >= Logger::log_level::MID)LOG_W(tag, fmt, ##args)
#define IBS_LOG_ERR(tag, fmt, args...) LOG_E(tag, fmt, ##args)

#define RX_LOG_INFO(tag, fmt, args...) if(Logger.GetRxLogLevel() == Logger::log_level::HIGH)LOG_I(tag, fmt, ##args)
#define RX_LOG_WAR(tag, fmt, args...) if(Logger.GetRxLogLevel() >= Logger::log_level::MID)LOG_W(tag, fmt, ##args)
#define RX_LOG_ERR(tag, fmt, args...) LOG_E(tag, fmt, ##args)

#define TX_LOG_INFO(tag, fmt, args...) if(Logger.GetTxLogLevel() == Logger::log_level::HIGH)LOG_I(tag, fmt, ##args)
#define TX_LOG_WAR(tag, fmt, args...) if(Logger.GetTxLogLevel() >= Logger::log_level::MID)LOG_W(tag, fmt, ##args)
#define TX_LOG_ERR(tag, fmt, args...) LOG_E(tag, fmt, ##args)

#define GEN_LOG_INFO(tag, fmt, args...) if(Logger.GetGenLogLevel() == Logger::log_level::HIGH)LOG_I(tag, fmt, ##args)
#define GEN_LOG_WAR(tag, fmt, args...) if(Logger.GetGenLogLevel() >= Logger::log_level::MID)LOG_W(tag, fmt, ##args)
#define GEN_LOG_ERR(tag, fmt, args...) LOG_E(tag, fmt, ##args)

namespace android {
namespace hardware {
namespace bluetooth {
namespace V1_0 {
namespace implementation {
using CrashDumpStatusCallback = std::function<void(bool)>;
using HWEventCallback = std::function<void()>;

typedef enum {
  BT_SOC_REASON_DEFAULT                 =  0x00,

  // SoC Crash Reasons
  BT_SOC_REASON_SOCCRASH_RX_NULL        =  0x01,
  BT_SOC_REASON_TX_RX_INVALID_PKT = 0x40,
  BT_SOC_REASON_TX_RX_INVALID_LEN = 0x41,
  BT_SOC_REASON_TX_RX_INVALID_PKT_FATAL = 0xC0,
  BT_SOC_REASON_TX_RX_INVALID_LEN_FATAL = 0xC1,
  BT_SOC_REASON_UNKNOWN        =  0x81,
  BT_SOC_REASON_SW_REQUESTED   =  0x82,
  BT_SOC_REASON_STACK_OVERFLOW =  0x83,
  BT_SOC_REASON_EXCEPTION      =  0x84,
  BT_SOC_REASON_ASSERT         =  0x85,
  BT_SOC_REASON_TRAP           =  0x86,
  BT_SOC_REASON_OS_FATAL       =  0x87,
  BT_SOC_REASON_HCI_RESET      =  0x88,
  BT_SOC_REASON_PATCH_RESET    =  0x89,
  BT_SOC_REASON_ABT            =  0x8A,
  BT_SOC_REASON_RAMMASK        =  0x8B,
  BT_SOC_REASON_PREBARK        =  0x8C,
  BT_SOC_REASON_BUSERROR       =  0x8D,
  BT_SOC_REASON_IO_FATAL       =  0x8E,
  BT_SOC_REASON_SSR_CMD        =  0x8F,
  BT_SOC_REASON_POWERON        =  0x90,
  BT_SOC_REASON_WATCHDOG       =  0x91,

  // Transport Driver Crash Reasons
  BT_HOST_REASON_UARTINIT_STUCK        =  0xB1,
  BT_HOST_REASON_GETVER_SEND_STUCK     =  0xB2,
  BT_HOST_REASON_GETVER_NO_RSP_RCVD    =  0xB3,
  BT_HOST_REASON_SETBAUDRATE_CMD_STUCK =  0xB4,
  BT_HOST_REASON_PATCH_DNLD_STUCK      =  0xB5,
  BT_HOST_REASON_GETBOARDID_CMD_STUCK  =  0xB6,
  BT_HOST_REASON_NVM_DNLD_STUCK        =  0xB7,
  BT_HOST_REASON_HCI_RESET_STUCK       =  0xB8,
  BT_HOST_REASON_GETBLDINFO_CMD_STUCK  =  0xB9,
  BT_HOST_REASON_ADDONFEAT_CMD_STUCK   =  0xBA,
  BT_HOST_REASON_ENHLOG_CMD_STUCK      =  0xBB,
  BT_HOST_REASON_DIAGINIT_STUCK        =  0xBC,
  BT_HOST_REASON_DIAGDEINIT_STUCK      =  0xBD,
  BT_HOST_REASON_XMEM_NVM_DNLD_STUCK   =  0xBE,
  BT_HOST_REASON_XMEM_PATCH_DNLD_STUCK =  0xBF,
  BT_HOST_REASON_SECURE_BRIDGE_CMD_STUCK = 0xC2,
  BT_HOST_REASON_FAILED_TO_SEND_CMD              =  0xC3,
  BT_HOST_REASON_HCI_RESET_CC_NOT_RCVD           =  0xC4,
  BT_HOST_REASON_HCI_PRE_SHUTDOWN_CC_NOT_RCVD    =  0xC5,
  BT_HOST_REASON_HCI_SET_BD_ADDRESS_CC_NOT_RCVD  =  0xC6,
  BT_HOST_REASON_FAILED_TO_RECEIVE_SLEEP_IND     =  0xC7,
} SecondaryReasonCode;

typedef struct {
  SecondaryReasonCode reason;
  char reasonstr[50];
} SecondaryReasonMap;

typedef struct {
  uint16_t year = 0;
  uint8_t month = 0;
  uint8_t date = 0;
  uint8_t hour = 0;
  uint8_t min = 0;
  uint8_t sec = 0;
} LogFileTS;

struct LogFileName{
  std::string name;
  LogFileTS ts;
};

static SecondaryReasonMap secReasonMap[] = {
  { BT_SOC_REASON_DEFAULT                      ,    "Default"},
  { BT_SOC_REASON_SOCCRASH_RX_NULL             ,    "Rx Null"},
  { BT_SOC_REASON_UNKNOWN                 ,    "Unknown"},
  { BT_SOC_REASON_TX_RX_INVALID_PKT       ,    "Tx/Rx invalid packet"},
  { BT_SOC_REASON_TX_RX_INVALID_LEN       ,    "Tx/Rx invalid len"},
  { BT_SOC_REASON_TX_RX_INVALID_PKT_FATAL ,    "Tx/Rx invalid packet fatal error"},
  { BT_SOC_REASON_TX_RX_INVALID_LEN_FATAL ,    "Tx/Rx invalid lenght fatal error"},
  { BT_SOC_REASON_SW_REQUESTED            ,    "SW Requested"},
  { BT_SOC_REASON_STACK_OVERFLOW          ,    "Stack Overflow"},
  { BT_SOC_REASON_EXCEPTION               ,    "Exception"},
  { BT_SOC_REASON_ASSERT                  ,    "Assert"},
  { BT_SOC_REASON_TRAP                    ,    "Trap"},
  { BT_SOC_REASON_OS_FATAL                ,    "OS Fatal"},
  { BT_SOC_REASON_HCI_RESET               ,    "HCI Reset"},
  { BT_SOC_REASON_PATCH_RESET             ,    "Patch Reset"},
  { BT_SOC_REASON_ABT                     ,    "SoC Abort"},
  { BT_SOC_REASON_RAMMASK                 ,    "RAM MASK"},
  { BT_SOC_REASON_PREBARK                 ,    "PREBARK"},
  { BT_SOC_REASON_BUSERROR                ,    "Bus error"},
  { BT_SOC_REASON_IO_FATAL                ,    "IO fatal eror"},
  { BT_SOC_REASON_SSR_CMD                 ,    "SSR CMD"},
  { BT_SOC_REASON_POWERON                 ,    "Power ON"},
  { BT_SOC_REASON_WATCHDOG                     ,    "Watchdog"},
  { BT_HOST_REASON_UARTINIT_STUCK              ,    "UartInitStuck"},
  { BT_HOST_REASON_GETVER_SEND_STUCK           ,    "GetVerSendStuck"},
  { BT_HOST_REASON_GETVER_NO_RSP_RCVD          ,    "GetVerNoRspRcvd"},
  { BT_HOST_REASON_SETBAUDRATE_CMD_STUCK       ,    "SetBaudRateStuck"},
  { BT_HOST_REASON_PATCH_DNLD_STUCK            ,    "PatchDnldStuck"},
  { BT_HOST_REASON_GETBOARDID_CMD_STUCK        ,    "GetBoardIdStuck"},
  { BT_HOST_REASON_NVM_DNLD_STUCK              ,    "NvmDnldStuck"},
  { BT_HOST_REASON_HCI_RESET_STUCK             ,    "HciResetStuck"},
  { BT_HOST_REASON_GETBLDINFO_CMD_STUCK        ,    "GetBldInfoCmdStuck"},
  { BT_HOST_REASON_ADDONFEAT_CMD_STUCK         ,    "AddOnFeatCmdStuck"},
  { BT_HOST_REASON_ENHLOG_CMD_STUCK            ,    "EnhLogCmdStuck"},
  { BT_HOST_REASON_DIAGINIT_STUCK              ,    "DiagInitStuck"},
  { BT_HOST_REASON_DIAGDEINIT_STUCK            ,    "DiagDeinitStuck"},
  { BT_HOST_REASON_XMEM_NVM_DNLD_STUCK         ,    "XMEM NVM Download stuck"},
  { BT_HOST_REASON_XMEM_PATCH_DNLD_STUCK       ,    "XMEM patch download stuck"},
  { BT_HOST_REASON_SECURE_BRIDGE_CMD_STUCK     ,    "Secure bridge cmd stuck"},
  { BT_HOST_REASON_FAILED_TO_SEND_CMD          ,    "Failed to send internal cmd"},
  { BT_HOST_REASON_HCI_RESET_CC_NOT_RCVD       ,    "HCI Reset Cmd CC Not Rcvd"},
  { BT_HOST_REASON_HCI_PRE_SHUTDOWN_CC_NOT_RCVD   , "HCI Pre shutdown Cmd CC not Rcvd"},
  { BT_HOST_REASON_HCI_SET_BD_ADDRESS_CC_NOT_RCVD , "HCI BD address CC not Rcvd"},
  { BT_HOST_REASON_FAILED_TO_RECEIVE_SLEEP_IND    , "Failed to receive SLEEP IND from SoC"},
};

class Logger
{
 private:
  static Logger *instance_;
  static const diagpkt_user_table_entry_type ssr_bt_tbl_[];
  short int ibs_log_level_, rx_log_level_, tx_log_level_, gen_log_level_;
  int time_year, time_month, time_day, time_hour, time_min, time_sec;
  static bool any_reg_file;
  PrimaryReasonCode dump_reason_;
  RingBuffer snoop_buff_;
#ifdef DUMP_IPC_LOG
  IpcLogs ipc_log_;
#endif
  DiagInterface diag_interface_;
  CrashDumpStatusCallback crash_dump_status_cb_;
  HWEventCallback hw_event_cb_;
  BtState *state_info_;

#ifdef DIAG_ENABLED
  static void *SsrBtHandler(void *, uint16);
#endif

  bool HandleHciEvent(HciPacketType, uint8_t *, uint16_t);
  void SaveSocMemDump(uint8_t *, uint16_t, PrimaryReasonCode);
  void DeleteDumpsIfRequired();
  void DeleteDumps(char *);
  void SetDumpTimestamp();

#ifdef DUMP_RINGBUF_LOG
  void DumpRingBuffers(void) {
    snoop_buff_.DumpData();
  };
#endif

#ifdef DUMP_IPC_LOG
  void DumpUartIpcLogs(void) {
    ipc_log_.DumpUartLogs();
  };
#endif

  static bool isLastestLogFileName(const LogFileName& current, const LogFileName& latest);
  static LogFileName LastestLogFileName(const std::string& prefix, const std::string& dir);
  static void WriteToFd(int fd, const LogFileTS& latest_file_ts, const std::string prefix,
                        const std::string dir, const std::string log_sufix);
  time_t time_rx_call_back;
 public:
  Logger();
  enum {
    LOW = 0,
    MID,
    HIGH,
  } log_level;
  static std::recursive_mutex bugreport_mutex;
  static bool is_crash_dump_in_progress_;
  static pthread_mutex_t crash_dump_lock;
  static pthread_cond_t crash_dump_cond;
  static std::mutex bugreport_wait_mutex;
  static std::condition_variable bugreport_wait_cv;
  static bool is_bugreport_triggered_during_crash_dump;
  static bool is_bugreport_triggered;
  bool host_crash_during_init;
  bool stack_timeout_triggered;
  static Logger *Get(void);
  void UnlockDiagMutex();
  void GetCrashDumpFileName(char*);
#ifdef DUMP_RINGBUF_LOG
  void GetSnoopFileName(char*);
  void SetNormalSsrTrigered(bool);
  void SaveIbsToRingBuf(HciPacketType type,  uint8_t ibs_data) {
    /* IBS is a one byte command or event sent or received */
    snoop_buff_.AddLog(RingBuffer::IBS_BYTE, type, &ibs_data, 1);
  };
#endif
#ifdef DUMP_IPC_LOG
  void GetUartDumpFilename(short int, char*);
#endif
  static void DumpLogsInBugreport(int fd);
  bool GetCleanupStatus(ProtocolType ptype);
  void StoreCrashReason(void);
  void FrameCrashEvent(hidl_vec<uint8_t>*bt_packet_);
  void FrameBqrRieEvent(hidl_vec<uint8_t>*bt_packet_);
  bool ProcessRx(HciPacketType type, const uint8_t *buff, uint16_t len);
  bool ProcessTx(HciPacketType, const uint8_t *, uint16_t);
  bool IsSnoopLogEnabled(void);
  bool IsHciFwSnoopEnabled(void);

  void Cleanup(void);

  bool IsControllerLogPkt(HciPacketType type, const uint8_t *buff, uint16_t len);
  bool IsCrashDumpStarted(void);
  bool RegisterCrashDumpCallback (CrashDumpStatusCallback crash_dump_cb);
  void RegisterHWEventCallback(HWEventCallback);
  bool PrepareDumpProcess();
  bool isSsrTriggered();
  bool isDiagSsrTriggered();
  void UpdateRxEventTag(RxThreadEventType rx_event) {
#ifdef DUMP_RINGBUF_LOG
    snoop_buff_.UpdateRxEventTag(rx_event);
#endif
  };
  void UpdateRxTimeStamp();
  void ResetSsrTriggeredFlag();
  bool SetSsrTriggeredFlag();
  void UnlockRingbufferMutex();

  void SaveLastStackHciCommand(char * cmd) {
#ifdef DUMP_RINGBUF_LOG
    snoop_buff_.SaveLastStackHciCommand(cmd);
#endif
  };

  void ResetCrashReasons();
  void SetPrimaryCrashReason(PrimaryReasonCode);
  void SetSecondaryCrashReason(SecondaryReasonCode);
  char *get_reset_reason_str(SecondaryReasonCode);
  void DecodeHwErrorEvt(uint8_t *buff);
  uint8_t GetClientStatus(ProtocolType);
  void SetClientStatus(bool , ProtocolType);
  bool IsPreviousItrCrashed();
  char *GetSecondaryCrashReason();
  PrimaryReasonCode GetPrimaryReason();

  void Init(HciTransport *hci_transport);

  void GetStateFileName(char*);

  inline short int GetIbsLogLevel() {return ibs_log_level_;};
  inline short int GetRxLogLevel() {return rx_log_level_;};
  inline short int GetTxLogLevel() {return tx_log_level_;};
  inline short int GetGenLogLevel() {return gen_log_level_;};
  void ResetForceSsrTriggeredFlag();
  void CollectDumps(bool is_uart_ipc_enabled, bool is_state_log_enabled);
  void ResetCleanupflag();
  void SetInternalCmdTimeout();
  bool DiagInitOnGoing();
};

} // namespace implementation
} // namespace V1_0
} // namespace bluetooth
} // namespace hardware
} // namespace android
