ifeq ($(call is-board-platform-in-list, msm8937 msm8953),true)
TARGET_USES_NQ_NFC := true
ifeq ($(TARGET_PRODUCT),msm8937_32go)
TARGET_USES_NQ_NFC := false
endif
endif

NQ_VENDOR_NFC_PROP := nqnfcinfo

#NQ and SN100 NFC Config files + firmware images
ifeq ($(TARGET_ARCH), arm64)
PRODUCT_COPY_FILES += \
    vendor/qcom/proprietary/nqnfc-firmware/pn547c2/64-bit/libpn547_fw.so:$(TARGET_COPY_OUT_VENDOR)/lib/libpn547_fw.so \
    vendor/qcom/proprietary/nqnfc-firmware/pn548ad/64-bit/libpn548ad_fw.so:$(TARGET_COPY_OUT_VENDOR)/lib/libpn548ad_fw.so \
    vendor/qcom/proprietary/nqnfc-firmware/pn551/64-bit/libpn551_fw.so:$(TARGET_COPY_OUT_VENDOR)/lib/libpn551_fw.so \
    vendor/qcom/proprietary/nqnfc-firmware/pn553/64-bit/libpn553_fw.so:$(TARGET_COPY_OUT_VENDOR)/lib/libpn553_fw.so \
    vendor/qcom/proprietary/nqnfc-firmware/pn557/64-bit/libpn557_fw.so:$(TARGET_COPY_OUT_VENDOR)/lib/libpn557_fw.so \
    vendor/qcom/proprietary/nqnfc-firmware/sn100x/64-bit/libsn100u_fw.so:$(TARGET_COPY_OUT_VENDOR)/lib/libsn100u_fw.so
else
PRODUCT_COPY_FILES += \
    vendor/qcom/proprietary/nqnfc-firmware/pn547c2/32-bit/libpn547_fw.so:$(TARGET_COPY_OUT_VENDOR)/lib/libpn547_fw.so \
    vendor/qcom/proprietary/nqnfc-firmware/pn548ad/32-bit/libpn548ad_fw.so:$(TARGET_COPY_OUT_VENDOR)/lib/libpn548ad_fw.so \
    vendor/qcom/proprietary/nqnfc-firmware/pn551/32-bit/libpn551_fw.so:$(TARGET_COPY_OUT_VENDOR)/lib/libpn551_fw.so \
    vendor/qcom/proprietary/nqnfc-firmware/pn553/32-bit/libpn553_fw.so:$(TARGET_COPY_OUT_VENDOR)/lib/libpn553_fw.so \
    vendor/qcom/proprietary/nqnfc-firmware/pn557/32-bit/libpn557_fw.so:$(TARGET_COPY_OUT_VENDOR)/lib/libpn557_fw.so \
    vendor/qcom/proprietary/nqnfc-firmware/sn100x/32-bit/libsn100u_fw.so:$(TARGET_COPY_OUT_VENDOR)/lib/libsn100u_fw.so
endif

PRODUCT_COPY_FILES += \
    vendor/qcom/proprietary/nqnfc-firmware/libnfc-mtp_default.conf:$(TARGET_COPY_OUT_VENDOR)/etc/libnfc-mtp_default.conf \
    vendor/qcom/proprietary/nqnfc-firmware/libnfc-mtp_rf1.conf:$(TARGET_COPY_OUT_VENDOR)/etc/libnfc-mtp_rf1.conf \
    vendor/qcom/proprietary/nqnfc-firmware/libnfc-mtp-NQ3XX.conf:$(TARGET_COPY_OUT_VENDOR)/etc/libnfc-mtp-NQ3XX.conf \
    vendor/qcom/proprietary/nqnfc-firmware/libnfc-mtp-NQ4XX.conf:$(TARGET_COPY_OUT_VENDOR)/etc/libnfc-mtp-NQ4XX.conf \
    vendor/qcom/proprietary/nqnfc-firmware/libnfc-mtp_rf2.conf:$(TARGET_COPY_OUT_VENDOR)/etc/libnfc-mtp_rf2.conf \
    vendor/qcom/proprietary/nqnfc-firmware/libnfc-qrd_default.conf:$(TARGET_COPY_OUT_VENDOR)/etc/libnfc-qrd_default.conf \
    vendor/qcom/proprietary/nqnfc-firmware/libnfc-qrd_rf1.conf:$(TARGET_COPY_OUT_VENDOR)/etc/libnfc-qrd_rf1.conf \
    vendor/qcom/proprietary/nqnfc-firmware/libnfc-qrd-NQ3XX.conf:$(TARGET_COPY_OUT_VENDOR)/etc/libnfc-qrd-NQ3XX.conf \
    vendor/qcom/proprietary/nqnfc-firmware/libnfc-qrd-NQ4XX.conf:$(TARGET_COPY_OUT_VENDOR)/etc/libnfc-qrd-NQ4XX.conf \
    vendor/qcom/proprietary/nqnfc-firmware/libnfc-qrd_rf2.conf:$(TARGET_COPY_OUT_VENDOR)/etc/libnfc-qrd_rf2.conf \
    vendor/qcom/proprietary/nqnfc-firmware/libnfc-mtp-SN100.conf:$(TARGET_COPY_OUT_VENDOR)/etc/libnfc-mtp-SN100.conf \
    vendor/qcom/proprietary/nqnfc-firmware/libnfc-qrd-SN100.conf:$(TARGET_COPY_OUT_VENDOR)/etc/libnfc-qrd-SN100.conf

ifeq ($(strip $(TARGET_USES_NQ_NFC)),true)
PRODUCT_PACKAGES += $(NQ_VENDOR_NFC_PROP)
endif
