////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2020 Qualcomm Technologies, Inc.
// All Rights Reserved.
// Confidential and Proprietary - Qualcomm Technologies, Inc.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// @file  QCameraSECCamEncode.cpp
/// @brief Copy Secure Content
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "QCameraSECCamEncode.h"
#include "QCameraHAL3Device.h"

const char* SECCAM_ENCODE_LIB  = "libseccamenc.so";

QCameraSECCamEncode::QCameraSECCamEncode()
    :mLibHandle(dlopen(SECCAM_ENCODE_LIB, RTLD_NOW)),
     l_QSEEComHandle(NULL)
{
    long ret = SCE_SUCCESS;
    ALOGD("%s:E", __FUNCTION__);

     /* Initialize functions */
    do
    {
        ALOGD("%s: Opening API libraries", __FUNCTION__);
        if(NULL == mLibHandle)
        {
             ALOGE("%s: Error: Opening copy libs failed. exits !", __FUNCTION__);
             break;
        }

        mSecure_Camera_Encode_Init =
            (Secure_Camera_Encode_Init)dlsym(mLibHandle, "sce_init");
        if(NULL == mSecure_Camera_Encode_Init)
        {
            ALOGE("%s: Error: loading symbol sce_init failed . exit !", __FUNCTION__);
            break;
        }

        mSecure_Camera_Encode_Terminate =
            (Secure_Camera_Encode_Terminate)dlsym(mLibHandle, "sce_terminate");
        if(NULL == mSecure_Camera_Encode_Terminate)
        {
            ALOGE("%s: Error: loading symbol sce_terminate failed . exit !", __FUNCTION__);
            break;
        }

        mSecure_Camera_Encode_Register_Buffers =
            (Secure_Camera_Encode_Register_Buffers)dlsym(mLibHandle, "sce_register_buffers");
        if ( NULL == mSecure_Camera_Encode_Register_Buffers)
        {
            ALOGE("%s: Error : loading symbol sce_register_buffers exit", __FUNCTION__);
            break;
        }

        mSecure_Camera_Encode_Prepare_Buffers =
            (Secure_Camera_Encode_Prepare_Buffers)dlsym(mLibHandle, "sce_prepare_buffers");
        if ( NULL == mSecure_Camera_Encode_Prepare_Buffers)
        {
            ALOGE("%s: Error : loading symbol sce_prepare_buffers exit", __FUNCTION__);
            break;
        }

        mSecure_Camera_Encode_Copy_Frame_Data =
            (Secure_Camera_Encode_Copy_Frame_Data)dlsym(mLibHandle, "sce_copy_frame_data");
        if ( NULL == mSecure_Camera_Encode_Copy_Frame_Data)
        {
            ALOGE("%s: Error : loading symbol sce_copy_frame_data exit", __FUNCTION__);
            break;
        }

        mSecure_Camera_Encode_Sign_Encrypt =
            (Secure_Camera_Encode_Sign_Encrypt)dlsym(mLibHandle, "sce_sign_encrypt");
        if ( NULL == mSecure_Camera_Encode_Sign_Encrypt)
        {
            ALOGE("%s: Error : loading symbol sce_sign_encrypt exit", __FUNCTION__);
            break;
        }

        ret = mSecure_Camera_Encode_Init(&l_QSEEComHandle, "seccamenc");
        if(ret != SCE_SUCCESS)
        {
            ALOGE("%s Error: Content_Protection_Copy_Init failed with ret = 0x%x", __FUNCTION__, ret);
            break;
        }
    } while(0);
    ALOGD("%s:X", __FUNCTION__);
}

QCameraSECCamEncode::~QCameraSECCamEncode()
{
    long ret = SCE_SUCCESS;
    ALOGD("%s:E", __FUNCTION__);
    /* terminate app */
    if (l_QSEEComHandle != NULL)
    {
        ret = mSecure_Camera_Encode_Terminate(&l_QSEEComHandle);
        if(ret != SCE_SUCCESS)
        {
            ALOGE("%s Error: Content_Protection_Copy_Terminate failed with ret = 0x%x", __FUNCTION__, ret);
        }
    }
    else
    {
        ALOGE("%s Error: Invalid QSEECom handle", __FUNCTION__);
    }
    ALOGD("%s:X", __FUNCTION__);
}

QCameraSECCamEncode * QCameraSECCamEncode::GetInstance()
{
        static QCameraSECCamEncode mInstance;
        return &mInstance;
}

QCameraSecureCopyResult QCameraSECCamEncode::CopySecureFrameData (
                                    BufferInfo * pNonSecBufferinfo,
                                    BufferInfo * pSecBufferinfo,
                                    sce_buf_type_t bufType)
{
    unsigned int outBuffLen = 0;
    sce_buf_tuple_t buff_for_copy;
    long ret = SCE_SUCCESS;

    ALOGI("%s: Sec_Ion_fd = %d size = %d", __FUNCTION__, pSecBufferinfo->fd, pSecBufferinfo->size);
    ALOGI("%s: non_secure_fd = %d size = %d", __FUNCTION__, pNonSecBufferinfo->fd, pNonSecBufferinfo->size);

    buff_for_copy.sce_buf1.buf_handle = pSecBufferinfo->fd;
    if ( pSecBufferinfo->size < pNonSecBufferinfo->size)
    {
        buff_for_copy.sce_buf1.buf_size = pSecBufferinfo->size;
    }
    else
    {
        buff_for_copy.sce_buf1.buf_size = pNonSecBufferinfo->size;
    }

    buff_for_copy.sce_buf1.buf_type = bufType;

    buff_for_copy.sce_buf2.buf_handle = pNonSecBufferinfo->fd;
    buff_for_copy.sce_buf2.buf_size = pNonSecBufferinfo->size;
    buff_for_copy.sce_buf2.buf_type = SCE_BUF_NON_SECURE;
    // outBuffLen = pNonSecBufferinfo->size;
    if (mSecure_Camera_Encode_Copy_Frame_Data != NULL && l_QSEEComHandle != NULL)
    {
        ret = mSecure_Camera_Encode_Copy_Frame_Data(l_QSEEComHandle, buff_for_copy, outBuffLen);
        if (ret != SCE_SUCCESS)
        {
            ALOGE("%s Error: Secure_Camera_Encode_Copy secure to non-secure failed.", __FUNCTION__);
            ret = SCE_ERROR_COPY_FAILED;
        }
    }
    else
    {
        ret = SCE_ERROR_INIT_FAILED;
    }
    ALOGI("%s:outBuffLen = %d",__FUNCTION__, outBuffLen);

    return (QCameraSecureCopyResult)ret;
}

QCameraSecureCopyResult QCameraSECCamEncode::CopySecureEncodedData (
                                    BufferInfo * pNonSecBufferinfo,
                                    BufferInfo * pSecBufferinfo)
{
    unsigned int outBuffLen = 0;
    sce_buf_tuple_t buff_for_copy;
    long ret = SCE_SUCCESS;

    ALOGI("%s: Sec_Ion_fd = %d size = %d", __FUNCTION__, pSecBufferinfo->fd, pSecBufferinfo->size);
    ALOGI("%s: non_secure_fd = %d size = %d", __FUNCTION__, pNonSecBufferinfo->fd, pNonSecBufferinfo->size);

    buff_for_copy.sce_buf1.buf_handle = pSecBufferinfo->fd;
    if ( pSecBufferinfo->size < pNonSecBufferinfo->size)
    {
        buff_for_copy.sce_buf1.buf_size = pSecBufferinfo->size;
    }
    else
    {
        buff_for_copy.sce_buf1.buf_size = pNonSecBufferinfo->size;
    }
    buff_for_copy.sce_buf1.buf_type = SCE_BUF_ENCODE_FRAME;

    buff_for_copy.sce_buf2.buf_handle = pNonSecBufferinfo->fd;
    buff_for_copy.sce_buf2.buf_size = pNonSecBufferinfo->size;
    buff_for_copy.sce_buf2.buf_type = SCE_BUF_NON_SECURE;

//    outBuffLen = pNonSecBufferinfo->size;
    if (mSecure_Camera_Encode_Copy_Frame_Data != NULL && l_QSEEComHandle != NULL)
    {
        ret = mSecure_Camera_Encode_Sign_Encrypt(l_QSEEComHandle, buff_for_copy, outBuffLen);
        if (ret != SCE_SUCCESS)
        {
            ALOGE("%s Error: Secure_Camera_Encode_Copy secure to non-secure failed.", __FUNCTION__);
            ret = SCE_ERROR_COPY_FAILED;
        }
    }
    else
    {
        ret = SCE_ERROR_INIT_FAILED;
    }
    ALOGI("%s:outBuffLen = %d",__FUNCTION__, outBuffLen);

    return (QCameraSecureCopyResult)ret;
}


QCameraSecureCopyResult QCameraSECCamEncode::RegisterBuffer (BufferManager *pBufferManager,
                                    camera3_stream_t* pStream)
{
    sce_buf_type_t SecBufType = SCE_BUF_INVALID_TYPE;
    uint32_t       NumSecBuffers = 0;
    sce_buf_t buffer;
    BufferInfo * pSecureBufInfo;
    sce_result_t ret = SCE_SUCCESS;
    sce_buf_t *buff_list = NULL;

    if (pBufferManager != NULL && pStream != NULL)
    {
        if(IsVideoStream(pStream) == true)
        {
            SecBufType = SCE_BUF_FULL_FRAME;
        }
        else if (IsPreviewStream(pStream) == true)
        {
            SecBufType = SCE_BUF_DOWN_SCALE_FRAME;
        }
        else if (IsRawStream(pStream) == true)
        {
            SecBufType = SCE_BUF_RAW_FRAME;
        }
        NumSecBuffers = pBufferManager->GetNumberofBuffers();
        ALOGD("%s:DBG...NumSecBuffers =%d.", __FUNCTION__, NumSecBuffers);
        buff_list = (sce_buf_t *)malloc(NumSecBuffers * sizeof(sce_buf_t));
        if (buff_list != NULL)
        {
            for (int i = 0; i < NumSecBuffers; i++)
            {
                pSecureBufInfo = pBufferManager->getBufferInfo(i);
                if (IsRawStream(pStream) == true)
                    pSecureBufInfo->size = 1024*10; // Only Registring 10KB for Raw buffer 
                buff_list[i].buf_handle = pSecureBufInfo->fd;
                buff_list[i].buf_size = pSecureBufInfo->size;
                buff_list[i].buf_type = SecBufType;
                ALOGD("%s: buffer.buf_handle =%" PRId64 "", __FUNCTION__, buff_list[i].buf_handle);
                ALOGD("%s: buffer.buf_size =%d", __FUNCTION__, buff_list[i].buf_size);
                ret = mSecure_Camera_Encode_Register_Buffers(l_QSEEComHandle, &buff_list[i], 1);
                if (ret != SCE_SUCCESS)
                {
                    break;
                }
            }
            ALOGD("%s: Buffer list size =%d", __FUNCTION__, NumSecBuffers * sizeof(sce_buf_t));
            free(buff_list);
            buff_list = NULL;
        }
        else
        {
            ALOGE("Failed to allocated buffer list");
            ret = SCE_FAILURE;
        }
    }
    ALOGD("%s: DBG...Registration done.", __FUNCTION__);
    return (QCameraSecureCopyResult)ret;
}

QCameraSecureCopyResult QCameraSECCamEncode::PrepareBuffer(BufferManager *FFBufferManager,
    BufferManager * DSBufferManager)
{
    uint32_t       NumSecBuffers = 0;
    sce_buf_tuple_t sce_buf_tuple;
    BufferInfo * pSecureBufInfo;
    sce_result_t ret = SCE_SUCCESS;

    if (FFBufferManager != NULL && DSBufferManager != NULL)
    {
        NumSecBuffers = FFBufferManager->GetNumberofBuffers();
        pSceFSBufmanager = FFBufferManager;
        pSceDSBufmanager = DSBufferManager;
        for (int i = 0; i < NumSecBuffers; i++)
        {
            pSecureBufInfo = FFBufferManager->getBufferInfo(i);
            sce_buf_tuple.sce_buf1.buf_handle = pSecureBufInfo->fd;
            sce_buf_tuple.sce_buf1.buf_size = pSecureBufInfo->size;
            sce_buf_tuple.sce_buf1.buf_type = SCE_BUF_FULL_FRAME;
            ALOGD("%s: buffer 1.buf_handle =%" PRId64 "", __FUNCTION__, sce_buf_tuple.sce_buf1.buf_handle);
            ALOGD("%s: buffer 1 buf_size =%d", __FUNCTION__, sce_buf_tuple.sce_buf1.buf_size);

            pSecureBufInfo = DSBufferManager->getBufferInfo(i);
            sce_buf_tuple.sce_buf2.buf_handle = pSecureBufInfo->fd;
            sce_buf_tuple.sce_buf2.buf_size = pSecureBufInfo->size;
            sce_buf_tuple.sce_buf2.buf_type = SCE_BUF_DOWN_SCALE_FRAME;
            ALOGD("%s: buffer 2.buf_handle =%" PRId64 "", __FUNCTION__, sce_buf_tuple.sce_buf2.buf_handle);
            ALOGD("%s: buffer 2 buf_size =%d", __FUNCTION__, sce_buf_tuple.sce_buf2.buf_size);

            ret = mSecure_Camera_Encode_Prepare_Buffers(l_QSEEComHandle, sce_buf_tuple);
            if (ret != SCE_SUCCESS)
            {
                ALOGE("%s Error: Secure_Camera_Encode_Copy secure to non-secure failed.", __FUNCTION__);
                ret = SCE_ERROR_PREPARE_BUF_FAILED;
            }
        }
    }
    ALOGD("%s: DBG...Preparation done.", __FUNCTION__);
    return (QCameraSecureCopyResult)ret;
}

QCameraSecureCopyResult QCameraSECCamEncode::RePrepareBuffer(
    BufferInfo * pSecFSBufferinfo)
{
    BufferInfo * pSecDSBufferinfo = NULL;
    sce_buf_tuple_t sce_buf_tuple;
    sce_result_t ret = SCE_SUCCESS;
    int32_t fs_buff_index = -1;

    if( pSecFSBufferinfo != NULL)
    {
        ALOGI("%s: Sec_Ion_fd = %d size = %d", __FUNCTION__, pSecFSBufferinfo->fd, pSecFSBufferinfo->size);
        fs_buff_index = pSceFSBufmanager->getBufferInfoIndex(pSecFSBufferinfo);
//        pSecDSBufferinfo = pSceDSBufmanager->getBufferInfo(fs_buff_index);

        sce_buf_tuple.sce_buf1.buf_handle = pSecFSBufferinfo->fd;
        sce_buf_tuple.sce_buf1.buf_size = pSecFSBufferinfo->size;
        sce_buf_tuple.sce_buf1.buf_type = SCE_BUF_FULL_FRAME;
        ALOGD("%s: buffer 1.buf_handle =%" PRId64 "", __FUNCTION__, sce_buf_tuple.sce_buf1.buf_handle);
        ALOGD("%s: buffer 1 buf_size =%d", __FUNCTION__, sce_buf_tuple.sce_buf1.buf_size);

        pSecDSBufferinfo = pSceDSBufmanager->getBufferInfo(fs_buff_index);
        sce_buf_tuple.sce_buf2.buf_handle = pSecDSBufferinfo->fd;
        sce_buf_tuple.sce_buf2.buf_size = pSecDSBufferinfo->size;
        sce_buf_tuple.sce_buf2.buf_type = SCE_BUF_DOWN_SCALE_FRAME;
        ALOGD("%s: buffer 2.buf_handle =%" PRId64 "", __FUNCTION__, sce_buf_tuple.sce_buf2.buf_handle);
        ALOGD("%s: buffer 2 buf_size =%d", __FUNCTION__, sce_buf_tuple.sce_buf2.buf_size);

        ret = mSecure_Camera_Encode_Prepare_Buffers(l_QSEEComHandle, sce_buf_tuple);
        if (ret != SCE_SUCCESS)
        {
            return (QCameraSecureCopyResult)ret;
        }
    }
    return (QCameraSecureCopyResult)ret;
}
