////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2020 Qualcomm Technologies, Inc.
// All Rights Reserved.
// Confidential and Proprietary - Qualcomm Technologies, Inc.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// @file  QCameraSECCamEncode.h
/// @brief Copy Secure Content
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __QCAMERA_SEECAM_ENCODE__
#define __QCAMERA_SEECAM_ENCODE__

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <dirent.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/eventfd.h>
#include <errno.h>
#include <linux/msm_ion.h>
#include <hardware/camera3.h>

#if TARGET_ION_ABI_VERSION >= 2
//#include <ion/ion.h>
#endif
#include <linux/dma-buf.h>

#ifdef ANDROID
#include <utils/Log.h>
#endif
#include "QSEEComAPI.h"
#include <sys/mman.h>
#include <getopt.h>
#include <dlfcn.h>
#include "SecCamEnc.h"
#include "BufferManager.h"

#define SCE_COPY_SECURE_TO_NONSECURE 0

typedef enum QCameraSecureCopyResult {
    QCAMERA_SECCOPY_SUCCESS = 0,
    QCAMERA_SECCOPY_ERROR_INIT_FAILED = 100,
    QCAMERA_SECCOPY_ERROR_TERMINATE_FAILED = 101,
    QCAMERA_SECCOPY_ERROR_FEATURE_NOT_SUPPORTED = 102,
    QCAMERA_SECCOPY_ERROR_INVALID_PARAMS = 103,
    QCAMERA_SECCOPY_ERROR_BUFFER_TOO_SHORT = 104,
    QCAMERA_SECCOPY_ERROR_REG_BUF_FAILED = 105,
    QCAMERA_SECCOPY_ERROR_PREPARE_BUF_FAILED = 106,
    QCAMERA_SECCOPY_ERROR_COPY_FAILED = 107,
    QCAMERA_SECCOPY_ERROR_CRYPTO_FAILED = 108,
    QCAMERA_SECCOPY_FATAL_ERROR = 109,
    QCAMERA_SECCOPY_FAILURE = 0x7FFFFFFF
} QCameraSecureCopyResult;

class QCameraSECCamEncode {
    void *              mLibHandle;
    sce_handle          l_QSEEComHandle;
    BufferManager       *pSceDSBufmanager;
    BufferManager       *pSceFSBufmanager;


    QCameraSECCamEncode();
    ~QCameraSECCamEncode();

public:

    typedef sce_result_t (*Secure_Camera_Encode_Init)(sce_handle *, const char*  appName);
    typedef sce_result_t (*Secure_Camera_Encode_Terminate)(sce_handle *);
    typedef sce_result_t (*Secure_Camera_Encode_Register_Buffers)(sce_handle ,
                                      const void *, const size_t);

    typedef sce_result_t (*Secure_Camera_Encode_Prepare_Buffers)(sce_handle ,
                                 const sce_buf_tuple_t &);
    typedef sce_result_t (*Secure_Camera_Encode_Copy_Frame_Data)(sce_handle ,
                                 const sce_buf_tuple_t &, size_t &);

    typedef sce_result_t (*Secure_Camera_Encode_Sign_Encrypt)(sce_handle ,
                              const sce_buf_tuple_t &, size_t &out_buf_size);

    Secure_Camera_Encode_Init                   mSecure_Camera_Encode_Init;
    Secure_Camera_Encode_Terminate              mSecure_Camera_Encode_Terminate;
    Secure_Camera_Encode_Register_Buffers       mSecure_Camera_Encode_Register_Buffers;
    Secure_Camera_Encode_Prepare_Buffers        mSecure_Camera_Encode_Prepare_Buffers;
    Secure_Camera_Encode_Copy_Frame_Data        mSecure_Camera_Encode_Copy_Frame_Data;
    Secure_Camera_Encode_Sign_Encrypt           mSecure_Camera_Encode_Sign_Encrypt;

    QCameraSecureCopyResult CopySecureFrameData(BufferInfo * pNonSecBufferinfo,
                            BufferInfo * pSecBufferinfo, sce_buf_type_t );

    QCameraSecureCopyResult CopySecureEncodedData(BufferInfo * pNonSecBufferinfo,
                            BufferInfo * pSecBufferinfo);

    QCameraSecureCopyResult RegisterBuffer(BufferManager *, camera3_stream_t* );

    QCameraSecureCopyResult PrepareBuffer(BufferManager *FFBufferManager, BufferManager * DSBufferManager);

    QCameraSecureCopyResult RePrepareBuffer(BufferInfo * );

    static QCameraSECCamEncode * GetInstance();
};


#endif
