////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017-2020 Qualcomm Technologies, Inc.
// All Rights Reserved.
// Confidential and Proprietary - Qualcomm Technologies, Inc.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// @file  g_version.h
/// @brief Compile-time generated version info
///         This file is generated automatically by version.pl/.exe. Do not edit.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef G_VERSION_H
#define G_VERSION_H

#define CAMTEST_SHA1      "4b57c0d517674ee3174632580cabc3cd98991745"
#define CAMTEST_COMMITID  "I2caa537c000e614bd4e3b000127e0eb7a9db3304"
#define CAMTEST_BUILD_TS  "9/17/2020 6:47:32"
#define CAMTESTHOSTNAME   ""
#define CAMBUILD_IP       "10.225.11.48/23"

#endif /* G_VERSION_H */
