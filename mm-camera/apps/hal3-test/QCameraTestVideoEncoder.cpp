////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018-2020 Qualcomm Technologies, Inc.
// All Rights Reserved.
// Confidential and Proprietary - Qualcomm Technologies, Inc.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// @file  QCameraTestVideoEncoder.cpp
/// @brief middle object between omx encoder and video test case
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "QCameraTestVideoEncoder.h"
#if TARGET_ION_ABI_VERSION >= 2
#include <ion/ion.h>
#endif

#ifdef USE_SECCAM_ENCODE
#include "QCameraSECCamEncode.h"
#else
#include "QCameraSEEComCPCopy.h"
#endif

using namespace android;
/************************************************************************
* name : QCameraTestVideoEncoder
* function: init default setting
************************************************************************/
QCameraTestVideoEncoder::QCameraTestVideoEncoder(TestConfig *config)
    :mCoder(NULL),
    mConfig({}),
    mBufferQueue(NULL)
{
    mCoder =  QCameraOMX_Encoder::getInstance();
    mConfig = {
        .componentName = (char *)"OMX.qcom.video.encoder.avc",
        .inputcolorfmt = HAL_PIXEL_FORMAT_YCbCr_420_SP_VENUS,
        /*
         * Support fmt: 0x7fa30c06 HAL_PIXEL_FORMAT_YCbCr_420_SP_VENUS_UBWC
         * Support fmt: 0x7fa30c04 HAL_PIXEL_FORMAT_YCbCr_420_SP_VENUS
         * Support fmt: 0x7fa30c00 HAL_PIXEL_FORMAT_NV21_ENCODEABLE or OMX_QCOM_COLOR_FormatYVU420SemiPlanar or QOMX_COLOR_FormatYVU420SemiPlanar
         * Support fmt: 0x7fa30c09 HAL_PIXEL_FORMAT_YCbCr_420_TP10_UBWC
         * Support fmt: 0x7fa30c0a HAL_PIXEL_FORMAT_YCbCr_420_P010_VENUS
         * Support fmt: 0x7fa30c08
         * Support fmt: 0x7fa30c07 QOMX_VIDEO_CodingMVC
         * Support fmt: 0x7f000789 OMX_COLOR_FormatAndroidOpaque
         * Support fmt: 0x15
         */
        .codec = OMX_VIDEO_CodingAVC,
        /*avc config*/
        .npframe = 29,
        .nbframes = 0,
        .eprofile = OMX_VIDEO_AVCProfileBaseline,
        .elevel = OMX_VIDEO_AVCLevel3,
        .bcabac = 0,
        .nframerate = 30,

        /*buf config*/
        .storemeta = (DISABLE_META_MODE==1)?0:1,

        /*input port param*/
        .input_w = 1920,
        .input_h = 1080,
        .input_buf_cnt = 10,

        /*output port param*/
        .output_w = 1920,
        .output_h = 1080,
        .output_buf_cnt = 10,
    };
    mConfig.isSecureMode = false;
    if (config->mIsSecureMode == true) {
        ALOGI("Secure AVC Component");
        mConfig.isSecureMode = true;
        mConfig.componentName = (char *)"OMX.qcom.video.encoder.avc.secure";
    }
    mConfig.input_w = mConfig.output_w = config->mVideoStream.width;
    mConfig.input_h = mConfig.output_h = config->mVideoStream.height;
#ifdef ENABLE_SECURE_MEM_DUMP
    if (mConfig.isSecureMode == true)
    {
        int ret = 0;
        ret = AllocateNonSecureEncoderBuffer(config->mVideoStream.width,
                               config->mVideoStream.height,
                               config->mVideoStream.format,
                               &mNonSecEncoderBufferHandle,
                               &mNonSecEncoderBufferInfo);
        if (ret == -1)
        {
            ALOGE("QCameraTestVideoEncoder::QCameraTestVideoEncoder: allocation of non secure buffer failed");
        }
    }
#endif
    mConfig.bitrate = config->mVideoRateConfig.bitrate;
    mConfig.targetBitrate = config->mVideoRateConfig.targetBitrate;
    mConfig.isBitRateConstant = config->mVideoRateConfig.isBitRateConstant;
    mTimeOffsetInc = 30000;
    mConfig.nframerate = config->mFpsRange[1];
    if (mConfig.nframerate > 60) {
        mTimeOffsetInc = 15000;
    }
    char path[256] = {0};
    if (config->mIsH265) {
        mConfig.componentName = (char *)"OMX.qcom.video.encoder.hevc",
        mConfig.codec = OMX_VIDEO_CodingHEVC,
        mConfig.eprofile = OMX_VIDEO_HEVCProfileMain,
        mConfig.elevel = OMX_VIDEO_HEVCHighTierLevel3,
        snprintf(path, sizeof(path) -1, "%s/camera_0.h265",CAMERA_STORAGE_DIR, "w+");
    } else {
        snprintf(path, sizeof(path) -1, "%s/camera_0.h264",CAMERA_STORAGE_DIR, "w+");
    }
    mOutFd = fopen(path,"w+");

    mTimeOffset = 0;
    mBufferQueue = new list<buffer_handle_t *>;
    pthread_mutex_init(&mLock, NULL);
    pthread_condattr_t cattr;
    pthread_condattr_init(&cattr);
    pthread_condattr_setclock(&cattr, CLOCK_MONOTONIC);
    pthread_cond_init(&mCond, &cattr);
    pthread_condattr_destroy(&cattr);
    mCoder->setConfig(&mConfig, this);
    mIsStop = false;
}

/************************************************************************
* name : ~QCameraTestVideoEncoder
* function: ~QCameraTestVideoEncoder
************************************************************************/
QCameraTestVideoEncoder::~QCameraTestVideoEncoder()
{
    pthread_mutex_destroy(&mLock);
    pthread_cond_destroy(&mCond);
#ifdef ENABLE_SECURE_MEM_DUMP
    if (mConfig.isSecureMode == true)
    {
        FreeNonSecureEnocoderBuffers(&mNonSecEncoderBufferInfo);
    }
#endif
    if (mOutFd != NULL){
        fclose(mOutFd);
        mOutFd = NULL;
    }
}

/************************************************************************
* name : run
* function: start encoder thread
************************************************************************/
void QCameraTestVideoEncoder::run()
{
    mCoder->start();
}

/************************************************************************
* name : stop
* function: stop encoder
************************************************************************/
void QCameraTestVideoEncoder::stop()
{
    //set stop state
    mIsStop = true;
    mCoder->stop();
    delete mCoder;
    mCoder = NULL;
    buffer_handle_t * buf_handle;
    while (true) {
        pthread_mutex_lock(&mLock);
        ALOGE("QCameraTestVideoEncoder::stop: mBufferQueue:%d\n",mBufferQueue->size());
        if (mBufferQueue->size() > 0) {
            buf_handle = mBufferQueue->front();
            mBufferQueue->pop_front();
        } else {
            delete mBufferQueue;
            mBufferQueue = NULL;
            pthread_mutex_unlock(&mLock);
            break;
        }
        pthread_mutex_unlock(&mLock);
        mStream->bufferManager->PutBuffer(buf_handle);
        buf_handle = NULL;
    }
}

/************************************************************************
* name : Read
* function: handler to fill a buffer of handle to omx input port
************************************************************************/
int QCameraTestVideoEncoder::Read(OMX_BUFFERHEADERTYPE *buf)
{
    encoder_media_buffer_type *meta_buffer;
    meta_buffer = (encoder_media_buffer_type*)(buf->pBuffer);//8 Byte;
    int ret = 0;
    int readLen = 0;
    if (!meta_buffer) {
        ALOGE("meta_buffer is empty");
        return OMX_ErrorNone;
    }

    struct timespec tv;
    pthread_mutex_lock(&mLock);
    if (mBufferQueue != NULL) {
        while (mBufferQueue->size() == 0) {
            clock_gettime(CLOCK_MONOTONIC,&tv);
            tv.tv_sec += 1;//1s
            ret = pthread_cond_timedwait(&mCond, &mLock,&tv);
            if (ret != 0) {
                /*CallStack e;
                  e.update();
                  e.log("TestVideoEncoder");*/
                pthread_mutex_unlock(&mLock);
                ALOGE("%s %d waiting timeout",__func__,__LINE__);
                return -1;
            }
        }
    } else {
        ALOGE("read exit");
        pthread_mutex_unlock(&mLock);
        return -1;
    }
    buffer_handle_t *buf_handle = mBufferQueue->front();
    mBufferQueue->pop_front();
    pthread_mutex_unlock(&mLock);
    if (!buf_handle) {
        ALOGE("buffer handle is NULL");
        return -1;
    } else {

#if DISABLE_META_MODE
//        memcpy(buf->pBuffer,mStream->bufferManager->getBufferInfo(buf_handle)->vaddr,buf->nAllocLen);
        mStream->bufferManager->PutBuffer(buf_handle);
        readLen = buf->nAllocLen;
#else
        meta_buffer->buffer_type = kMetadataBufferTypeGrallocSource;     //0:camera_source 1:gralloc_source
        meta_buffer->meta_handle = *buf_handle;
        ALOGI("meta_buffer->meta_handle %p", meta_buffer->meta_handle);
#endif
    }
    buf->nFilledLen = readLen;
    buf->nTimeStamp = mTimeOffset;
    mTimeOffset += mTimeOffsetInc;
    return ret;
}

/************************************************************************
* name : Write
* function: handler to write omx output data
************************************************************************/
OMX_ERRORTYPE QCameraTestVideoEncoder::Write(OMX_BUFFERHEADERTYPE *buf)
{
#if defined(USE_SECCAM_ENCODE) && defined(ENABLE_SECURE_MEM_DUMP)
     int ret = 0;
     BufferInfo         SecBufferinfo;
     output_metabuffer  *buffer = NULL;
     native_handle_t    *handl  = NULL;

    if (mConfig.isSecureMode == true)
    {

        if (buf) {
            buffer = (output_metabuffer*)buf->pBuffer;
            if (buffer)
                handl = (native_handle_t *)buffer->nh;
        }

        struct pmem *pPmem =(struct pmem *)buf->pOutputPortPrivate;
        SecBufferinfo.fd = pPmem->fd;
        SecBufferinfo.size = handl->data[3]; //4149248;

        ALOGI("%s: SecBufferinfo.fd %d handl fd %d filled len %d used %d ", __FUNCTION__, SecBufferinfo.fd, handl->data[0], buf->nFilledLen, handl->data[2]);
        ALOGI("%s: SecBufferinfo.size %d size is %d ", __FUNCTION__, SecBufferinfo.size, handl->data[3]);
        ALOGI("%s: mNonSecEncoderBufferInfo.size %d", __FUNCTION__, mNonSecEncoderBufferInfo.size);
        ret = QCameraSECCamEncode::GetInstance()->CopySecureEncodedData(
            &mNonSecEncoderBufferInfo, &SecBufferinfo);
        ALOGI("%s: ret %d", __FUNCTION__, ret);
        fwrite(mNonSecEncoderBufferInfo.vaddr, 1, handl->data[2], mOutFd);
    }
    else
    {
        fwrite(buf->pBuffer, 1, buf->nFilledLen, mOutFd);
    }
#else
    fwrite(buf->pBuffer, 1, buf->nFilledLen, mOutFd);
#endif
    ALOGE("%s: received filled lenght of %d", __FUNCTION__, buf->nFilledLen);
    return OMX_ErrorNone;
}

/************************************************************************
* name : EmptyDone
* function: handler to put input buffer
************************************************************************/
OMX_ERRORTYPE QCameraTestVideoEncoder::EmptyDone(OMX_BUFFERHEADERTYPE *buf)
{

#if (!DISABLE_META_MODE)
    encoder_media_buffer_type *meta_buffer;
    BufferInfo * SecFSBufferinfo = NULL;
    QCameraSecureCopyResult  ret        = QCAMERA_SECCOPY_SUCCESS;

    meta_buffer = (encoder_media_buffer_type*)(buf->pBuffer);//8 Byte;
    if (!meta_buffer) {
        ALOGE("meta_buffer is empty");
        return OMX_ErrorNone;
    }
    if (meta_buffer->meta_handle != NULL) {
        int fd = meta_buffer->meta_handle->data[0];
        if (mFrameHandleMap.find(fd)!=mFrameHandleMap.end()) {
#ifdef USE_SECCAM_ENCODE
            if (mConfig.isSecureMode== true)
            {
                SecFSBufferinfo = mStream->bufferManager->getBufferInfo(mFrameHandleMap[fd]);
                if(SecFSBufferinfo != NULL)
                {
                    QCameraSECCamEncode::GetInstance()->RePrepareBuffer(SecFSBufferinfo);
                    if (ret != QCAMERA_SECCOPY_SUCCESS)
                    {
                        ALOGE("%s Error: Secure_Camera_Encode_Copy secure to non-secure failed.", __FUNCTION__);
                        ret = QCAMERA_SECCOPY_ERROR_PREPARE_BUF_FAILED;
                    }
                }
            }
#endif
            mStream->bufferManager->PutBuffer(mFrameHandleMap[fd]);
        }
    }
#endif
    return OMX_ErrorNone;
}

/************************************************************************
* name : EnqueueFrameBuffer
* function: enq a buffer to input port queue
************************************************************************/
void QCameraTestVideoEncoder::EnqueueFrameBuffer(CameraStream *stream, buffer_handle_t *buf_handle){
    mStream = stream;
    mFrameHandleMap[(*buf_handle)->data[0]] = buf_handle;
    pthread_mutex_lock(&mLock);
    mBufferQueue->push_back(buf_handle);
    pthread_cond_signal(&mCond);
    pthread_mutex_unlock(&mLock);
}
#ifdef ENABLE_SECURE_MEM_DUMP
int QCameraTestVideoEncoder::AllocateNonSecureEncoderBuffer(
    uint32_t           width,
    uint32_t           height,
    uint32_t           format,
    buffer_handle_t*   pAllocatedBuffer,
    BufferInfo*        pNonSecBufferInfo)
{
    int32_t result =  0;
#if TARGET_ION_ABI_VERSION >= 2
        if (pAllocatedBuffer == NULL || pNonSecBufferInfo == NULL)
            return -1;

        struct ion_allocation_data alloc;
        struct ion_fd_data ion_info_fd;
        struct ion_fd_data data2;
        native_handle_t* nh = nullptr;
        int rc;
        size_t buf_size;
        int ion_fd;
        struct dma_buf_sync buf_sync;

        ion_fd = ion_open();
        if (ion_fd < 0)
        {
            ALOGE("Opening ION device for allocation failed, errno = %x ", errno);
            return -1;
        }

        memset(&alloc, 0, sizeof(alloc));
        if (height == 1) {
            // Blob
            buf_size = (size_t)width;
        } else {
            buf_size = (size_t)(width * height * 2);
        }

        if ((format == HAL_PIXEL_FORMAT_RAW16) || (format == HAL_PIXEL_FORMAT_RAW10))
        {
            buf_size = (size_t)(width * height * 2)/4;
        }
        alloc.len = (size_t)(buf_size);
        alloc.len = (alloc.len + 4095U) & (~4095U);
        alloc.align = 4096;
        alloc.flags = 0;
        alloc.heap_id_mask = ION_HEAP(ION_QSECOM_HEAP_ID);
        memset(&ion_info_fd, 0, sizeof(ion_info_fd));

        rc= ion_alloc_fd(ion_fd, alloc.len, alloc.align, alloc.heap_id_mask, alloc.flags, &(ion_info_fd.fd));
        if (rc < 0)
        {
            ALOGE("Error: ion_alloc_fd failed, ret = %d, errno = %x, len = %zu", rc, errno, alloc.len);
            ion_info_fd.fd = -1;
            return -1;
        }

        pNonSecBufferInfo->fd = ion_info_fd.fd;
        pNonSecBufferInfo->size = alloc.len;
        pNonSecBufferInfo->vaddr = (uint8_t*)mmap(NULL, alloc.len, PROT_READ | PROT_WRITE, MAP_SHARED, pNonSecBufferInfo->fd, 0);
        if (pNonSecBufferInfo->vaddr == MAP_FAILED)
        {
            ALOGE("Error::ION MMAP failed: len %zu, errno = %d", alloc.len, errno);
            pNonSecBufferInfo->vaddr = NULL;
            return -1;
        }
        buf_sync.flags = DMA_BUF_SYNC_START | DMA_BUF_SYNC_RW;
        rc = ioctl(pNonSecBufferInfo->fd, DMA_BUF_IOCTL_SYNC, &buf_sync);
        if (rc)
        {
            ALOGE("Error:: DMA_BUF_IOCTL_SYNC start failed, ret = %d, errno = %d", rc, errno);
            munmap(pNonSecBufferInfo->vaddr, pNonSecBufferInfo->size);
            pNonSecBufferInfo->vaddr = NULL;
            return -1;
        }

        ALOGD("%s ION FD %d len %d\n", __func__, pNonSecBufferInfo->fd, alloc.len);
//        if (!mIsMetaBuf) {
            if (true) {
            *pAllocatedBuffer = native_handle_create(2, 4);
            nh = (native_handle_t*)(*pAllocatedBuffer);
            (nh)->data[0] = pNonSecBufferInfo->fd;
            (nh)->data[1] = 0;
            (nh)->data[2] = 0;
            (nh)->data[3] = 0;
            (nh)->data[4] = alloc.len;
            (nh)->data[5] = 0;
        } else {
            /*alloc private handle_t */

            /*(buffer_handler_t **)*/
//            if (!mIsUBWC) {
            if (true) {
                *pAllocatedBuffer = native_handle_create(1, 2);
                nh = (native_handle_t*)(*pAllocatedBuffer);
                (nh)->data[0] = pNonSecBufferInfo->fd;
                (nh)->data[1] = 0;
                (nh)->data[2] = alloc.len;
            } else {
                /*UBWC Mode*/
            #if 0
                private_handle_t *pnh = new private_handle_t(ion_info_fd.fd,
                    alloc.len,
                    0,
                    0,
                    0,
                    width,
                    height);
                *pAllocatedBuffer = (native_handle_t *)pnh;
                pnh->flags |= private_handle_t::PRIV_FLAGS_UBWC_ALIGNED;
                pnh->width = width;
                pnh->height = height;
                pnh->size = alloc.len;
                pnh->unaligned_width = width;
                pnh->unaligned_height = height;
                pnh->fd = ion_info_fd.fd;
                pnh->format = format;
            #endif
            }
        }
#endif
    return result;
}

void QCameraTestVideoEncoder::FreeNonSecureEnocoderBuffers(BufferInfo* pNonSecBufferInfo)
{
    if (NULL != pNonSecBufferInfo->vaddr)
    {
#if TARGET_ION_ABI_VERSION >= 2
        struct dma_buf_sync buf_sync;
        int rc;
        if (pNonSecBufferInfo->vaddr == NULL || pNonSecBufferInfo->fd < 0)
        {
            ALOGE("%s - Error invalid parameters vaddr = %p, fd = %d", __FUNCTION__, pNonSecBufferInfo->vaddr,
                pNonSecBufferInfo->fd);
            return;
        }
        rc = munmap(pNonSecBufferInfo->vaddr, pNonSecBufferInfo->size);
        if (rc)
        {
            ALOGE("Error: ion munmap failed, ret = %d, errno = %x", rc, errno);
            return;
        }
        buf_sync.flags = DMA_BUF_SYNC_END | DMA_BUF_SYNC_RW;
        rc = ioctl(pNonSecBufferInfo->fd, DMA_BUF_IOCTL_SYNC, &buf_sync);
        if (rc)
        {
            ALOGE("Error:: DMA_BUF_IOCTL_SYNC start failed, ret = %d, errno = %d\n", rc, errno);
            return;
        }
        if(pNonSecBufferInfo->fd >= 0)
        {
            rc= close(pNonSecBufferInfo->fd);
            if (rc != 0)
            {
                ALOGE("Error: cp_ion_free failed %s , ret = %d, errno = %x , ion_fd_data->fd = %d", __FUNCTION__, rc, errno, pNonSecBufferInfo->fd);
                return;
            }
            pNonSecBufferInfo->fd = -1;
        }
#endif
    }
}
#endif
