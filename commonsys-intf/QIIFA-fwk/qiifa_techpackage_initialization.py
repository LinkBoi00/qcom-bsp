#!/usr/bin/python
# -*- coding: utf-8 -*-
#Copyright (c) 2020 Qualcomm Technologies, Inc.
#All Rights Reserved.
#Confidential and Proprietary - Qualcomm Technologies, Inc.

import os,argparse,textwrap
import xml.etree.ElementTree as gfg

def qiifa_initialization(arg_techpackage):
    qiifa_build_config_path = os.getenv("QIIFA_BUILD_CONFIG")
    if(not os.path.exists(os.path.dirname(qiifa_build_config_path))):
        os.makedirs(os.path.dirname(qiifa_build_config_path))
    fileName = qiifa_build_config_path
    root = gfg.Element("techpackages")
    root.text = "\n    "
    tech_package_list = arg_techpackage.split(",")
    for tech_package in tech_package_list:
        x = gfg.SubElement(root,"techpackage")
        x.text = tech_package
        if(tech_package != tech_package_list[-1]):
            x.tail = "\n    "
        else:
            x.tail = "\n"
    tree = gfg.ElementTree(root)
    with open (fileName, "wb") as files :
        tree.write(files)

def main():
     parser = argparse.ArgumentParser(
             formatter_class=argparse.RawDescriptionHelpFormatter,
             description=textwrap.dedent(
                 '''Tech Package Argument by , seperated'''))
     parser.add_argument("tech_package",
              type=str, nargs='?')
     args = parser.parse_args()
     arg_techpackage = args.tech_package
     if arg_techpackage != None:
        qiifa_initialization(arg_techpackage)
        return
if __name__ == '__main__':
     main()
