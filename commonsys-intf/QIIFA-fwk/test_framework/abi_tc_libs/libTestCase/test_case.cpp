////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017-2019 Qualcomm Technologies, Inc.
// All Rights Reserved.
// Confidential and Proprietary - Qualcomm Technologies, Inc.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <vector>
#include "test_case.h"

std::vector<int> initVector(int lower_range, int upper_range){
    std::vector<int> newVector;
    for(int i = lower_range; i < upper_range; ++i){
        newVector.push_back(i);
    }
    return newVector;
}

int add(int n1, int n2){
    return n1 + n2;
}

int multiply(int n1, int n2){
    return n1 * n2;
}

void deinitVector(std::vector<int> &vec){
    vec.clear();
}

int enumMult(A a){
    return a;
}
