LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
$(shell mkdir -p $(PRODUCT_OUT))
ifeq ($(call is-board-platform-in-list, lahaina),true)
  ECHO_RESULT := $(shell python ./vendor/qcom/proprietary/securemsm/tools/qls/qlstool.py -i ./vendor/qcom/proprietary/securemsm/qapi/qwes/data/UEFI/ -o ./vendor/qcom/proprietary/securemsm/tools/qls/)
  $(shell cp -rf ./vendor/qcom/proprietary/securemsm/tools/qls/qweslicstore.bin $(PRODUCT_OUT))
  $(shell cp -rf ./vendor/qcom/proprietary/securemsm/tools/qls/qlstool.py $(PRODUCT_OUT))
  $(shell cp ./vendor/qcom/proprietary/securemsm/qapi/qwes/data/UEFI/*.pfm $(PRODUCT_OUT)/persist/data/pfm/licenses)
  $(shell cp ./vendor/qcom/proprietary/securemsm/qapi/qwes/data/*.pfm $(PRODUCT_OUT)/persist/data/pfm/licenses)
endif
