# Copyright (c) 2020 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Confidential and Proprietary - Qualcomm Technologies, Inc.

import os
import sys
import struct
import argparse
import fnmatch

# Name & version of the tool
QLS_TOOL_NAME = 'qlstool'
QLS_TOOL_VERSION = '1.0'

__version__ = QLS_TOOL_NAME + ' ' + QLS_TOOL_VERSION

# Path definitions
DEFAULT_QLS_DIR_PATH = os.getcwd()

# Tool configuration
FILE_NAME = 'qweslicstore.bin'
SECTOR_SIZE_IN_BYTES = 4096
PARTITION_SIZE_IN_SECTORS = 29
TOTAL_NUM_OF_PARTITIONS = 2
TOTAL_NUM_OF_SECTORS = 64
SLOT_SIZE_IN_SECTORS = 3
MIN_LICENSES = 1
MAX_LICENSES = ((((PARTITION_SIZE_IN_SECTORS - 1 - 4) / SLOT_SIZE_IN_SECTORS) / 2) * TOTAL_NUM_OF_PARTITIONS) - 1
PARTITION_ID_START = 0
PARTITION_OFFSET = 1
RESERVED = 0x00000000000000000
PFM_FILE_MAXSIZE = 10240

MAGIC_1 = b'PTBL'
MAGIC_2 = 0x00000100

COPY_1 = 1
COPY_2 = 2

LOG_PARTI_1_COPY_1 = {0: 0x2000, 1: 0x5000, 2: 0x8000, 3: 0xb000}
LOG_PARTI_1_COPY_2 = {0: 0x10000, 1: 0x13000, 2: 0x16000, 3: 0x19000}
LOG_PARTI_2_COPY_1 = {4: 0x1F000, 5: 0x22000, 6: 0x25000, 7: 0x28000}
LOG_PARTI_2_COPY_2 = {4: 0x2D000, 5: 0x30000, 6: 0x33000, 7: 0x36000}

Copy_1_Offsets = [LOG_PARTI_1_COPY_1, LOG_PARTI_2_COPY_1]
Copy_2_Offsets = [LOG_PARTI_1_COPY_2, LOG_PARTI_2_COPY_2]


def getHeader():
    """Creates a header for the partition."""
    return (struct.pack('4s', MAGIC_1) +
            struct.pack('I', MAGIC_2) +
            struct.pack('I', (TOTAL_NUM_OF_PARTITIONS * PARTITION_SIZE_IN_SECTORS)) +
            struct.pack('I', TOTAL_NUM_OF_PARTITIONS) + getHdrEntries())


def getHdrEntries():
    """Creates an entries based on number of logical partitions configured."""
    i = 0
    data = bytearray()
    partitionOffset = PARTITION_OFFSET
    while (i < TOTAL_NUM_OF_PARTITIONS):
        data.extend((struct.pack('I', PARTITION_ID_START + i) +
                     struct.pack('HH', partitionOffset, PARTITION_SIZE_IN_SECTORS) +
                     struct.pack('Q', RESERVED)))
        partitionOffset = partitionOffset + PARTITION_SIZE_IN_SECTORS
        i += 1

    return data


def getOffset(slot, copy):
    """Returns an offset for the corresponding given slot and copy."""
    if copy == COPY_1:
        for index in range(len(Copy_1_Offsets)):
            for key, value in Copy_1_Offsets[index].items():
                if slot == key:
                    return value
                else:
                    continue

    elif copy == COPY_2:
        for index in range(len(Copy_2_Offsets)):
            for key, value in Copy_2_Offsets[index].items():
                if slot == key:
                    return value
                else:
                    continue

    else:
        return


def createEmptyPartition(path):
    """Creates an empty partition."""
    qls = open(os.path.join(path, FILE_NAME), "w+b")
    bytes = [0] * (SECTOR_SIZE_IN_BYTES * TOTAL_NUM_OF_SECTORS)
    qls.write(struct.pack("%iB" % (SECTOR_SIZE_IN_BYTES * TOTAL_NUM_OF_SECTORS), *bytes))
    qls.close()


def updatePartition(path, offset, data):
    """Updates the partition with the given data and at the given offset."""
    qls = open(os.path.join(path, FILE_NAME), "r+b")
    qls.seek(offset)
    qls.write(data)
    qls.close()


def main(args):
    """Parses the command line arguments and generates a qweslicstore binary file."""
    print('qlstool launched as: "' + ' '.join(sys.argv) + '"\n')

    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)

    createEmptyPartition(args.output_dir)
    updatePartition(args.output_dir, 0, getHeader())

    if not os.path.exists(args.input_dir):
        print('Input directory and pfm files are not present.\n' +
              'Generated an empty qweslicstore.bin file.')

    dir_path = os.listdir(args.input_dir)
    pfmfiles = len(fnmatch.filter(dir_path, '*.pfm'))
    if (pfmfiles == 0) or (pfmfiles > MAX_LICENSES):
        print('Generated an empty qweslicstore.bin file.\n' +
              '%d number of pfm files are available.\n' % pfmfiles +
              'Minimum of %d file or upto maximum of %d files are supported.' % (MIN_LICENSES, MAX_LICENSES))
        return

    index = 0
    filecount = 0
    for file in dir_path:
        if fnmatch.fnmatch(file, '*.pfm'):
            index += 1
            file_path = os.path.join(args.input_dir, file)
            file_size = os.path.getsize(os.path.join(args.input_dir, file))
            if (file_size == 0) or (file_size > PFM_FILE_MAXSIZE):
                print('Skipping "%s" file, it is either an empty file or it crosses the allowed max file size limit' % file)
                print('Received PFM file size - %d Bytes, Allowed max file size limit - %d Bytes\n' %(file_size, PFM_FILE_MAXSIZE))
                continue
            data = struct.pack(">Q", file_size) + open(file_path, "rb").read()
            updatePartition(args.output_dir, getOffset(index, COPY_1), data)
            updatePartition(args.output_dir, getOffset(index, COPY_2), data)
            filecount += 1
            continue

    print('Successfully generated a "qweslicstore.bin" with %d pfmfiles in the following path. \n' % filecount +
          '%s' % str(args.output_dir))
    return


def parseArgs(argv):
    # Specifying the usage and description.
    # noinspection PyTypeChecker
    qlsparser = argparse.ArgumentParser(usage='python %(prog)s [options]',
                                        description='Generates the qweslicstore binary file.',
                                        formatter_class=argparse.RawDescriptionHelpFormatter,
                                        epilog=('\n'
                                                'This tool is currently configured with the following parameters:-\n'
                                                '1) Generates a qweslicstore.bin of size 256KB.\n'
                                                '2) Supports 2 logical partitions (Data Stores).\n'
                                                '3) Processes only of type *.pfm files from the given input directory '
                                                'only.\n'
                                                '4) Does not recognize the duplicate pfm files.\n'
                                                '5) Generates an empty qweslicstore.bin if there are 0 pfm files or '
                                                'greater than %d pfm files.' % MAX_LICENSES))

    # Specifying the version information.
    qlsparser.add_argument('--version', action='version',
                           version=__version__,
                           help='show the version number and exit')

    # Specifying the input location
    qlsparser.add_argument('-i', '--input_dir', metavar='<dir>',
                           help='directory to process input pfm files. DEFAULT: "./"',
                           default=DEFAULT_QLS_DIR_PATH)

    # Specifying the output location
    qlsparser.add_argument('-o', '--output_dir', metavar='<dir>',
                           help='directory to store output files. DEFAULT: "./"',
                           default=DEFAULT_QLS_DIR_PATH)

    return qlsparser.parse_args()


if __name__ == '__main__':
    try:
        main(parseArgs(sys.argv))

    except Exception:
        print('Exception Raised. Exiting!')
        sys.exit(1)

    except KeyboardInterrupt:
        print('Keyboard Interrupt Received. Exiting!')
        sys.exit(1)

    sys.exit(0)
