ifeq ($(strip $(TARGET_USES_GPQESE)),true)
#SECURE_ELEMENT Build Rules
SECUREMSM_VENDOR += android.hardware.secure_element@1.0-impl
SECUREMSM_VENDOR += vendor.qti.secure_element@1.2-service
SECUREMSM_VENDOR += vendor.qti.secure_element@1.2-service.rc
endif

#eSEPowerManager Build Rules
SECUREMSM_VENDOR += vendor.qti.esepowermanager@1.1-impl
SECUREMSM_VENDOR += vendor.qti.esepowermanager@1.1-service
SECUREMSM_VENDOR += vendor.qti.esepowermanager@1.1-service.rc

ifneq ($(TARGET_PRODUCT),bengal_32go)
PRODUCT_PACKAGES += $(SECUREMSM_VENDOR)
endif
