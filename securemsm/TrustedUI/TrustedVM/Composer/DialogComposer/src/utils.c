/*===========================================================================
 Copyright (c) 2020 Qualcomm Technologies, Inc.
 All Rights Reserved.
 Confidential and Proprietary - Qualcomm Technologies, Inc.
===========================================================================*/
#ifdef SFS
#include "qsee_fs.h"
#include "qsee_sfs.h"
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#ifdef ENABLE_ON_LEVM
#include <stringl.h>
#endif

#include "utils.h"
#include "TUILog.h"
#include "memscpy.h"
#include "TUIHeap.h"
#include "secure_memset.h"

// Set this flag to read secure indicator image from SFS
//#define SFS 1

// Set this flag to provision secure indicator image to SFS
//#define PROVISION 1
#ifdef SFS
static void provision(const char *fsPath, const char *sfsPath)
{
    int fsFd, sfsFd;
    uint32_t size = 0;
    char tempBuff[1024];

    TUILOGD("trying to open the image = %s", fsPath);
    fsFd = open(fsPath, O_RDWR);
    if (fsFd < 0) {
        TUILOGE("failed to open the image = %s", fsPath);
        return;
    }

    qsee_sfs_rm(sfsPath);
    sfsFd = qsee_sfs_open(sfsPath, O_RDWR | O_CREAT | O_TRUNC);
    if (sfsFd <= 0) {
        TUILOGE("sfs open failed for %s, returned = %d",
                 sfsPath, sfsFd);
        close(fsFd);
        return;
    }

    /* Copy the image from fs to sfs */
    do {
        size = read(fsFd, tempBuff, sizeof(tempBuff));
        qsee_sfs_write(sfsFd, (const char *)tempBuff, size);
    } while (size > 0);

    close(fsFd);
    qsee_sfs_close(sfsFd);

    TUILOGD("Done provisioning image = %s", fsPath);
}
#endif


uint32_t loadLogo(uint8_t **logoBuff)
{
    int fd = 0, size = 0, status = 0;
    struct stat fsstat;
    uint32_t logoSize = 0;
    char logoPath[] = "/usr/data/logo.png";

    if (logoBuff == NULL) {
        return 0;
    }

    TUILOGD("read logo file, from = %s", logoPath);
    status = lstat(logoPath, &fsstat);
    if (status < 0) {
        TUILOGE("failed lstat for the logo");
        return 0;
    }

    logoSize = fsstat.st_size;

    *logoBuff = (uint8_t *)tui_malloc(logoSize);
    if (*logoBuff == NULL) {
        TUILOGE("failed to allocate memory for logo");
        return 0;
    }

    fd = open(logoPath, O_RDONLY);
    if (fd < 0) {
        TUILOGE("failed to open logo file");
        tui_free(*logoBuff);
        *logoBuff = NULL;
        return 0;
    }

    size = read(fd, *logoBuff, logoSize);
    if (size != logoSize) {
        TUILOGE("failed to read logo file");
        tui_free(*logoBuff);
        *logoBuff = NULL;
        return 0;
    }

    TUILOGD("read logo file, size = %d", size);
    close(fd);

    return size;
}

uint32_t loadIndicator(uint8_t **indicatorBuff)
{
    int fd = 0, size = 0, status = 0;
    uint32_t indicatorSize = 0;
    char indicatorPathSFS[] =
        "/data/misc/secure_ui/secure_images/secure_logo.png";
    char indicatorPath[] = "/usr/data/sec_ind.png";

    if (indicatorBuff == NULL) {
        return 0;
    }
    TUILOGD("read indicator file, from = %s", indicatorPath);

#ifdef PROVISION
    provision(indicatorPath, indicatorPathSFS);
#endif

#ifdef SFS
    fd = qsee_sfs_open(indicatorPathSFS, O_RDWR);
    if (fd < 0) {
        TUILOGE("failed to open indicator file");
        return 0;
    }

    status = qsee_sfs_getSize(fd, &indicatorSize);
    if (status) {
        TUILOGE("failed to get size for indicator");
        qsee_sfs_close(fd);
        return 0;
    }

    *indicatorBuff = (uint8_t *)tui_malloc(indicatorSize);
    if (*indicatorBuff == NULL) {
        TUILOGE("failed to allocate buffer for indicator");
        tui_sfs_close(fd);
        return 0;
    }

    size = qsee_sfs_read(fd, (char *)*indicatorBuff, indicatorSize);
    if (size != indicatorSize) {
        TUILOGE("failed to read indicator file");
        tui_free(*indicatorBuff);
        *indicatorBuff = NULL;
        return 0;
    }

    TUILOGD("read indicator file, size = %d", size);
    qsee_sfs_close(fd);
#else
    struct stat fsstat;
    status = lstat(indicatorPath, &fsstat);
    if (status < 0) {
        TUILOGE("failed lstat for the indicator");
        return 0;
    }

    indicatorSize = fsstat.st_size;

    *indicatorBuff = (uint8_t *)tui_malloc(indicatorSize);
    if (*indicatorBuff == NULL) {
        TUILOGD("failed to allocate buffer for indicator");
        return 0;
    }

    fd = open(indicatorPath, O_RDONLY);
    if (fd < 0) {
        TUILOGD("failed to open indicator file");
        tui_free(*indicatorBuff);
        *indicatorBuff = NULL;
        return 0;
    }

    size = read(fd, *indicatorBuff, indicatorSize);
    if (size != indicatorSize) {
        TUILOGE("failed to read indicator file");
        tui_free(*indicatorBuff);
        *indicatorBuff = NULL;
        return 0;
    }

    TUILOGD("read indicator file, size = %d", size);
    close(fd);
#endif
    return size;
}

LayoutPage_t *getLayout(const char *id, uint32_t width, uint32_t height)
{
    const char *suffixes[] = {"_large", "", "_small"};
    const uint32_t MAX_DIALOG_NAME_LENGTH = 64;
    uint32_t layout_width = 0, layout_height = 0;
    char buffer[MAX_DIALOG_NAME_LENGTH];
    secure_memset(buffer, 0, MAX_DIALOG_NAME_LENGTH);
    layout_mgr_err_t status = LAYOUT_MGR_SUCCESS;
    LayoutPage_t *layoutPage = NULL;
    TUILOGE("%s : %d, id - %s, width - %d, height - %d", __func__, __LINE__, id, width, height);
    if ((id == NULL) || (width == 0) || (height == 0)) {
        TUILOGE("invalid input param received");
        return NULL;
    }

    /* Iteration is done from large to small layout sizes */
    for (uint32_t i = 0; i < (sizeof(suffixes) / sizeof(*suffixes)); i++) {
        size_t len1 = strlcpy(buffer, id, sizeof(buffer));
        size_t len2 = strlcat(buffer, suffixes[i], sizeof(buffer));
        if (len1 >= sizeof(buffer) || len2 >= sizeof(buffer)) {
            TUILOGE("Truncation error while processing dialog name: %s",
                     buffer);
        }
        TUILOGE("%s : %d, buffer - %s", __func__, __LINE__, buffer);
        layoutPage = get_layout_by_name(buffer);
        if (layoutPage == NULL) {
             TUILOGE("%s : %d, DEBUG: ERROR - get_layout_by_name layoutPage is null", __func__, __LINE__);
            continue;
        }

        status = layout_mgr_load_layout(layoutPage);
        if (status < 0) {
            TUILOGE("%s : %d,DEBUG: ERROR - layout_mgr_load_layout, status - %d", __func__, __LINE__, status);
            continue;
        }
        TUILOGE("%s : %d,DEBUG: layout_mgr_load_layout, status - %d", __func__, __LINE__, status);

        status = layout_mgr_get_layout_size(&layout_height, &layout_width);
        if (status < 0) {
             TUILOGE("%s : %d,DEBUG: ERROR - layout_mgr_get_layout_size, status - %d", __func__, __LINE__, status);
            continue;
        }
        TUILOGE("%s : %d,DEBUG: layout_mgr_get_layout_size, status - %d", __func__, __LINE__, status);
        TUILOGE("%s : %d, DEBUG: Initial height - %d, layout_height - %d, width - %d, layout_width - %d", __func__, __LINE__, height, layout_height, width, layout_width);

        if ((height >= layout_height && width >= layout_width)) {
            TUILOGD("Suitable layout found (%s)", buffer);
            TUILOGD("id: %s, width: %u, height: %u, layout_width: %u, "
                     "layout_height: %u",
                     id, width, height, layout_width, layout_height);
            TUILOGE("%s : %d,DEBUG: height - %d, layout_height - %d, width - %d, layout_width - %d", __func__, __LINE__, height, layout_height, width, layout_width);
            return layoutPage;
        }
    }

    return NULL;
}
