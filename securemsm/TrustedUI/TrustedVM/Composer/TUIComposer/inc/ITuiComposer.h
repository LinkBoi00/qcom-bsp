/*
 * Copyright (c) 2020 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */
#ifndef __TUI_COMPOSER_H__
#define __TUI_COMPOSER_H__

#include <stdint.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C"
{
#endif
/* Generic defines */
typedef enum tuiComposerType {
    TUI_COMPOSER_TYPE_DIALOG = 0x1,
    TUI_COMPOSER_TYPE_CUSTOM = 0x10000,
    TUI_COMPOSER_TYPE_MAX
} tuiComposerType_t;

typedef enum tuiComposerStatus {
    TUI_COMPOSER_STATUS_OK = 0x0,
    TUI_COMPOSER_STATUS_FAILED = 0x1,
    TUI_COMPOSER_STATUS_BAD_PARAM = 0x2,
    TUI_COMPOSER_STATUS_NO_MEM = 0x3,
    TUI_COMPOSER_STATUS_NO_INIT = 0x4,
    TUI_COMPOSER_STATUS_INVALID_LAYOUT = 0x5,
} tuiComposerStatus_t;

typedef enum tuiComposerFormat {
    TUI_COMPOSER_FORMAT_RGBA = 0x1,
    TUI_COMPOSER_FORMAT_MAX
} tuiComposerFormat_t;

typedef struct tuiComposerParam {
    char *layoutId;
    char *msg;
    uint32_t width;
    uint32_t height;
    tuiComposerFormat_t format;
} tuiComposerParam_t;

/* Input related defines */
typedef enum tuiComposerInputType {
    TUI_COMPOSER_INPUT_TOUCH = 0x1,
    TUI_COMPOSER_INPUT_MAX
} tuiComposerInputType_t;

typedef enum tuiComposerInputCommand {
    /**
     * Command used to prepare init screen, this screen will be presented
     * as 1st frame on display (post successful aquisition of secureInput).
     */
    TUI_COMPOSER_INPUT_CMD_START = 0x1,
    /**
     * Command used to prepare termination screen, this screen will be presented
     * as last secure display frame on display (before starting termination of
     * secureInput).
     */
    TUI_COMPOSER_INPUT_CMD_STOP = 0x2,
    /**
     * Command used to prepare next screen based on user input data.
     */
    TUI_COMPOSER_INPUT_CMD_DATA = 0x3,
    /**
     * Command used to prepare next screen based on timeout for user input.
     */
    TUI_COMPOSER_INPUT_CMD_TIMEOUT = 0x4,
    /**
     * Command limit.
     */
    TUI_COMPOSER_INPUT_CMD_MAX = TUI_COMPOSER_INPUT_CMD_TIMEOUT,
} tuiComposerInputCommand_t;

typedef enum tuiComposerInputStatus {
    TUI_COMPOSER_INPUT_STATUS_CONTINUE = 0x1,
    TUI_COMPOSER_INPUT_STATUS_CANCEL = 0x2,
    TUI_COMPOSER_INPUT_STATUS_COMPLETE = 0x3,
    TUI_COMPOSER_INPUT_STATUS_MAX = TUI_COMPOSER_INPUT_STATUS_COMPLETE,
} tuiComposerInputStatus_t;

#define TLAPI_TOUCH_EVENT_NONE  0
/** Down touch event. */
#define TLAPI_TOUCH_EVENT_DOWN  1
/** Move touch event. */
#define TLAPI_TOUCH_EVENT_MOVE  2
/** Up touch event. */
#define TLAPI_TOUCH_EVENT_UP    4

#define MAX_FINGER_NUM 10
typedef struct tuiComposerTouchData {
    uint32_t event; /* 0: None, 1: Down, 2: Move, 4: Up */
    uint32_t xCoOrdinate;
    uint32_t yCoOrdinate;
    uint64_t timestamp; /* Unused */
    uint32_t dirty;
} tuiComposerTouchData_t;

typedef struct tuiComposerTouchInput {
    tuiComposerTouchData_t finger[MAX_FINGER_NUM];
} tuiComposerTouchInput_t;

typedef struct tuiComposerInput {
    tuiComposerInputType_t type;
    tuiComposerInputCommand_t cmd;
    tuiComposerInputStatus_t status;
    int32_t timeOut;
    union {
        tuiComposerTouchInput_t touch;
    } data;
} tuiComposerInput_t;

/* Buffer related defines */
typedef struct tuiComposerBufferHandle {
    uint8_t *vAddr;
    uint32_t size;
} tuiComposerBufferHandle_t;

typedef struct tuiComposerDisplayConfig {
    uint32_t width;
    uint32_t stride;
    uint32_t height;
    tuiComposerFormat_t format;
} tuiComposerDisplayConfig_t;

typedef struct tuiComposerDisplayBuffer {
    tuiComposerDisplayConfig_t config;
    tuiComposerBufferHandle_t handle;
} tuiComposerDisplayBuffer_t;

/* Custom command payload define */
typedef struct tuiComposerCmdPayload {
    void *cmdPtr;
    uint32_t cmdSize;
    void *respPtr;
    uint32_t respSize;
} tuiComposerCmdPayload_t;

/**
 * ITuiComposerCallback:
 *
 * ITuiComposerCallback is callback interface expected to be implemented
 * by caller of ITuiComposer interface.
 * It allows ITuiComposer to request for helper buffer allocation
 * (if needed for rendering/compostion operation).
 *
 */
typedef struct ITuiComposerCallback {
    void *cbData;

    /**
     * allocHelperBuff:
     *
     * Allocate helper buffer for requested size (in bytes).
     *
     * @param cbData ComposerCallback data.
     * @param buffer Buffer handle with requested size {@link
     * tuiComposerBufferHandle_t}.
     *
     * @return status Return status of this operation:
     *     TUI_COMPOSER_STATUS_OK:
     *         Allocation successfull.
     *     TUI_COMPOSER_STATUS_BAD_PARAM:
     *         Invalid parameter.
     *     TUI_COMPOSER_STATUS_NO_MEM:
     *         Allocation failed due to low memory.
     *     TUI_COMPOSER_STATUS_FAILED:
     *         Operation failed due to generic error.
     *
     */
    tuiComposerStatus_t (*allocHelperBuff)(void *cbData,
                                           tuiComposerBufferHandle_t *buffer);

    /**
     * freeHelperBuff:
     *
     * Free already allocated helper buffer.
     *
     * @param cbData ComposerCallback data.
     * @param buffer Buffer handle {@link tuiComposerBufferHandle_t}.
     *
     * @return status Return status of this operation:
     *     TUI_COMPOSER_STATUS_OK:
     *         Allocation successfull.
     *     TUI_COMPOSER_STATUS_BAD_PARAM:
     *         Invalid parameter.
     *     TUI_COMPOSER_STATUS_FAILED:
     *         Operation failed due to generic error.
     *
     */
    tuiComposerStatus_t (*freeHelperBuff)(void *cbData,
                                          tuiComposerBufferHandle_t *buffer);

} ITuiComposerCallback_t;

/**
 * ITuiComposer:
 *
 * TuiComposer public interface for secure graphics content
 * rendering/compostion.
 *
 */
typedef struct ITuiComposer {
    void *ctx;
    tuiComposerType_t type;

    /**
     * init:
     *
     * Initialise composer for requested layout.
     *
     * @param ctx Composer context.
     * @param param Composer init params {@link tuiComposerParam_t}.
     *
     * @return status Return status of this operation:
     *     TUI_COMPOSER_STATUS_OK:
     *         Init completed.
     *     TUI_COMPOSER_STATUS_BAD_PARAM:
     *         Invalid parameter.
     *     TUI_COMPOSER_STATUS_FAILED:
     *         Init failed.
     *
     */
    tuiComposerStatus_t (*init)(void *ctx, tuiComposerParam_t *param,
                                ITuiComposerCallback_t *cb);

    /**
     * compose:
     *
     * Process user input and compose a graphics buffer in accordance with
     * selected layout to be presented on display.
     *
     * @param ctx Composer context.
     * @param input User input {@link tuiComposerInput_t}.
     * @param buffer Display buffer to render into {@link
     * tuiComposerDisplayBuffer_t}.
     *
     * @return status Return status of this operation:
     *     TUI_COMPOSER_STATUS_OK:
     *         Init completed.
     *     TUI_COMPOSER_STATUS_BAD_PARAM:
     *         Invalid parameter.
     *     TUI_COMPOSER_STATUS_FAILED:
     *         Init failed.
     *
     */
    tuiComposerStatus_t (*compose)(void *ctx, tuiComposerInput_t *input,
                                   tuiComposerDisplayBuffer_t *buffer);

    /**
     * sendCommand:
     *
     * Send custom command to composer.
     *
     * @param ctx Composer context.
     * @param cmd Command identifier.
     * @param ctx Command payload structure.
     *
     * @return result Session result.
     *
     */
    tuiComposerStatus_t (*sendCommand)(void *ctx, uint32_t cmd,
                                       tuiComposerCmdPayload_t *payload);

    /**
     * deInit:
     *
     * De-initialise composer.
     *
     * @param ctx Composer context.
     *
     * @return status Return status of this operation:
     *     TUI_COMPOSER_STATUS_OK:
     *         deInit completed.
     *     TUI_COMPOSER_STATUS_BAD_PARAM:
     *         Invalid parameter.
     *     TUI_COMPOSER_STATUS_FAILED:
     *         deInit failed.
     *
     */
    tuiComposerStatus_t (*deInit)(void *ctx);

} ITuiComposer_t;

/* Factory methods to instantiate and destroy Composer objects. */

/**
 * createTUIComposer:
 *
 * Create a new composer object (of requested type) for secure graphics
 * content rendering/compostion.
 *
 * @param type Composer type {@link tuiComposerType_t}.
 *
 * @return object Newly created composer object.
 *
 */
ITuiComposer_t *createTUIComposer(tuiComposerType_t type);

/**
 * destroyTUIComposer:
 *
 * Destroy an already created composer object.
 *
 * @param composer Composer object.
 *
 */
void destroyTUIComposer(ITuiComposer_t *composer);

#ifdef __cplusplus
}
#endif

#endif //__TUI_COMPOSER_H__