/*=============================================================================
  Copyright (c) 2020 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
=============================================================================*/
#pragma once
// AUTOGENERATED FILE: DO NOT EDIT

#include <cstdint>
#include "ITUISystemSession.hpp"
#include "ITUITypes.hpp"
#include "object.h"


/** @file  ITUISystemSession.idl */

/**
 * @brief
 * TUISystemSession interface provides methods needed for TUI Session
 */

class TUISystemSessionImplBase : protected ImplBase, public ITUISystemSession
{
   public:
    TUISystemSessionImplBase()
    {
    }
    virtual ~TUISystemSessionImplBase()
    {
    }

    virtual int32_t invoke(ObjectOp op, ObjectArg *a, ObjectCounts k)
    {
        switch (ObjectOp_methodID(op)) {
            case OP_startSession: {
                if (k != ObjectCounts_pack(2, 0, 0, 0) || a[0].b.size != 4 ||
                    a[1].b.size != 28) {
                    break;
                }
                const int32_t *ihandle_ptr = (const int32_t *)a[0].b.ptr;
                const displayConfig *cfgInfo_ptr =
                    (const displayConfig *)a[1].b.ptr;
                return startSession(*ihandle_ptr, cfgInfo_ptr);
            }
            case OP_getDisplayProperties: {
                if (k != ObjectCounts_pack(0, 1, 0, 0) || a[0].b.size != 28) {
                    break;
                }
                displayConfig *cfgInfo_ptr = (displayConfig *)a[0].b.ptr;
                return getDisplayProperties(cfgInfo_ptr);
            }
            case OP_enqueueBuffer: {
                if (k != ObjectCounts_pack(2, 0, 0, 0) || a[0].b.size != 4 ||
                    a[1].b.size != 28) {
                    break;
                }
                const int32_t *ihandle_ptr = (const int32_t *)a[0].b.ptr;
                const displayConfig *cfgInfo_ptr =
                    (const displayConfig *)a[1].b.ptr;
                return enqueueBuffer(*ihandle_ptr, cfgInfo_ptr);
            }
            case OP_dequeueBuffer: {
                if (k != ObjectCounts_pack(1, 1, 0, 1) || a[0].b.size != 4 ||
                    a[1].b.size != 4) {
                    break;
                }
                int32_t *ihandle_ptr = (int32_t *)a[1].b.ptr;
                const uint32_t *buffSize_ptr = (const uint32_t *)a[0].b.ptr;
                return dequeueBuffer(a[2].o, ihandle_ptr, *buffSize_ptr);
            }
            case OP_allocateAppBuffer: {
                if (k != ObjectCounts_pack(1, 1, 0, 1) || a[0].b.size != 4 ||
                    a[1].b.size != 4) {
                    break;
                }
                const uint32_t *buffSize_ptr = (const uint32_t *)a[0].b.ptr;
                int32_t *ihandle_ptr = (int32_t *)a[1].b.ptr;
                return allocateAppBuffer(*buffSize_ptr, a[2].o, ihandle_ptr);
            }
            case OP_freeAppBuffer: {
                if (k != ObjectCounts_pack(1, 0, 0, 0) || a[0].b.size != 4) {
                    break;
                }
                const int32_t *ihandle_ptr = (const int32_t *)a[0].b.ptr;
                return freeAppBuffer(*ihandle_ptr);
            }
            case OP_getInputData: {
                if (k != ObjectCounts_pack(1, 0, 0, 0) || a[0].b.size != 4) {
                    break;
                }
                const int32_t *timeout_ptr = (const int32_t *)a[0].b.ptr;
                return getInputData(*timeout_ptr);
            }
            case OP_stopSession: {
                return stopSession();
            }
            default: {
                return Object_ERROR_INVALID;
            }
        }
        return Object_ERROR_INVALID;
    }
};
