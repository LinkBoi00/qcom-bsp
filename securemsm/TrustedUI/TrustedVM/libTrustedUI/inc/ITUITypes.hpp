/*=============================================================================
  Copyright (c) 2020 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
=============================================================================*/
#pragma once
// AUTOGENERATED FILE: DO NOT EDIT

#include <cstdint>
#include "object.h"


/** @file  types.idl */

struct TUICreateConfig {
    uint32_t displayId;
    uint32_t numDisplayBuffers;
    uint32_t colorFormat;
    uint32_t inputUID;
};

struct displayConfig {
    uint32_t u32Height;
    uint32_t u32Width;
    uint32_t u32Stride;
    uint32_t u32pixelWidth;
    uint32_t u32ColorFormat;
    uint32_t u32OffHeight;
    uint32_t u32OffWidth;
};
