/*===================================================================================
  Copyright (c) 2020 Qualcomm Technologies, Inc.
  All Rights Reserved.
  Confidential and Proprietary - Qualcomm Technologies, Inc.
  ===================================================================================*/

/******************************************************************************
 *                         Header Inclusions
 *****************************************************************************/
#include <unistd.h>

#include "CTouchInput.hpp"
#include "ITUICoreService.hpp"
#include "ITUICoreService.hpp"
#include "ITrustedInput.hpp"
#include "ITrustedInput_invoke.hpp"
#include "TUILog.h"
#include "TUIUtils.h"
#include "TouchInput.h"
#include "minkipc.h"

using namespace std;

/******************************************************************************
 *       Constant Definitions And Local Variables
*****************************************************************************/
#ifdef LOG_TAG
#undef LOG_TAG
#endif
#define LOG_TAG "TUIVMInput::TrustedInputMain"

static const string coreServiceSocket = "/dev/TUICoreSVC";
static Object TUICoreServiceObj = Object_NULL;
static Object TouchInputObj = Object_NULL;
static MinkIPC *MinkIPCHandle = nullptr;
static unique_ptr<ITUICoreService> mCoreService = nullptr;

/* Description : This API creates a CTouchInput class object pointer
* as the object context and TouchInput mink interface as the invoke.
*
* Out : objOut : Returns the object with invoke and context.
*
* Return : Object_ERROR_KMEM,
*          ITrustedInput::SUCCESS
*/
int32_t CTouchInput_open(Object *objOut)
{
    int32_t ret = ITrustedInput::SUCCESS;
    CTouchInput *me = new CTouchInput();
    if (me == nullptr) {
        TUILOGE("%s::%d - Error: Creating TouchInput pointer object Failed!", __func__, __LINE__);
        ret = Object_ERROR_KMEM;
        goto errorExit;
    }
    *objOut = (Object){ImplBase::invoke, me};
errorExit:
    return ret;
}

/* Description : This API release the MinkIPC handle along
* with TUICoreServiceObj and TouchInputObj.
*/

void cleanUpTrustedInput()
{
    TUILOGD("%s: ++ ", __func__);

    Object_ASSIGN_NULL(TouchInputObj);
    Object_ASSIGN_NULL(TUICoreServiceObj);

    if (MinkIPCHandle != nullptr) {
        MinkIPC_release(MinkIPCHandle);
        MinkIPCHandle = nullptr;
    }

}

/******************************************************************************
 *       Main Entry to TouchInput
*****************************************************************************/

int main()
{
    int32_t ret = ITrustedInput::SUCCESS;

    MinkIPC *MinkIPCHandle =
        MinkIPC_connect(coreServiceSocket.c_str(), &TUICoreServiceObj);

    TUI_CHECK_COND(MinkIPCHandle != nullptr, Object_ERROR);
    TUI_CHECK_COND(!Object_isNull(TUICoreServiceObj), Object_ERROR);

    TUILOGV("%s::%d - Successfully connected to the Coreservice",
           __func__, __LINE__);
    mCoreService = make_unique<TUICoreService>(TUICoreServiceObj);
    TUI_CHECK_COND(mCoreService != nullptr, Object_ERROR);

    ret = CTouchInput_open(&TouchInputObj);
    TUI_CHECK_COND(!Object_isNull(TouchInputObj), Object_ERROR_KMEM);

    //TouchInputObj's lifetime is forever, as long as InputService is alive
    ret = mCoreService->registerInputService(CTouchInput_UID, TouchInputObj);
    TUI_CHECK_COND(ret == ITrustedInput::SUCCESS, ret);
    TUILOGV("%s::%d - Successfully registered input service to the CoreService",
           __func__, __LINE__);
    while (1) {
        sleep(UINT32_MAX);
    }
errorExit:
    cleanUpTrustedInput();
    return ret;
}
