/*========================================================================
Copyright (c) 2020 Qualcomm Technologies, Inc.
All Rights Reserved.
Confidential and Proprietary - Qualcomm Technologies, Inc.
=========================================================================*/
/******************************************************************************
 *                   Header Inclusions
 *****************************************************************************/
#include "object.h"
#include "impl_base.hpp"
#include "proxy_base.hpp"
#include "IOpener_invoke.hpp"
#include "CTrustedUIApp.hpp" //definition of CTrustedUIApp_open
#include "ITrustedUIApp.hpp" //definitions of initSession, deleteSession
#include "TUIUtils.h"
#include "fdwrapper.h"
#include "heap.h"
#include "minkipc.h"
#include <cstddef>
#include <cstdint>
#include <fcntl.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include "TUILog.h"
#include "IRegisterApp.h"

#define LOG_TAG "TUIAPP_SERVICE"

#define TUI_UID 1234

/**
 * @brief This class provides the interface to opener object for TUI APP Service
 */
class CTUIAppOpener : public IOpenerImplBase {
public:
  CTUIAppOpener() {}
  virtual ~CTUIAppOpener() {}
  int32_t open(uint32_t id_val, Object *obj_ref);
};

/*
 * @brief Get TUI App Object. Creates a new app object per TUI session
 * @return Object_OK if success
 * @return Object_ERROR_BADOBJ in case of failure to create an app obj
 * @return ERROR_NOT_FOUND if no spp was regitered with the given UID
 * */
int32_t CTUIAppOpener::open(uint32_t id_val, Object *obj_ref) {
  int32_t ret = Object_OK;
  Object objOut = Object_NULL;
  TUILOGD("%s:%u id=%u", __func__, __LINE__, id_val);
  switch (id_val) {
  case CTrustedUIApp_UID:
    /* Create a new TUI app object for every session*/
    ret = CTrustedUIApp_open(obj_ref);
    if ((ret != 0) || Object_isNull(*obj_ref)) {
      TUILOGE("Failed to get TUI object from tui  service");
      return Object_ERROR_BADOBJ;
    }
    return ret;
  default:
    break;
  }
  TUILOGE("%s: No object found with UID: %d", __func__, id_val);
  return CTUIAppOpener::ERROR_NOT_FOUND;
}

int32_t main() {
  int32_t ret = 0;
  Object tuiAppObject = Object_NULL;
  Object tvmAppOpener = Object_NULL;
  uint8_t  secret[] = "password";
  MinkIPC *client = MinkIPC_connect("/dev/socket/letzd", &tvmAppOpener);

  if (!client) {
    TUILOGE("Failed to create MinkIPC client");
    return Object_ERROR_BADOBJ;
  }
  else if  (Object_isNull(tvmAppOpener)) {
    TUILOGE("Failed to get TVM Opener");
    return Object_ERROR_BADOBJ;
  }
  ret = CTrustedUIApp_open(&tuiAppObject);
  if ((ret != 0) || Object_isNull(tuiAppObject)) {
    TUILOGE("Failed to get TUI object from tui  service");
    return Object_ERROR_BADOBJ;
  }
  TUILOGE("registering tui app");
  ret = IRegisterApp_registerApp(tvmAppOpener, TUI_UID, tuiAppObject, secret, sizeof(secret)/sizeof(uint8_t));

  TUILOGE("%s Registered TUI App to TVM Service", __func__);

  while (1) {
    sleep(UINT32_MAX);
  }

  return 0;
}
