ifeq ($(ENABLE_TRUSTED_UI_2_0), true)
LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_C_INCLUDES := $(TARGET_OUT_HEADERS)/common/inc \
        $(SECUREMSM_SHIP_PATH)/sse/SecureIndicator/inc \
        $(SSE_PATH)/SecureIndicator/SecureIndicator/inc

LOCAL_SHARED_LIBRARIES := \
        libcutils \
        libutils \
        liblog \
        libdisplayconfig.qti

LOCAL_CFLAGS := -O3


LOCAL_SHARED_LIBRARIES += libhidlbase libhidlmemory \
        libbase vendor.display.config@1.3 vendor.qti.hardware.trustedui@1.0 libsi

LOCAL_HEADER_LIBRARIES := display_intf_headers
LOCAL_STATIC_LIBRARIES += libgtest

LOCAL_MODULE := TrustedUISampleTest
LOCAL_PROPRIETARY_MODULE := true
LOCAL_SRC_FILES := TrustedUISampleClient.cpp
LOCAL_MODULE_TAGS := optional
LOCAL_SANITIZE := cfi integer_overflow
LOCAL_MODULE_OWNER := qti
include $(BUILD_EXECUTABLE)
endif
